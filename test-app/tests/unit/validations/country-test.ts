import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { country } from '@getflights/ember-mist-components/validations/country';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import type AddressService from '@getflights/ember-mist-components/services/address';
import sinon from 'sinon';
import { trackedFunction } from 'reactiveweb/function';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';

module('Validation | country', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const addressService = this.owner.lookup(
      'service:address',
    ) as AddressService;

    const selectOptionsStub = sinon.stub(
      addressService,
      'countrySelectOptionsRequest',
    );

    const countrySelectOptionsRequest = trackedFunction(this, async () => {
      const value = await new Promise<SelectOption[]>((resolve) => {
        setTimeout(() => {
          resolve([
            { value: 'BE', label: 'Belgium' },
            { value: 'FR', label: 'France' },
          ]);
        }, 50);
      });

      return value;
    });

    selectOptionsStub.get(() => countrySelectOptionsRequest);

    const countryValidation = country();

    assert.strictEqual(
      countryValidation.name,
      'country',
      'country validation has the correct name',
    );

    const api = {
      owner: this.owner,
    };

    // None
    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // Valid country
      ['BE', '"BE"'],
      ['FR', '"FR"'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await countryValidation.test(value, api),
        `${label} is valid`,
      );
    }

    // Not truthy
    const invalids = [
      [{}, '{}'],
      // Invalid country
      ['NL', '"XY"'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        countryValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
