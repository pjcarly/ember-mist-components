import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { dependent } from '@getflights/ember-mist-components/validations/dependent';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | dependent', function (hooks) {
  setupTest(hooks);

  test('validate (field)', async function (assert) {
    const target: {
      dependentField: string | undefined;
      field: string | undefined;
    } = {
      dependentField: undefined,
      field: undefined,
    };

    const dependencies = new Map<string, string[]>([
      ['A', ['Alfa', 'Alpha']],
      ['B', ['Bravo']],
    ]);

    const dependentValidation = dependent<
      typeof target,
      'field',
      'dependentField'
    >('dependentField', dependencies);

    assert.strictEqual(
      dependentValidation.name,
      'dependent',
      'dependent validation has the correct name',
    );

    const api = {
      owner: this.owner,
      context: {
        target,
        key: 'field',
      },
    } as const;

    assert.true(
      await dependentValidation.test(undefined, api),
      `undefined is valid`,
    );
    assert.true(await dependentValidation.test(null, api), `null is valid`);

    const alwaysInvalids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    /**
     * dependentField = undefined: nothing is valid
     */
    target.dependentField = undefined;

    await assert.rejects(
      dependentValidation.test('Alpha', api),
      ValidationFailure,
      `"Alpha" is not valid (dependentField is none)`,
    );

    await assert.rejects(
      dependentValidation.test('Alfa', api),
      ValidationFailure,
      `"Alfa" is not valid (dependentField is none)`,
    );

    await assert.rejects(
      dependentValidation.test('Bravo', api),
      ValidationFailure,
      `"Bravo" is not valid (dependentField is none)`,
    );

    for (const [value, label] of alwaysInvalids) {
      await assert.rejects(
        dependentValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (always)`,
      );
    }

    /**
     * dependentField = "A": ['Alpha', 'Alfa] are valid
     */
    target.dependentField = 'A';

    assert.true(
      await dependentValidation.test('Alpha', api),
      `"Alpha" is valid (dependentField is "A")`,
    );

    assert.true(
      await dependentValidation.test('Alfa', api),
      `"Alfa" is valid (dependentField is "A")`,
    );

    await assert.rejects(
      dependentValidation.test('Bravo', api),
      ValidationFailure,
      `"Bravo" is not valid (dependentField is "A")`,
    );

    for (const [value, label] of alwaysInvalids) {
      await assert.rejects(
        dependentValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (always)`,
      );
    }

    /**
     * dependentField = "A": Bravo is valid
     */
    target.dependentField = 'B';

    await assert.rejects(
      dependentValidation.test('Alpha', api),
      ValidationFailure,
      `"Alpha" is not valid (dependentField is "B")`,
    );

    await assert.rejects(
      dependentValidation.test('Alfa', api),
      ValidationFailure,
      `"Alfa" is not valid (dependentField is "B")`,
    );

    assert.true(
      await dependentValidation.test('Bravo', api),
      `"Bravo" is valid (dependentField is "B")`,
    );

    for (const [value, label] of alwaysInvalids) {
      await assert.rejects(
        dependentValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (always)`,
      );
    }
  });
});
