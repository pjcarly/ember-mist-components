import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { address } from '@getflights/ember-mist-components/validations/address';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import type Store from '@ember-data/store';

module('Validation | address', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const addressValidation = address();

    assert.strictEqual(
      addressValidation.name,
      'address',
      'address validation has the correct name',
    );

    const store = this.owner.lookup('service:store') as Store;

    store.createRecord('address-format', {
      id: 'BE',
      requiredFields: ['addressLine1', 'postalCode'],
    });

    const api = {
      owner: this.owner,
    };

    // None
    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Address, with correct required fields
      [
        { countryCode: 'BE', addressLine1: 'Line 1', postalCode: '1234' },
        '{ countryCode: "BE", addressLine1: "Line 1", postalCode: "1234" }',
      ],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await addressValidation.test(value, api),
        `${label} is valid`,
      );
    }

    // Not truthy
    const invalids = [
      [{}, '{}'],
      ['notAnAddress', '"notAnAddress"'],
      // Address, but missing required fields
      [{ countryCode: 'BE' }, '{ countryCode: "BE" }'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        addressValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
