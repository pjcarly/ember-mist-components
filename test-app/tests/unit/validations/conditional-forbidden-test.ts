import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { conditionalForbidden } from '@getflights/ember-mist-components/validations/conditional-forbidden';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import type { ContextualValidationFunctionAPI } from '@getflights/ember-attribute-validations/interfaces/validation';

module('Validation | conditional-forbidden', function (hooks) {
  setupTest(hooks);

  test('validate (field)', async function (assert) {
    const target = {
      key: 'value',
      condition: false,
    };

    const api: ContextualValidationFunctionAPI<typeof target, 'key'> = {
      owner: this.owner,
      context: {
        target,
        key: 'key',
      },
    };

    const conditionalForbiddenValidation = conditionalForbidden<
      typeof target,
      'key'
    >('condition');

    assert.strictEqual(
      conditionalForbiddenValidation.name,
      'conditional-forbidden',
      'conditional-forbidden validation has the correct name',
    );

    // Null, undefined or empty is valid
    const valids = [
      [null, 'null'],
      [undefined, 'undefined'],
      [[], 'empty array'],
      ['', 'empty string'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await conditionalForbiddenValidation.test(value, api),
        `${label} is valid`,
      );
    }

    // All else is invalid
    const invalids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    // Valid because condition is not met
    for (const [value, label] of valids) {
      assert.true(
        await conditionalForbiddenValidation.test(value, api),
        `${label} is valid (condition is not met)`,
      );
    }

    target.condition = true;

    for (const [value, label] of invalids) {
      await assert.rejects(
        conditionalForbiddenValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (condition is met)`,
      );
    }
  });

  test('validate (nested field)', async function (assert) {
    const target = {
      key: 'value',
      nested: {
        condition: false,
      },
    };

    const api: ContextualValidationFunctionAPI<typeof target, 'key'> = {
      owner: this.owner,
      context: {
        target,
        key: 'key',
      },
    };

    const conditionalForbiddenValidation = conditionalForbidden<
      typeof target,
      'key'
    >('nested.condition');

    // Null, undefined or empty is valid
    const valids = [
      [null, 'null'],
      [undefined, 'undefined'],
      [[], 'empty array'],
      ['', 'empty string'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await conditionalForbiddenValidation.test(value, api),
        `${label} is valid`,
      );
    }

    // All else is invalid
    const invalids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    // Valid because condition is not met
    for (const [value, label] of valids) {
      assert.true(
        await conditionalForbiddenValidation.test(value, api),
        `${label} is valid (condition is not met)`,
      );
    }

    target.nested.condition = true;

    for (const [value, label] of invalids) {
      await assert.rejects(
        conditionalForbiddenValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (condition is met)`,
      );
    }
  });

  test('validate (assertion function)', async function (assert) {
    const target = {
      key: 'value',
    };

    const api: ContextualValidationFunctionAPI<typeof target, 'key'> = {
      owner: this.owner,
      context: {
        target,
        key: 'key' as const,
      },
    };

    let result = false;
    const conditionalForbiddenValidation = conditionalForbidden<
      typeof target,
      'key'
    >(() => result);

    // Null, undefined or empty is valid
    const valids = [
      [null, 'null'],
      [undefined, 'undefined'],
      [[], 'empty array'],
      ['', 'empty string'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await conditionalForbiddenValidation.test(value, api),
        `${label} is valid`,
      );
    }

    // All else is invalid
    const invalids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    // Valid because condition is not met
    for (const [value, label] of valids) {
      assert.true(
        await conditionalForbiddenValidation.test(value, api),
        `${label} is valid (condition is not met)`,
      );
    }

    result = true;

    for (const [value, label] of invalids) {
      await assert.rejects(
        conditionalForbiddenValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (condition is met)`,
      );
    }
  });
});
