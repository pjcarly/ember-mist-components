import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { conditionalRequired } from '@getflights/ember-mist-components/validations/conditional-required';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import type { ContextualValidationFunctionAPI } from '@getflights/ember-attribute-validations/interfaces/validation';

module('Validation | conditional-required', function (hooks) {
  setupTest(hooks);

  test('validate (field)', async function (assert) {
    const target = {
      key: 'value',
      condition: false,
    };

    const api: ContextualValidationFunctionAPI<typeof target, 'key'> = {
      owner: this.owner,
      context: {
        target,
        key: 'key',
      },
    };

    const conditionalRequiredValidation = conditionalRequired<
      typeof target,
      'key'
    >('condition');

    assert.strictEqual(
      conditionalRequiredValidation.name,
      'conditional-required',
      'conditional-required validation has the correct name',
    );

    // Null, undefined or empty is not valid
    const invalids = [
      [null, 'null'],
      [undefined, 'undefined'],
      [[], 'empty array'],
      ['', 'empty string'],
    ];

    // Valid because condition is not met
    for (const [value, label] of invalids) {
      assert.true(
        await conditionalRequiredValidation.test(value, api),
        `${label} is valid (condition is not met)`,
      );
    }

    // All else is valid
    const valids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await conditionalRequiredValidation.test(value, api),
        `${label} is valid`,
      );
    }

    target.condition = true;

    for (const [value, label] of invalids) {
      await assert.rejects(
        conditionalRequiredValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (condition is met)`,
      );
    }
  });

  test('validate (nested field)', async function (assert) {
    const target = {
      key: 'value',
      nested: {
        condition: false,
      },
    };

    const api: ContextualValidationFunctionAPI<typeof target, 'key'> = {
      owner: this.owner,
      context: {
        target,
        key: 'key',
      },
    };

    const conditionalRequiredValidation = conditionalRequired<
      typeof target,
      'key'
    >('nested.condition');

    // Null, undefined or empty is not valid
    const invalids = [
      [null, 'null'],
      [undefined, 'undefined'],
      [[], 'empty array'],
      ['', 'empty string'],
    ];

    // Valid because condition is not met
    for (const [value, label] of invalids) {
      assert.true(
        await conditionalRequiredValidation.test(value, api),
        `${label} is valid (condition is not met)`,
      );
    }

    // All else is valid
    const valids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await conditionalRequiredValidation.test(value, api),
        `${label} is valid`,
      );
    }

    target.nested.condition = true;

    for (const [value, label] of invalids) {
      await assert.rejects(
        conditionalRequiredValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (condition is met)`,
      );
    }
  });

  test('validate (assertion function)', async function (assert) {
    const target = {
      key: 'value',
    };

    const api: ContextualValidationFunctionAPI<typeof target, 'key'> = {
      owner: this.owner,
      context: {
        target,
        key: 'key' as const,
      },
    };

    let result = false;
    const conditionalRequiredValidation = conditionalRequired<
      typeof target,
      'key'
    >(() => result);

    // Null, undefined or empty is not valid
    const invalids = [
      [null, 'null'],
      [undefined, 'undefined'],
      [[], 'empty array'],
      ['', 'empty string'],
    ];

    // Valid because condition is not met
    for (const [value, label] of invalids) {
      assert.true(
        await conditionalRequiredValidation.test(value, api),
        `${label} is valid (condition is not met)`,
      );
    }

    // All else is valid
    const valids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await conditionalRequiredValidation.test(value, api),
        `${label} is valid`,
      );
    }

    result = true;

    for (const [value, label] of invalids) {
      await assert.rejects(
        conditionalRequiredValidation.test(value, api),
        ValidationFailure,
        `${label} is not valid (condition is met)`,
      );
    }
  });
});
