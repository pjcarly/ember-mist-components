import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { phone } from '@getflights/ember-mist-components/validations/phone';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | phone', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const phoneValidation = phone();

    assert.strictEqual(
      phoneValidation.name,
      'phone',
      'phone validation has the correct name',
    );

    const api = {
      owner: this.owner,
    };

    // None
    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Valid phone numbers
      ['+3250661616'],
      ['+32474123456'],
      ['+32474123456'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await phoneValidation.test(value, api),
        `${label ?? value} is valid`,
      );
    }

    // Not truthy
    const invalids = [['tel', '"tel"'], ['+3249136']];

    for (const [value, label] of invalids) {
      await assert.rejects(
        phoneValidation.test(value, api),
        ValidationFailure,
        `${label ?? value} is not valid`,
      );
    }
  });
});
