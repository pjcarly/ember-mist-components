import type Store from '@ember-data/store';
import type { TestContext } from '@ember/test-helpers';
import {
  camelizeObject,
  dasherizeObject,
} from '@getflights/ember-mist-components/utils/camelize-dasherize-object';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';

interface PSUtilsTestContext extends TestContext {
  store: Store;
}

module('Unit | Utils | camelizeObject + dasherizeObject', function (hooks) {
  setupTest(hooks);

  test('camelizeObject', async function (this: PSUtilsTestContext, assert) {
    // A JSON value can be an object, array, number, string, true, false, or null.
    // So we test all of these
    const initial = {
      'object-property': {
        'array-property': [123],
        'object-property': {
          'some-property': 123,
        },
        'number-property': 123,
        'string-property': 'str',
        'true-property': true,
        'false-property': false,
        'null-property': null,
      },
      'array-property': [
        { 'some-property': 123 },
        [123, { 'some-property': [123] }],
        123,
        'str',
        true,
        false,
        null,
      ],
      'number-property': 123,
      'string-property': 'str',
      'true-property': true,
      'false-property': false,
      'null-property': null,
    };

    const expected = {
      objectProperty: {
        'array-property': [123],
        'object-property': {
          'some-property': 123,
        },
        'number-property': 123,
        'string-property': 'str',
        'true-property': true,
        'false-property': false,
        'null-property': null,
      },
      arrayProperty: [
        { 'some-property': 123 },
        [123, { 'some-property': [123] }],
        123,
        'str',
        true,
        false,
        null,
      ],
      numberProperty: 123,
      stringProperty: 'str',
      trueProperty: true,
      falseProperty: false,
      nullProperty: null,
    };

    const expectedRecursive = {
      objectProperty: {
        arrayProperty: [123],
        objectProperty: {
          someProperty: 123,
        },
        numberProperty: 123,
        stringProperty: 'str',
        trueProperty: true,
        falseProperty: false,
        nullProperty: null,
      },
      arrayProperty: [
        { someProperty: 123 },
        [123, { someProperty: [123] }],
        123,
        'str',
        true,
        false,
        null,
      ],
      numberProperty: 123,
      stringProperty: 'str',
      trueProperty: true,
      falseProperty: false,
      nullProperty: null,
    };

    assert.propEqual(
      camelizeObject(initial),
      expected,
      'ok (not recursive by default)',
    );
    assert.propEqual(
      camelizeObject(initial, { recursive: true }),
      expectedRecursive,
      'recursive ok',
    );
  });

  test('camelizeObject: replace null with undefined', async function (this: PSUtilsTestContext, assert) {
    // A JSON value can be an object, array, number, string, true, false, or null.
    // So we test all of these
    const initial = {
      'object-property': {
        'null-property': null,
      },
      'array-property': [null],
      'null-property': null,
    };

    const expected = {
      objectProperty: {
        'null-property': null,
      },
      arrayProperty: [null],
      nullProperty: undefined,
    };

    const expectedRecursive = {
      objectProperty: {
        nullProperty: undefined,
      },
      arrayProperty: [undefined],
      nullProperty: undefined,
    };

    assert.propEqual(
      camelizeObject(initial, {
        replaceNullWithUndefined: true,
      }),
      expected,
      'ok (not recursive by default)',
    );
    assert.propEqual(
      camelizeObject(initial, {
        recursive: true,
        replaceNullWithUndefined: true,
      }),
      expectedRecursive,
      'recursive ok',
    );
  });

  test('dasherizeObject', async function (this: PSUtilsTestContext, assert) {
    // A JSON value can be an object, array, number, string, true, false, or null.
    // So we test all of these
    const initial = {
      objectProperty: {
        arrayProperty: [123],
        objectProperty: {
          someProperty: 123,
        },
        numberProperty: 123,
        stringProperty: 'str',
        trueProperty: true,
        falseProperty: false,
        nullProperty: null,
      },
      arrayProperty: [
        { someProperty: 123 },
        [123, { someProperty: [123] }],
        123,
        'str',
        true,
        false,
        null,
      ],
      numberProperty: 123,
      stringProperty: 'str',
      trueProperty: true,
      falseProperty: false,
      nullProperty: null,
    };

    const expected = {
      'object-property': {
        arrayProperty: [123],
        objectProperty: {
          someProperty: 123,
        },
        numberProperty: 123,
        stringProperty: 'str',
        trueProperty: true,
        falseProperty: false,
        nullProperty: null,
      },
      'array-property': [
        { someProperty: 123 },
        [123, { someProperty: [123] }],
        123,
        'str',
        true,
        false,
        null,
      ],
      'number-property': 123,
      'string-property': 'str',
      'true-property': true,
      'false-property': false,
      'null-property': null,
    };

    const expectedRecursive = {
      'object-property': {
        'array-property': [123],
        'object-property': {
          'some-property': 123,
        },
        'number-property': 123,
        'string-property': 'str',
        'true-property': true,
        'false-property': false,
        'null-property': null,
      },
      'array-property': [
        { 'some-property': 123 },
        [123, { 'some-property': [123] }],
        123,
        'str',
        true,
        false,
        null,
      ],
      'number-property': 123,
      'string-property': 'str',
      'true-property': true,
      'false-property': false,
      'null-property': null,
    };

    assert.propEqual(
      dasherizeObject(initial),
      expected,
      'ok (not recursive by default)',
    );
    assert.propEqual(
      dasherizeObject(initial, { recursive: true }),
      expectedRecursive,
      'recursive ok',
    );
  });
});
