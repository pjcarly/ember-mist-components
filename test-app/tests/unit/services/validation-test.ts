import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import MistValidationService from '@getflights/ember-mist-components/services/validation';
import { maxLength } from '@getflights/ember-attribute-validations/validations/max-length';
import { number } from '@getflights/ember-attribute-validations/validations/number';
import { address } from '@getflights/ember-mist-components/validations/address';
// import field from '@getflights/ember-field-components/decorators/field';

module('Validation Service', function (hooks) {
  setupTest(hooks);

  test('validationsForTransformType', function (assert) {
    const validationService = this.owner.lookup(
      'service:validation',
    ) as MistValidationService;

    const maxLength255Validation = maxLength(255);
    const numberValidation = number();
    const addressValidation = address();

    // string
    assert.propEqual(
      validationService
        .validationsForTransformType('string')
        .map((v) => v.name),
      [maxLength255Validation.name],
      'string type has maxLength(255) validation',
    );

    // number
    assert.propEqual(
      validationService
        .validationsForTransformType('number')
        .map((v) => v.name),
      [numberValidation.name],
      'number type has number validation',
    );

    // address
    assert.propEqual(
      validationService
        .validationsForTransformType('address')
        .map((v) => v.name),
      [addressValidation.name],
      'address type has address validation',
    );
  });
});
