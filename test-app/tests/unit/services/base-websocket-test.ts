import ApplicationInstance from '@ember/application/instance';
import { type TestContext, triggerEvent } from '@ember/test-helpers';
import BaseWebsocketService, {
  BaseWebsocketEvent,
  type WebsocketServiceArguments,
} from '@getflights/ember-mist-components/services/base-websocket';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { Server as MockSocketServer } from 'mock-socket';

const socketUrl = 'ws://ws.somewebsocket.abc';

interface WebsocketServiceTestContext extends TestContext {
  server: MockSocketServer;
}

const timeout = (ms: number) =>
  new Promise((_resolve, reject) => setTimeout(reject, ms));

const setVisibility = (value: 'hidden' | 'visible') => {
  Object.defineProperty(document, 'visibilityState', { value, writable: true });
  triggerEvent(document, 'visibilitychange');
};

module('Unit | Service | Websocket', function (hooks) {
  setupTest(hooks);

  hooks.beforeEach(function (this: WebsocketServiceTestContext) {
    this.server = new MockSocketServer(socketUrl);
  });

  hooks.afterEach(function (this: WebsocketServiceTestContext) {
    this.server.stop();
  });

  test('Connect / autoReconnect / suspend', async function (this: WebsocketServiceTestContext, assert) {
    class TestWSService extends BaseWebsocketService {
      suspendConnectionWhenIdle = true;
      autoReconnect = true;

      constructor(
        owner: ApplicationInstance,
        _args: WebsocketServiceArguments,
      ) {
        super(owner, { url: socketUrl });
      }
    }

    this.owner.register('service:websocket', TestWSService);
    const websocketService = this.owner.lookup(
      'service:websocket',
    ) as TestWSService;

    /**
     * Opening the connection
     */
    const testOpenConnection = new Promise<void>((resolve) => {
      // Websocket connection
      const onOpen = () => {
        websocketService.removeWebsocketSubscription(
          BaseWebsocketEvent.OPEN,
          onOpen,
        );
        resolve();
      };
      websocketService.addWebsocketSubscription(
        BaseWebsocketEvent.OPEN,
        onOpen,
      );
      // Open websocket via service
      websocketService.openConnection();
    });

    await Promise.race([testOpenConnection, timeout(5000)]);

    assert.strictEqual(
      websocketService.socketRef.readyState,
      websocketService.socketRef.OPEN,
      'Websocket is OPEN, connecting works.',
    );

    /**
     * Should automatically retry
     */
    const testAutoreconnect = new Promise<void>((resolve) => {
      // Websocket connection
      const onOpen = () => {
        websocketService.removeWebsocketSubscription(
          BaseWebsocketEvent.OPEN,
          onOpen,
        );
        resolve();
      };
      websocketService.addWebsocketSubscription(
        BaseWebsocketEvent.OPEN,
        onOpen,
      );

      // Should try to reconnect
      this.server.close();
      this.server = new MockSocketServer(socketUrl);
    });

    await Promise.race([testAutoreconnect, timeout(5000)]);

    assert.strictEqual(
      websocketService.socketRef.readyState,
      websocketService.socketRef.OPEN,
      'Websocket is OPEN, retrying connection works.',
    );

    /**
     * Should suspend connection
     */
    const testSuspend = new Promise<void>((resolve) => {
      // Websocket connection
      const onClose = () => {
        websocketService.removeWebsocketSubscription(
          BaseWebsocketEvent.CLOSE,
          onClose,
        );
        resolve();
      };
      websocketService.addWebsocketSubscription(
        BaseWebsocketEvent.CLOSE,
        onClose,
      );

      // Should suspend
      setVisibility('hidden');
    });

    await Promise.race([testSuspend, timeout(5000)]);

    assert.strictEqual(
      websocketService.socketRef.readyState,
      websocketService.socketRef.CLOSED,
      'Websocket is CLOSED, suspending websocket works.',
    );

    /**
     * Should reconnect after returning to tab
     */
    const testSuspendReconnect = new Promise<void>((resolve) => {
      // Websocket connection
      const onOpen = () => {
        websocketService.removeWebsocketSubscription(
          BaseWebsocketEvent.OPEN,
          onOpen,
        );
        resolve();
      };
      websocketService.addWebsocketSubscription(
        BaseWebsocketEvent.OPEN,
        onOpen,
      );

      // Should suspend
      setVisibility('visible');
    });

    await Promise.race([testSuspendReconnect, timeout(5000)]);

    assert.strictEqual(
      websocketService.socketRef.readyState,
      websocketService.socketRef.OPEN,
      'Websocket is OPEN, suspending undone when returning to tab.',
    );

    websocketService.destroy();
  });
});
