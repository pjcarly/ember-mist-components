import Model, {
  attr,
  belongsTo,
  hasMany,
  type SyncHasMany,
} from '@ember-data/model';
import type Store from '@ember-data/store';
// @ts-ignore
import { recordIdentifierFor } from '@ember-data/store';
import { module, skip, test } from 'qunit';
import JSONAPIEmbeddedSerializer from 'test-app/serializers/json-api-embedded';
import { setupTest } from 'test-app/tests/helpers';

class SalesInvoiceModel extends Model {
  @hasMany('sales-invoice-line', {
    inverse: 'salesInvoice',
    rollback: true,
    async: false,
  })
  declare salesInvoiceLines: SyncHasMany<SalesInvoiceLineModel>;
}

class SalesInvoiceLineModel extends Model {
  @belongsTo('sales-invoice', { inverse: 'salesInvoiceLines', async: false })
  declare salesInvoice?: SalesInvoiceModel;

  @attr()
  declare description: string | undefined;
}

class SalesInvoiceSerializer extends JSONAPIEmbeddedSerializer {
  attrs = {
    salesInvoiceLines: {
      serialize: 'records',
    },
  };
}

module(
  'Integration | Serializers | JsonAPIEmbeddedSerializer',
  function (hooks) {
    setupTest(hooks);

    let store: Store;

    hooks.beforeEach(function () {
      this.owner.register('model:sales-invoice', SalesInvoiceModel);
      this.owner.register('model:sales-invoice-line', SalesInvoiceLineModel);
      this.owner.register('serializer:sales-invoice', SalesInvoiceSerializer);
      store = this.owner.lookup('service:store');
    });

    skip('Creating a new record', async function (assert) {
      const newSalesInvoice = store.createRecord('sales-invoice');

      // What's the `lid` of this store?

      const newSalesInvoiceIdentifier = recordIdentifierFor(newSalesInvoice);

      console.log('The newly created lid is', newSalesInvoiceIdentifier.lid);

      const serialized = newSalesInvoice.serialize();

      console.log('Serialized', serialized);

      assert.notStrictEqual(
        newSalesInvoiceIdentifier.lid,
        undefined,
        'Has a lid',
      );

      assert.propEqual(
        serialized,
        {
          data: {
            type: 'sales-invoices',
            lid: newSalesInvoiceIdentifier.lid,
          },
        },
        'Serialized invoice has a lid',
      );

      store.pushPayload({
        data: {
          id: '1',
          lid: newSalesInvoiceIdentifier.lid,
          type: 'sales-invoices',
        },
      });

      const pushedSalesInvoice = store.peekRecord('sales-invoice', '1');

      assert.strictEqual(
        newSalesInvoice,
        pushedSalesInvoice,
        'Matches on lid successfully',
      );
    });

    test('Creating a new record with embedded records', async function (assert) {
      const newSalesInvoice = store.createRecord('sales-invoice');
      const newSalesInvoiceLine = store.createRecord('sales-invoice-line', {
        salesInvoice: newSalesInvoice,
      });

      assert.strictEqual(
        newSalesInvoice.salesInvoiceLines.length,
        1,
        'Relation on invoice OK (length)',
      );
      assert.strictEqual(
        newSalesInvoice.salesInvoiceLines.firstObject,
        newSalesInvoiceLine,
        'Relation on invoice OK (firstObject)',
      );
      assert.strictEqual(
        newSalesInvoiceLine.salesInvoice,
        newSalesInvoice,
        'Relation on line OK',
      );

      // What's the `lid` of this store?

      const newSalesInvoiceIdentifier = recordIdentifierFor(newSalesInvoice);
      const newSalesInvoiceLineIdentifier =
        recordIdentifierFor(newSalesInvoiceLine);

      console.log(
        'The newly created lids are',
        newSalesInvoiceIdentifier.lid,
        'and',
        newSalesInvoiceLineIdentifier.lid,
      );

      console.log('Serialized', newSalesInvoice.serialize());

      store.pushPayload({
        data: {
          id: '1',
          type: 'sales-invoices',
          lid: newSalesInvoiceIdentifier.lid,
          relationships: {
            'sales-invoice-lines': {
              data: [
                {
                  id: '99',
                  type: 'sales-invoice-lines',
                  lid: newSalesInvoiceLineIdentifier.lid,
                },
              ],
            },
          },
        },
        included: [
          {
            id: '99',
            type: 'sales-invoice-lines',
            lid: newSalesInvoiceLineIdentifier.lid,
            attributes: {
              description: 'Eerste lijntje',
            },
          },
        ],
      });

      const pushedSalesInvoice = store.peekRecord('sales-invoice', '1');
      const pushedSalesInvoiceLine = store.peekRecord(
        'sales-invoice-line',
        '99',
      );

      assert.strictEqual(
        newSalesInvoice,
        pushedSalesInvoice,
        'Invoice matches on lid successfully',
      );
      assert.strictEqual(
        newSalesInvoiceLine,
        pushedSalesInvoiceLine,
        'Invoice line matches on lid successfully',
      );

      console.log(store);
    });
  },
);
