import { module, test } from 'qunit';
import { setupRenderingTest } from 'test-app/tests/helpers';
import { clearRender, render, waitFor } from '@ember/test-helpers';
import { setupIntl, t } from 'ember-intl/test-support';
import Adapter from '@ember-data/adapter';
import type RSVP from 'rsvp';
import Model from '@ember-data/model';
import ListViewSelectComponent from '@getflights/ember-mist-components/components/list-view-select';

class ListViewModelStub extends Model {}

/**
 * This test is probably over-complicated with the adapter and everything but it works!
 */
module('Integration | Component | list-view-select', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  hooks.beforeEach(async function () {
    this.owner.register('model:list-view', ListViewModelStub);
  });

  test('it renders', async function (assert) {
    const stubListView = {
      id: '1',
      type: 'list-view',
      attributes: {
        name: 'x',
      },
    };

    class StubAdapter extends Adapter {
      query(_store: any, _type: any, query: any, _recordArray: any) {
        const { filter } = query;

        const firstFilter = filter[1];

        return new Promise((resolve) => {
          const result = {
            data: [] as any[],
          };

          if (
            firstFilter.field === 'grouping' &&
            firstFilter.value === 'exists'
          ) {
            result.data.push(stubListView);
          }

          resolve(result);
        }) as RSVP.Promise<any>;
      }
    }

    this.owner.register('adapter:list-view', StubAdapter);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(<template>
      <ListViewSelectComponent @grouping='doesnotexist' />
    </template>);

    // Has the 'All' option
    assert.dom('select').exists();
    assert.dom('option').exists({ count: 1 });
    // Blank value = "All"
    assert.dom('option').hasValue('');
    assert.dom('option').hasText(t('label.all'));

    await clearRender();

    await render(<template>
      <ListViewSelectComponent @grouping='exists' />
    </template>);

    // Not really needed, but to be sure
    await waitFor('select > option');

    assert.dom('select').exists();
    assert.dom('option').exists({ count: 2 });
    // Blank value = "All"
    assert.dom('option[value=""]').exists();
    assert.dom('option[value=""]').hasText(t('label.all'));
    assert.dom(`option[value="${stubListView.id}"]`).exists();
  });
});
