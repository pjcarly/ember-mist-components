import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  fillIn,
  render,
  settled,
  tab,
  triggerKeyEvent,
} from '@ember/test-helpers';
import InputTimeComponent from '@getflights/ember-mist-components/components/input/time';
import { tracked } from 'tracked-built-ins';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Component | input/time', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('Using the input to manually insert time', async function (assert) {
    const state = tracked({
      value: undefined as string | undefined,
    });

    const valueChanged = (newValue: string | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputTimeComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');
    assert.dom('[data-test-input]').hasAttribute('placeholder', 'HH:mm:ss');
    // Using TAB to blur input
    await fillIn('[data-test-input]', '12:34:56');
    await tab();

    assert
      .dom('[data-test-input]')
      .hasValue('12:34:56', 'Value is "12:34:56" after filling in.');
    assert.strictEqual(
      state.value,
      '12:34:56',
      'Variable has been updated by valueChanged',
    );

    // Using ENTER to blur input
    await fillIn('[data-test-input]', '06:12:34');
    await triggerKeyEvent('[data-test-input]', 'keydown', 'Enter');

    assert
      .dom('[data-test-input]')
      .hasValue('06:12:34', 'Value is "06:12:34" after filling in.');
    assert.strictEqual(
      state.value,
      '06:12:34',
      'Variable has been updated by valueChanged',
    );

    // Clearing input
    await fillIn('[data-test-input]', '');
    await tab();

    assert
      .dom('[data-test-input]')
      .hasValue('', 'Value is empty after filling in nothing.');
    assert.strictEqual(
      state.value,
      undefined,
      'Variable has been updated to undefined by valueChanged',
    );
  });

  // test('Using time picker to select a time', async function (assert) {
  // });

  test('@required', async function (assert) {
    const state = tracked<{ required?: boolean }>({});

    await render(<template>
      <InputTimeComponent
        @value={{undefined}}
        @required={{state.required}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is undefined => not required.');

    state.required = true;
    await settled();

    assert
      .dom('[data-test-input]')
      .isRequired('@required is true => required.');

    state.required = false;
    await settled();

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is false => not required.');
  });
});
