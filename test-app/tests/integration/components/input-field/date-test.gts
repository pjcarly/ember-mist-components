import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import { render } from '@ember/test-helpers';
import InputFieldDateComponent from '@getflights/ember-mist-components/components/input-field/date';

module('Integration | Component | input-field/date', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('renders an input/date', async function (assert) {
    const model = { field: 'value' };

    await render(<template>
      <InputFieldDateComponent @model={{model}} @field={{'field'}} />
    </template>);

    assert.dom('input').hasClass('input-date', 'Input has "input-date" class.');
  });
});
