import { module, test } from 'qunit';
import { setupRenderingTest } from 'test-app/tests/helpers';
import { type TestContext, render, settled } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import ChangeTrackerModel from '@getflights/ember-mist-components/models/change-tracker-model';
import Store from '@ember-data/store';
import Model, {
  type SyncHasMany,
  attr,
  belongsTo,
  hasMany,
} from '@ember-data/model';
import EmberObject from '@ember/object';

class ParentModel extends Model {
  @hasMany('test', { async: false, inverse: 'parent' })
  declare tests: SyncHasMany<TestModel>;
}

class TestModel extends ChangeTrackerModel {
  @belongsTo('parent', { async: false, inverse: 'tests' })
  declare parent: ParentModel;

  @attr()
  declare someProperty?: string;
}

interface ChangeTrackerModelTestContext extends TestContext {
  store: Store;
  model: TestModel;
}

module('Integration | Models | ChangeTrackerModel', function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function (this: ChangeTrackerModelTestContext) {
    this.owner.register('model:test', TestModel);
    this.owner.register('model:parent', ParentModel);

    this.owner.register(
      'adapter:test',
      class TestAdapter extends EmberObject {
        createRecord() {
          return Promise.resolve();
        }

        updateRecord() {
          return Promise.resolve();
        }
      },
    );

    this.store = this.owner.lookup('service:store') as Store;
    this.model = this.store.createRecord('test', {
      id: 'id',
      type: 'test',
    }) as TestModel;
    this.model.save();

    this.owner.register(
      'template:components/change-tracker-test',
      hbs`{{! @glint-nocheck: not typesafe yet }}
<div>
        {{#if @model.isDirty}}
          <div id="isDirty">isDirty</div>
        {{/if}}
        {{#if @model.hasDirtyAttributes}}
          <div id="hasDirtyAttributes">hasDirtyAttributes</div>
        {{/if}}
        {{#if @model.hasDirtyRelations}}
          <div id="hasDirtyRelations">hasDirtyRelations</div>
        {{/if}}
      </div>`,
    );
  });

  test('isDirty - hasDirtyRelations correctly works in templates (tracked)', async function (this: ChangeTrackerModelTestContext, assert) {
    await render(
      hbs`{{! @glint-nocheck }}<ChangeTrackerTest @model={{this.model}} />`,
    );

    assert.dom('#isDirty').doesNotExist();
    assert.dom('#hasDirtyAttributes').doesNotExist();
    assert.dom('#hasDirtyRelations').doesNotExist();

    this.model.someProperty = 'test';
    await settled();

    assert.dom('#isDirty').exists();
    assert.dom('#hasDirtyAttributes').exists();
    assert.dom('#hasDirtyRelations').doesNotExist();

    this.model.rollback();
    await settled();

    assert.dom('#isDirty').doesNotExist();
    assert.dom('#hasDirtyAttributes').doesNotExist();
    assert.dom('#hasDirtyRelations').doesNotExist();

    const parent1 = this.store.push({
      data: { id: 'p1', type: 'parent' },
    }) as ParentModel;
    this.model.parent = parent1;
    await settled();

    assert.dom('#isDirty').exists();
    assert.dom('#hasDirtyAttributes').doesNotExist();
    assert.dom('#hasDirtyRelations').exists();
  });
});
