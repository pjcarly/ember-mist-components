import JSONAPISerializer from '@ember-data/serializer/json-api';
import type { Snapshot } from '@ember-data/store';
import type { ModelSchema } from 'ember-data';

export default class ApplicationSerializer extends JSONAPISerializer {
  serialize(snapshot: Snapshot, options: any) {
    const serialized = super.serialize(snapshot, options);
    // @ts-ignore
    serialized.data.lid = snapshot.identifier.lid;
    return serialized;
  }

  normalize(modelClass: ModelSchema, resourceHash: any) {
    const normalized = super.normalize(modelClass, resourceHash);

    if (resourceHash.lid) {
      // @ts-ignore
      normalized.data.lid = resourceHash.lid;
    }

    return normalized;
  }
}
