import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import JSONAPISerializer from '@ember-data/serializer/json-api';
import type { ModelSchema } from 'ember-data';
import type { Snapshot } from '@ember-data/store';

/**
 * This is an extension for the JSON API serializer, where relationships marked as embedded will be serialized
 * into the response when creating/updating a record.
 *
 * When doing an update, embedded records will be unloaded from the store after the save.
 * But the response will have the embedded records included in the response.
 *
 * However, when creating a record with embedded relationships, related records will not be unloaded
 * - The problem is that we do not know the instance of the model doing the save, so we cant do the rollback
 * This must be handled by the application
 *  one way you could do this, is mark the relationship as rollback: true,
 *  this will delete unsaved records from the store when doing a transition
 */
export default class JSONAPIEmbeddedSerializer extends JSONAPISerializer.extend(
  EmbeddedRecordsMixin,
) {
  isEmbeddedRecordsMixinCompatible = true;
  attrs: any;

  serialize(snapshot: Snapshot, options: any) {
    const serialized = super.serialize(snapshot, options);

    // @ts-ignore
    serialized.data.lid = snapshot.identifier.lid;

    return serialized;
  }

  normalize(modelClass: ModelSchema, resourceHash: any) {
    const normalized = super.normalize(modelClass, resourceHash);

    if (resourceHash.lid) {
      // @ts-ignore
      normalized.data.lid = resourceHash.lid;
    }

    return normalized;
  }

  // normalizeResponse(
  //   store: Store,
  //   primaryModelClass: ModelSchema,
  //   payload: {
  //     meta?: any;
  //     data: any;
  //     included?: any;
  //   },
  //   id: string | number,
  //   requestType: string,
  // ) {
  //   const normalizedResponse = super.normalizeResponse(
  //     store,
  //     primaryModelClass,
  //     payload,
  //     id,
  //     requestType,
  //   );

  //   return normalizedResponse;
  // }
}
