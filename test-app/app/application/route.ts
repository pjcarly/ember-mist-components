import Route from '@ember/routing/route';
import type Transition from '@ember/routing/transition';
import { inject as service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { handlers } from '../mocking/handlers';
import { setupWorker } from 'msw/browser';

export const worker = setupWorker(...handlers);

export default class ApplicationRoute extends Route {
  @service declare intl: IntlService;

  async beforeModel(transition: Transition) {
    super.beforeModel(transition);
    this.intl.setLocale(['en-001']); // we set the locale to english international    debugger;

    await worker.start();
  }
}
