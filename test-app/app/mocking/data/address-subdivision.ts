export const subdivisionSelectOptionsCL = {
  data: [
    {
      id: 'CL-Antofagasta',
      type: 'address-subdivision',
      attributes: {
        locale: null,
        code: 'Antofagasta',
        'local-code': null,
        name: 'Antofagasta',
        'local-name': null,
        'iso-code': 'CL-AN',
        'postalcode-pattern': null,
        'postalcode-pattern-type': 'start',
        'has-children': true,
      },
    },
    {
      id: 'CL-Atacama',
      type: 'address-subdivision',
      attributes: {
        locale: null,
        code: 'Atacama',
        'local-code': null,
        name: 'Atacama',
        'local-name': null,
        'iso-code': 'CL-AT',
        'postalcode-pattern': null,
        'postalcode-pattern-type': 'start',
        'has-children': true,
      },
    },
  ],
};

export const subdivisionSelectOptionsCLAntofagasta = {
  data: [
    {
      id: 'CL-Antofagasta',
      type: 'address-subdivision',
      attributes: {
        locale: null,
        code: 'Antofagasta',
        'local-code': null,
        name: 'Antofagasta',
        'local-name': null,
        'iso-code': null,
        'postalcode-pattern': null,
        'postalcode-pattern-type': 'start',
        'has-children': false,
      },
    },
    {
      id: 'CL-Mejillones',
      type: 'address-subdivision',
      attributes: {
        locale: null,
        code: 'Mejillones',
        'local-code': null,
        name: 'Mejillones',
        'local-name': null,
        'iso-code': null,
        'postalcode-pattern': null,
        'postalcode-pattern-type': 'start',
        'has-children': false,
      },
    },
  ],
};

export const subdivisionSelectOptionsCLAtacama = {
  data: [
    {
      id: 'CL-Alto del Carmen',
      type: 'address-subdivision',
      attributes: {
        locale: null,
        code: 'Alto del Carmen',
        'local-code': null,
        name: 'Alto del Carmen',
        'local-name': null,
        'iso-code': null,
        'postalcode-pattern': null,
        'postalcode-pattern-type': 'start',
        'has-children': false,
      },
    },
    {
      id: 'CL-Copiap\u00f3',
      type: 'address-subdivision',
      attributes: {
        locale: null,
        code: 'Copiap\u00f3',
        'local-code': null,
        name: 'Copiap\u00f3',
        'local-name': null,
        'iso-code': null,
        'postalcode-pattern': null,
        'postalcode-pattern-type': 'start',
        'has-children': false,
      },
    },
  ],
};
