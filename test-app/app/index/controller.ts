import Controller from '@ember/controller';
import { tracked } from 'tracked-built-ins';
import { action } from '@ember/object';
import type Address from '@getflights/ember-mist-components/interfaces/address';
import { address } from '@getflights/ember-mist-components/validations/address';
import { getOwner } from '@ember/application';

const addressValidation = address();

export default class IndexController extends Controller {
  // declare model: ExampleModel;

  toggle = (key: keyof this) => {
    // @ts-ignore
    this[key] = !this[key];
  };

  @tracked showCountryCode = false;
  @tracked showAddress = false;
  @tracked showPhone = false;

  @tracked countryCode = 'BE';

  @action
  countryCodeChanged(newCountry: string) {
    this.countryCode = newCountry;
  }

  @tracked address: Address | undefined = undefined;

  @action
  addressChanged(newAddress: Address | undefined) {
    this.address = newAddress;
  }

  @tracked validationResult?: string;

  @action
  validateAddress() {
    if (this.address) {
      addressValidation
        .test(this.address, { owner: getOwner(this)! })
        .then(() => {
          this.validationResult = 'VALID';
        })
        .catch(() => {
          this.validationResult = 'INVALID';
        });
    }
  }

  @tracked phoneNumber?: string;

  phoneNumberChanged = (newPhone: string | undefined) => {
    this.phoneNumber = newPhone;
  };
}
