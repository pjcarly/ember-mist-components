import Route from '@ember/routing/route';
import { service } from '@ember/service';
import type { IntlService } from 'ember-intl';

export default class IndexRoute extends Route {
  @service declare intl: IntlService;

  beforeModel() {
    this.intl.addTranslations('en-001', {
      'label.loading': 'Loading',
    });
  }
}
