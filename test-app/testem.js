'use strict';

function getReporterConfig() {
  if (process.env.CI) {
    // We have an XUnit report file output path, so set the xunit reporter,
    // point it to the right path, and set the intermediate output option so
    // stdout will still get the default human-readable output
    return {
      reporter: 'xunit',
      report_file: 'logs/xunit.xml',
      xunit_intermediate_output: true,
    };
  } else {
    return {};
  }
}

module.exports = {
  test_page: 'tests/index.html?hidepassed',
  disable_watching: true,
  launch_in_ci: ['Chrome'],
  launch_in_dev: ['Chrome'],
  browser_start_timeout: 120,
  browser_args: {
    Chrome: {
      ci: [
        // --no-sandbox is needed when running Chrome inside a container
        process.env.CI ? '--no-sandbox' : null,
        '--headless',
        '--disable-dev-shm-usage',
        '--disable-software-rasterizer',
        '--mute-audio',
        '--remote-debugging-port=0',
        '--window-size=1440,900',
      ].filter(Boolean),
    },
  },
  ...getReporterConfig(),
};
