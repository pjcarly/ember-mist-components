import Service, { service } from '@ember/service';
import { task } from 'ember-concurrency';
import DialogService from './dialog.ts';
import HttpService from './http.ts';
import PhoneIntlService from './phone-intl.ts';
import ToastService from './toast.ts';

export default class PhoneService extends Service {
  @service declare dialog: DialogService;
  @service declare http: HttpService;
  @service declare phoneIntl: PhoneIntlService;
  @service declare toast: ToastService;

  clickToDialEnabled = false;

  clickToDial = task(async (destination: string, showDialog = true) => {
    const payload = {
      destination,
    };

    await this.phoneIntl.loadUtils.perform();
    const formatted = this.phoneIntl.formatNumber(destination);

    const confirmed = showDialog
      ? await this.dialog.confirm(`Are you sure you want to call ${formatted}?`)
      : true;

    if (confirmed) {
      await this.http
        .fetch(
          this.http.endpoint + 'crm/phone',
          'POST',
          JSON.stringify(payload),
        )
        .then(() => {
          this.toast.success('Phone should be ringing now...');
        })
        .catch(() => {
          this.toast.error(`Could not ring ${formatted}.`);
        });
    }
  });
}
