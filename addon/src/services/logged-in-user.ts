import Service from '@ember/service';
import type Store from '@ember-data/store';
import Query from '..//query/Query.ts';
import { service } from '@ember/service';
import { task } from 'ember-concurrency';
import type SessionService from 'ember-simple-auth/services/session';
import { tracked } from '@glimmer/tracking';
import type ModelRegistry from 'ember-data/types/registries/model';

export default class LoggedInUserService extends Service {
  @service declare session: SessionService;
  @service declare store: Store;

  /**
   * A reference to the logged in user model
   */
  @tracked user?: ModelRegistry['user'];

  get isAuthenticated() {
    return this.session.isAuthenticated;
  }

  /**
   * Loads the current user from the store
   * @param query Query Params where possible include query parameter will be taken from
   */
  loadCurrentUser = task({ drop: true }, async (query?: Query) => {
    let options: any = {};

    if (query) {
      options = query.queryParams;
    }

    if (this.user) {
      this.user.rollback();
    }

    await this.store
      .queryRecord('user', options)
      .then((user) => {
        this.user = user;
      })
      .catch((_exception: any) => {
        this.logOut();
      });
  });

  signOut = task({ drop: true }, async () => {
    await this.session.invalidate();
    this.user = undefined;
    this.store.unloadAll('user');
  });

  /**
   * Invalidates the session, and unsets the user
   */
  logOut() {
    this.signOut.perform();
  }
}
