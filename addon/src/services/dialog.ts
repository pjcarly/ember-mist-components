import Service from '@ember/service';
import { service } from '@ember/service';
import swal, { type SweetAlertOptions } from 'sweetalert2';
import type IntlService from 'ember-intl/services/intl';

export default class DialogService extends Service {
  @service declare intl: IntlService;

  async confirm(message?: string, title?: string): Promise<boolean> {
    return swal
      .fire({
        title: title ?? this.intl.t('label.are_you_sure'),
        text: message,
        icon: 'warning',
        confirmButtonText: this.intl.t('label.yes'),
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-link',
        },
        showCancelButton: true,
        cancelButtonText: this.intl.t('label.cancel'),
        reverseButtons: true,
        buttonsStyling: false,
        heightAuto: false,
      })
      .then((result) => {
        return result.isConfirmed;
      });
  }

  async input(
    message?: string,
    title?: string,
    swalOptions?: SweetAlertOptions,
  ): Promise<string | undefined> {
    return swal
      .fire({
        title,
        text: message,
        input: 'text',
        confirmButtonText: this.intl.t('label.submit'),
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-link',
        },
        showCancelButton: true,
        cancelButtonText: this.intl.t('label.cancel'),
        reverseButtons: true,
        buttonsStyling: false,
        heightAuto: false,
        ...swalOptions,
      })
      .then((result) => {
        if (result.isConfirmed) {
          return result.value as string;
        }

        return undefined;
      });
  }
}
