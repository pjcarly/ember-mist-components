import Service from '@ember/service';
import { service } from '@ember/service';
import { action } from '@ember/object';
import WebsocketService from './websocket.ts';

export enum NotificationMessageType {
  NOTIFICATION = 'notification',
}

export interface NotificationMessage {
  meta: {
    type: NotificationMessageType.NOTIFICATION;
  };
  data: {
    type: 'notification';
    id: string | number;
    attributes: {
      [key: string]: any;
    };
  };
}

type NotificationCallback = (message: NotificationMessage) => void;

export default abstract class NotificationService extends Service {
  @service declare websocket: WebsocketService;

  private subscriptions = new Map<string, Set<NotificationCallback>>();

  constructor(...args: any[]) {
    super(...args);

    this.websocket.addMessageSubscription(
      NotificationMessageType.NOTIFICATION,
      this.triggerNotification,
    );
  }

  public subscribe(type: string, callback: NotificationCallback) {
    if (!this.subscriptions.has(type)) {
      this.subscriptions.set(type, new Set());
    }

    this.subscriptions.get(type)?.add(callback);
  }

  public unsubscribe(type: string, callback: NotificationCallback) {
    if (this.subscriptions.has(type)) {
      this.subscriptions.get(type)?.delete(callback);
    }
  }

  @action
  public triggerNotification(message: any) {
    const notificationMessage = message as NotificationMessage;

    if (notificationMessage?.data?.type) {
      this.subscriptions
        .get(notificationMessage.data.type)
        ?.forEach((callback) => {
          callback(notificationMessage);
        });
    }
  }
}
