import type Store from '@ember-data/store';
import { action } from '@ember/object';
import Service, { service } from '@ember/service';
import { isBlank } from '@ember/utils';
import {
  add,
  compareAsc,
  differenceInMilliseconds,
  formatISO,
  parseISO,
} from 'date-fns';
import { trackedFunction } from 'reactiveweb/function';
import Query from '../query/Query.ts';
import StorageService from './storage.ts';

export interface CampaignSessionData {
  access_token: string;
  expiry: string;
}

export default class CampaignSessionService extends Service {
  @service declare storage: StorageService;
  @service declare store: Store;

  constructor(args: any) {
    super(args);

    this.storageItem = this.storage.track('campaignAuth').current;

    // Check if token has expired
    if (this.data?.expiry) {
      const expiry = parseISO(this.data.expiry);
      if (compareAsc(new Date(), expiry) === 1) {
        this.storageItem.clear();
      } else {
        this.scheduleCampaignAuthExpiry(expiry);
      }
    }
  }

  get isAuthenticated() {
    return !isBlank(this.data?.access_token);
  }

  /**
   * Campaign auth token
   */
  get data(): CampaignSessionData | undefined {
    return this.storageItem.value;
  }

  private storageItem;

  @action
  clear() {
    if (this.isAuthenticated) {
      this.storageItem.clear();

      // Refetch the campaign member
      this.campaignMemberRequest.retry();
    }
  }

  private scheduleCampaignAuthExpiry(expiry: Date) {
    const ms = differenceInMilliseconds(expiry, new Date());
    setTimeout(this.clear, ms);
  }

  @action
  setToken(access_token: string, expiresIn: number) {
    const expiry = add(new Date(), { seconds: expiresIn });
    this.storageItem.set({
      access_token,
      expiry: formatISO(expiry),
    });
    this.scheduleCampaignAuthExpiry(expiry);

    // Refetch the campaign member
    this.campaignMemberRequest.retry();
  }

  /**
   * Campaign member
   */

  campaignMemberRequest = trackedFunction(this, async () => {
    // @ts-ignore
    const query = new Query('campaign-member');
    return await query.fetchRecord(this.store);
  });

  get campaignMember() {
    return this.campaignMemberRequest.value;
  }
}
