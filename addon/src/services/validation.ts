import ValidationService from '@getflights/ember-attribute-validations/services/validation';
import { maxLength } from '@getflights/ember-attribute-validations/validations/max-length';
import { number } from '@getflights/ember-attribute-validations/validations/number';
import { address } from '../validations/address.ts';
import Model from '@ember-data/model';
import type {
  ContextualValidation,
  Validation,
} from '@getflights/ember-attribute-validations/interfaces/validation';
import type TransformRegistry from 'ember-data/types/registries/transform';

/**
 * Here we extend the Validator Service, and add Default validations for different types
 * This is specific to the mist platform logic, and should be manually chosen per application
 * By explicitly exporting this version of the ValidatorService
 * Compared to the one in the ember-attribute-validations addon, which is exported by default
 */
export default class MistValidationService extends ValidationService {
  validationsForTransformType<M extends Model, F extends keyof M>(
    transformType: keyof TransformRegistry,
  ): (Validation | ContextualValidation<M, F>)[] {
    const extraValidations: (Validation | ContextualValidation<M, F>)[] = [];

    switch (transformType) {
      case 'string':
        extraValidations.push(maxLength(255));
        break;
      case 'number':
        extraValidations.push(number());
        break;
      case 'address':
        extraValidations.push(address());
        break;
    }

    return extraValidations;
  }
}
