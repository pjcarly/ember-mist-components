import Service from '@ember/service';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import { service } from '@ember/service';
import { task } from 'ember-concurrency';
import { getOwner } from '@ember/application';
import { isBlank } from '@ember/utils';
import type HttpService from './http.ts';
import type StorageService from '../services/storage.ts';
import { tracked } from 'tracked-built-ins';
import { trackedFunction } from 'reactiveweb/function';
import type Store from '@ember-data/store';

export default class AddressService extends Service {
  @service declare http: HttpService;
  @service declare storage: StorageService;
  @service declare store: Store;

  config: any;
  shouldCache: boolean;

  constructor(properties: any) {
    super(properties);

    // @ts-ignore
    this.config = getOwner(this).resolveRegistration('config:environment');

    if (
      'ember-mist-components' in this.config &&
      'cacheFields' in this.config['ember-mist-components']
    ) {
      this.shouldCache = !!this.config['ember-mist-components'].cacheFields;
    } else {
      this.shouldCache = false;
    }
  }

  //#region Countries
  /**
   * Default country for all inputs throughout the application
   */
  @tracked defaultCountry?: string;

  countrySelectOptionsRequest = trackedFunction(this, async () => {
    let selectOptions = this.shouldCache
      ? (this.storage.retrieve('addressCountrySelectOptions') as
          | SelectOption[]
          | undefined)
      : undefined;

    if (!selectOptions) {
      await this.http
        .fetch(`${this.http.endpoint}address/countries/selectoptions`)
        .then(async (response) => {
          selectOptions = await response.json();
        })
        .catch((error: any) => {
          console.log(error);
        })
        .finally(() => {
          if (selectOptions && this.shouldCache) {
            this.storage.persist('addressCountrySelectOptions', selectOptions);
          }
        });
    }

    return selectOptions;
  });

  get countrySelectOptions() {
    return this.countrySelectOptionsRequest.value;
  }
  //#endregion

  subdivisionSelectOptions: Map<string, SelectOption[]> = new Map();

  /**
   * Returns the select options for a subdivision.
   * @param parentGrouping The grouping you want the subdivision for. This can be the Country code, the city code, the state code, ... You can find this in the format
   */
  getSubdivisionSelectOptions = task(
    { enqueue: true },
    async (parentGrouping: string) => {
      const cacheKey =
        'addressSubdivisionSelectOptions' + parentGrouping.replaceAll(',', '');

      // We might have already fetched this, so lets check here first
      if (this.subdivisionSelectOptions.has(cacheKey)) {
        return this.subdivisionSelectOptions.get(cacheKey);
      }

      // Maybe in the local storage
      let selectoptions: SelectOption[] = [];
      if (this.shouldCache) {
        selectoptions = this.storage.retrieve(cacheKey);

        if (selectoptions) {
          this.subdivisionSelectOptions.set(cacheKey, selectoptions);
          return selectoptions;
        } else {
          selectoptions = [];
        }
      }

      await this.http
        .fetch(`${this.http.endpoint}address/subdivisions/${parentGrouping}`)
        .then((response) => {
          return response.json().then((body) => {
            if (!isBlank(body) && !isBlank(body.data)) {
              for (const subdivision of body.data) {
                const selectoption: SelectOption = {
                  value: subdivision.attributes['code'],
                  label: subdivision.attributes['name'],
                };

                if (!isBlank(subdivision.attributes['local-name'])) {
                  selectoption.label += ` (${subdivision.attributes['local-name']})`;
                }

                selectoptions.push(selectoption);
              }
            }
          });
        })
        .catch((error: any) => {
          console.log(error);
        });

      this.subdivisionSelectOptions.set(cacheKey, selectoptions);

      if (this.shouldCache) {
        this.storage.persist(cacheKey, selectoptions);
      }

      return selectoptions;
    },
  );

  async loadAddressFormat(countryCode: string) {
    let format = this.store.peekRecord('address-format', countryCode);

    if (format === null) {
      format = await this.store.findRecord('address-format', countryCode);
    }

    return format;
  }
}
