import type ApplicationInstance from '@ember/application/instance';
import { action } from '@ember/object';
import { runTask } from 'ember-lifeline';
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';

export enum BaseWebsocketEvent {
  OPEN = 'open',
  MESSAGE = 'message',
  ERROR = 'error',
  CLOSE = 'close',
}

export interface WebsocketServiceArguments {
  url: string;
  autoReconnect?: boolean;
}

type BaseWebsocketEventCallback<K extends BaseWebsocketEvent> = (
  event: WebSocketEventMap[K],
) => any;

/**
 * Websocket Service, this service is not meant to be used as-is
 * Extend from this service and call super(owner, { url: websocketUrl }).
 */
export default abstract class BaseWebsocketService extends Service {
  readonly url: string;
  socketRef!: WebSocket;

  suspendConnectionWhenIdle = false;
  autoReconnect = false;

  constructor(owner: ApplicationInstance, args: WebsocketServiceArguments) {
    super(owner);

    document.addEventListener('visibilitychange', this.inactivePageChanged);

    this.url = args.url;
  }

  @tracked reconnectAttempts = 0;
  private retryConnection() {
    this.reconnectAttempts++;
    const exponent = Math.min(this.reconnectAttempts, 6);
    const waitFor = Math.pow(2, exponent) * 1000;

    runTask(this, this.openConnection, waitFor);
  }

  @action
  public openConnection() {
    if (!this.socketRef || this.socketRef.readyState !== this.socketRef.OPEN) {
      this.connectionSuspended = false;
      this.manuallyClosed = false;

      // Create websocket
      try {
        this.socketRef = new WebSocket(this.url);

        this.addEventListeners();
      } catch (error) {
        if (this.autoReconnect) {
          this.retryConnection();
        }
      }
    }
  }

  private addEventListeners() {
    this.socketRef.addEventListener('open', this.onOpen);
    this.socketRef.addEventListener('message', this.onMessage);
    this.socketRef.addEventListener('error', this.onError);
    this.socketRef.addEventListener('close', this.onClose);
  }

  private removeEventListeners() {
    this.socketRef.removeEventListener('open', this.onOpen);
    this.socketRef.removeEventListener('message', this.onMessage);
    this.socketRef.removeEventListener('error', this.onError);
    this.socketRef.removeEventListener('close', this.onClose);
  }

  @tracked private manuallyClosed = false;
  @action
  public closeConnection() {
    this.manuallyClosed = true;
    this.socketRef.close();
  }

  @tracked private connectionSuspended = false;
  @action
  private suspendConnection() {
    this.connectionSuspended = true;
    this.socketRef.close();
  }

  //#region WEBSOCKET EVENT SUBSCRIPTIONS
  private websocketSubscriptions = new Map<
    BaseWebsocketEvent,
    Set<BaseWebsocketEventCallback<any>>
  >();

  @action
  addWebsocketSubscription<K extends BaseWebsocketEvent>(
    event: K,
    callback: BaseWebsocketEventCallback<K>,
  ) {
    if (!this.websocketSubscriptions.has(event)) {
      this.websocketSubscriptions.set(event, new Set());
    }

    this.websocketSubscriptions.get(event)!.add(callback);
  }

  @action
  removeWebsocketSubscription<K extends BaseWebsocketEvent>(
    event: K,
    callback: BaseWebsocketEventCallback<K>,
  ) {
    if (this.websocketSubscriptions.has(event)) {
      this.websocketSubscriptions.get(event)!.delete(callback);
    }
  }

  @action
  private emitWebsocketEvent(wse: BaseWebsocketEvent, event: Event) {
    this.websocketSubscriptions.get(wse)?.forEach((subscription) => {
      subscription(event);
    });
  }
  //#endregion

  //#region WEBSOCKET EVENTS
  /**
   * Fired when a connection with a WebSocket is opened.
   */
  @action
  private onOpen(ev: Event) {
    this.reconnectAttempts = 0;
    this.emitWebsocketEvent(BaseWebsocketEvent.OPEN, ev);
  }

  /**
   * Fired when data is received through a WebSocket.
   */
  @action
  private onMessage(ev: Event) {
    this.emitWebsocketEvent(BaseWebsocketEvent.MESSAGE, ev);
  }

  /**
   * Fired when a connection with a WebSocket has been closed because of an error, such as when some data couldn't be sent.
   */
  @action
  private onError(ev: Event) {
    this.removeEventListeners();

    // Reconnect
    if (this.autoReconnect) {
      this.retryConnection();
    }

    this.emitWebsocketEvent(BaseWebsocketEvent.ERROR, ev);
  }

  /**
   * Fired when a connection with a WebSocket is closed.
   */
  @action
  private onClose(ev: Event) {
    this.removeEventListeners();

    // Reconnect if allowed and not manually closed OR suspended
    if (
      this.autoReconnect &&
      !this.manuallyClosed &&
      !this.connectionSuspended
    ) {
      this.retryConnection();
    }

    this.emitWebsocketEvent(BaseWebsocketEvent.CLOSE, ev);
  }
  //#endregion

  @action
  sendMessage(message: object) {
    if (this.socketRef && this.socketRef.readyState === this.socketRef.OPEN) {
      this.socketRef.send(JSON.stringify(message));
    } else {
      debug('Cannot send message while websocket is not OPEN');
    }
  }

  //#region AUTOMATIC SUSPENSION
  @action
  private inactivePageChanged() {
    if (!this.socketRef) {
      return;
    }

    if (this.suspendConnectionWhenIdle) {
      if (document.visibilityState === 'hidden') {
        this.suspendConnection();
      } else {
        this.openConnection();
      }
    } else {
      // This event listener is not needed
      document.removeEventListener(
        'visibilitychange',
        this.inactivePageChanged,
      );
    }
  }
  //#endregion
}
