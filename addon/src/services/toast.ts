import Service from '@ember/service';
import ToastMessage, { MessageType } from '../objects/toast-message.ts';
import { TrackedSet } from 'tracked-built-ins';

export default class ToastService extends Service {
  private _toasts = new TrackedSet<ToastMessage>();

  // Makes the property getter only
  get toasts() {
    return this._toasts;
  }

  success(subject: string, message?: string) {
    const toast = new ToastMessage(MessageType.SUCCESS, subject, message);
    this.addToast(toast);
  }

  info(subject: string, message?: string) {
    const toast = new ToastMessage(MessageType.INFO, subject, message);
    this.addToast(toast);
  }

  error(subject: string, message?: string) {
    const toast = new ToastMessage(MessageType.ERROR, subject, message);
    this.addToast(toast);
  }

  warning(subject: string, message?: string) {
    const toast = new ToastMessage(MessageType.WARNING, subject, message);
    this.addToast(toast);
  }

  private addToast(toast: ToastMessage) {
    this.toasts.add(toast);
    setTimeout(() => this.toasts.delete(toast), 5000);
  }
}
