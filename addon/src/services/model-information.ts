import Service from '@ember/service';
import type Store from '@ember-data/store';
import Model from '@ember-data/model';
import { service } from '@ember/service';
import { isBlank } from '@ember/utils';
import { assert } from '@ember/debug';
import type ModelRegistry from 'ember-data/types/registries/model';
import type { IntlService } from 'ember-intl';
import { capitalize } from '@ember/string';
import type { FieldOf } from '@getflights/ember-field-components';
import type FieldComponentsService from '@getflights/ember-field-components/services/field-components';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';

export default class ModelInformationService extends Service {
  @service declare fieldComponents: FieldComponentsService;
  @service declare intl: IntlService;
  @service declare store: Store;

  //#region Model names, classes...
  /**
   * Returns the dasherized name of the model class
   * @param model The model you want the dasherized name for
   */
  getModelName(model: Model): keyof ModelRegistry | undefined {
    assert('No model provided for getModelName', !isBlank(model));
    // @ts-ignore
    return model.constructor.modelName;
  }

  /**
   * Returns the model class looked up from the ember-data store.
   * @param modelName The dasherized string name of your model
   */
  getModelClass = (modelName: keyof ModelRegistry) => {
    return this.store.modelFor(modelName);
  };
  //#endregion

  //#region Attributes
  /**
   * This function returns the type of a field, nested lookups are possible.
   * @param modelName The name of the model where the field exists
   * @param field The name of the field
   */
  getTransformType = (
    modelName: keyof ModelRegistry,
    field: string,
  ): string | undefined => {
    const modelClass = this.getModelClass(modelName);
    const splittedField = field.split('.');

    if (splittedField[0] === 'id') {
      return 'string';
    }

    const meta = modelClass.metaForProperty(splittedField[0]);

    if (!meta) {
      assert(
        `No field type found for field ${field} on ${modelName}, attribute or relationship not defined on model?`,
      );
      return;
    } else if (meta.isAttribute) {
      return meta.type;
    } else if (meta.isRelationship) {
      return meta.kind;
    }

    // else: not an attribute or relationship
  };

  getFieldOptionsByModelname = <
    K extends keyof ModelRegistry,
    M extends ModelRegistry[K],
    F extends FieldOf<M>,
  >(
    modelName: K,
    field: F,
  ): FieldDecoratorOptions<M, F> | undefined => {
    const someInstance = this.store.createRecord(modelName);

    if (someInstance) {
      const fieldOptions = this.fieldComponents.getFieldOptions(
        someInstance,
        field,
      );
      this.store.unloadRecord(someInstance);
      return fieldOptions;
    }

    return undefined;
  };
  //#endregion

  //#region Relationships
  /**
   * Returns an array of default includes defined on the provided modelclass
   * @param modelName The name of the model you want the default includes for
   */
  getDefaultIncludes = (modelName: keyof ModelRegistry): string[] => {
    const modelClass = this.getModelClass(modelName);

    if ('settings' in modelClass && 'defaultIncludes' in modelClass.settings) {
      return modelClass.settings.defaultIncludes;
    }

    return [];
  };

  /**
   * Looks up the belongsto relationship and returns the modelname for the relationship. This is only for non-polymorphic relationships
   * @param modelName the name of the model where the relationship exists
   * @param relationshipName the name of the relationship on the model
   */
  getBelongsToModelName(
    modelName: keyof ModelRegistry,
    relationshipName: string,
  ): string | undefined {
    const parent = this.getModelClass(modelName);
    const relationships = parent.relationshipsByName;

    if (
      relationships.has(relationshipName) &&
      relationships.get(relationshipName).kind === 'belongsTo'
    ) {
      const relationship = relationships.get(relationshipName);
      assert(
        `Relationship ${relationshipName} for ${modelName} is a polymorphic relationship, use getBelongsToModelNames function instead`,
        !relationship.options.polymorphic,
      );

      return relationship.type;
    }

    return;
  }

  /**
   * Looks up a hasMany relationship, and returns the modelname for the relationship
   * @param modelName the name of the model where the hasmany is defined
   * @param relationshipName the name of the relationship
   */
  getHasManyModelName = (
    modelName: keyof ModelRegistry,
    relationshipName: string,
  ): keyof ModelRegistry | undefined => {
    const parent = this.getModelClass(modelName);
    const relationships = parent.relationshipsByName;

    if (
      relationships.has(relationshipName) &&
      relationships.get(relationshipName).kind === 'hasMany'
    ) {
      return relationships.get(relationshipName).type;
    }

    return;
  };

  /**
   * Returns the inverse relationshipname for the passed modelname and relationshipname
   * @param modelName The name of the model where the relationship is defined
   * @param relationshipName The relationship you want to lookup the inverse relationshpname for
   */
  getInverseRelationshipName = (
    modelName: keyof ModelRegistry,
    relationshipName: string,
  ): string | undefined => {
    const parent = this.getModelClass(modelName);
    const relationships = parent.relationshipsByName;

    if (relationships.has(relationshipName)) {
      const relationship = relationships.get(relationshipName);
      assert(
        `No explicit inverse relationship defined for ${relationshipName} on ${modelName}`,
        !isBlank(relationship.options.inverse),
      );

      return relationship.options.inverse;
    }

    return;
  };

  /**
   * Check what type a relationship is
   * @param modelName The model you are check the relationship type on
   * @param relationshipName THe name of the relationship to check
   */
  getRelationshipType(
    modelName: keyof ModelRegistry,
    relationshipName: string,
  ): 'hasMany' | 'belongsTo' {
    const model = this.getModelClass(modelName);
    const relationships = model.relationshipsByName;

    return relationships.get(relationshipName).kind;
  }
  //#endregion

  //#region intl
  /**
   * Looks up the translations for the singular of a modelname, in case nothing was found, the modelName will be returned again
   * @param modelName THe model name you want the translated plural form of
   */
  getTranslatedSingular(modelName: string): string {
    if (this.intl.exists(`ember-field-components.${modelName}.singular`)) {
      return this.intl.t(`ember-field-components.${modelName}.singular`);
    }

    return modelName;
  }

  /**
   * Looks up the translations for the plural of a modelname, in case nothing was found, the modelName will be returned again
   * @param modelName THe model name you want the translated plural form of
   */
  getTranslatedPlural = (modelName: keyof ModelRegistry) => {
    if (this.intl.exists(`ember-field-components.${modelName}.plural`)) {
      return this.intl.t(`ember-field-components.${modelName}.plural`);
    }

    return modelName;
  };

  /**
   * You can lookup a translated selectOptionLabel through this function
   * @param modelName Name of the model where the field exists
   * @param field Name of the field
   * @param value The value of the SelectOption
   */
  getTranslatedSelectOptionLabel(
    modelName: keyof ModelRegistry,
    field: string,
    value: string,
  ): string {
    if (
      this.intl.exists(
        `ember-field-components.${modelName}.select.${field}.${value}`,
      )
    ) {
      return this.intl.t(
        `ember-field-components.${modelName}.select.${field}.${value}`,
      );
    } else if (
      this.intl.exists(`ember-field-components.global.select.${field}.${value}`)
    ) {
      return this.intl.t(
        `ember-field-components.global.select.${field}.${value}`,
      );
    } else {
      return capitalize(value);
    }
  }

  /**
   * You can lookup the none label translation for a specific field
   * @param modelName Name of the model where the field exists
   * @param field Name of the field
   */
  getTranslatedSelectNoneLabel(modelName: string, field: string) {
    if (
      this.intl.exists(
        `ember-field-components.${modelName}.selectNone.${field}`,
      )
    ) {
      return this.intl.t(
        `ember-field-components.${modelName}.selectNone.${field}`,
      );
    } else if (
      this.intl.exists(`ember-field-components.global.selectNone.${field}`)
    ) {
      return this.intl.t(`ember-field-components.global.selectNone.${field}`);
    }
  }
  //#endregion
}
