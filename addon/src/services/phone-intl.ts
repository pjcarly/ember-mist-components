import { getOwner } from '@ember/application';
import Service from '@ember/service';
import { cached } from '@glimmer/tracking';
import { task } from 'ember-concurrency';

export default class PhoneIntlService extends Service {
  @cached
  get config(): any {
    // @ts-ignore
    return getOwner(this).resolveRegistration('config:environment');
  }

  loadUtils = task(async () => {
    // @ts-ignore
    await import('intl-tel-input/build/js/utils.js');
    return intlTelInputUtils;
  });

  /**
   * Validate an international phone number
   * @param number Number in international format
   */
  async isValidNumber(number: string) {
    const utils = await this.loadUtils.perform();

    if (!utils) {
      throw new Error('Could not find phone validation utils.');
    }

    // @ts-ignore
    return utils.isValidNumber(number, undefined);
  }

  /**
   * Format an international number according to local formatting rules
   * @param number Number in international format
   */
  formatNumber(
    number?: string,
    numberFormat: intlTelInputUtils.numberFormat = intlTelInputUtils
      .numberFormat.INTERNATIONAL,
  ): string | undefined {
    if (this.loadUtils.isRunning) {
      return number;
    } else if (number) {
      return (
        // @ts-ignore
        intlTelInputUtils?.formatNumber(
          number,
          // @ts-ignore
          undefined,
          numberFormat,
        ) ?? number
      );
    }

    return;
  }
}
