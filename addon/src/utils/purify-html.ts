import DOMPurify from 'dompurify';

const purifyHtml = (value: string) => {
  if (!DOMPurify.isSupported) {
    throw new Error('Cannot purify HTML: browser not supported.');
  }

  const sanitized = DOMPurify.sanitize(value);
  return sanitized;
};

export default purifyHtml;
