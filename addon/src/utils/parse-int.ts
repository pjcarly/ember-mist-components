export const parseIntOrDefault = (
  numberStr: string | number | undefined,
  defaultNumber = 0,
) => {
  // @ts-ignore
  const result = parseInt(numberStr);
  if (!numberStr || isNaN(result)) {
    return defaultNumber;
  }
  return result;
};
