import type Address from '../interfaces/address';

export const isAddress = (maybeAddress: any): maybeAddress is Address => {
  return typeof maybeAddress === 'object' && 'countryCode' in maybeAddress;
};

export const copyAddress = (address: Address): Readonly<Address> => {
  return Object.freeze({
    ...address,
  });
};
