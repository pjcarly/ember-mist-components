export type NestedKey<O> = `${keyof O & string}.${string}`;

export default function getNested<O extends object>(
  obj: O,
  propertyKey: keyof O | NestedKey<O>,
) {
  if (typeof propertyKey === 'symbol') {
    return obj[propertyKey];
  }

  const keys = String(propertyKey).split('.');

  return keys.reduce(function (value, key) {
    if (obj === undefined || obj === null) {
      return undefined;
    }

    return value[key];
  }, obj as any);
}
