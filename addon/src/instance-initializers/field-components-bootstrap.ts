import type ApplicationInstance from '@ember/application/instance';
import type FieldComponentsService from '@getflights/ember-field-components/services/field-components';
import InputFieldBootstrapLayoutDefault from '../components/input-field/-layouts/bootstrap/default.gts';
import InputFieldBootstrapLayoutCheckbox from '../components/input-field/-layouts/bootstrap/checkbox.gts';
import InputFieldBootstrapLayoutSwitch from '../components/input-field/-layouts/bootstrap/switch.gts';
import OutputFieldBootstrapLayoutCheckbox from '../components/output-field/-layouts/bootstrap/checkbox.gts';
import OutputFieldBootstrapLayoutSwitch from '../components/output-field/-layouts/bootstrap/switch.gts';

export function initialize(application: ApplicationInstance) {
  // This will lookup and instantiate the websocket service
  const fieldComponentsService = application.lookup(
    'service:field-components',
  ) as FieldComponentsService;

  /**
   * Normally you would run this setup in the instance-initializer, but since we don't want to mess with the tests, we won't.
   */
  fieldComponentsService.setDefaultInputFieldLayout(
    InputFieldBootstrapLayoutDefault,
  );
  fieldComponentsService.registerInputFieldLayout(
    'checkbox',
    InputFieldBootstrapLayoutCheckbox,
  );
  fieldComponentsService.registerInputFieldLayout(
    'switch',
    InputFieldBootstrapLayoutSwitch,
  );

  fieldComponentsService.registerOutputFieldLayout(
    'checkbox',
    OutputFieldBootstrapLayoutCheckbox,
  );
  fieldComponentsService.registerOutputFieldLayout(
    'switch',
    OutputFieldBootstrapLayoutSwitch,
  );
}

export default {
  name: 'field-components-bootstrap',
  initialize,
};
