import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldFileComponent from '../components/input-field/file.gts';
import OutputFieldFileComponent from '../components/output-field/file.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';

type FieldFileOptions = BaseInputFieldOptions & {
  endpoint?: string;
  entity?: string;
};

const file = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldFileOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
    inputOptions: {
      ...baseInputFieldOptions,
      endpoint: options.endpoint,
      entity: options.entity,
    },
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'file',
    // @ts-ignore
    inputField: InputFieldFileComponent,
    inputFieldOptions,
    outputField: OutputFieldFileComponent,
    outputFieldOptions,
  };
};

export default file;
