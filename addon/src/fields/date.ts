import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { InputDateOptions } from '../components/gf-input/date';
import InputFieldDateComponent from '../components/input-field/date.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldDateComponent from '@getflights/ember-field-components/components/output-field/date';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

type FieldDateOptions = BaseInputFieldOptions & {
  inputOptions?: InputDateOptions<false>;
};

const date = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldDateOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'date',
    inputField: InputFieldDateComponent,
    inputFieldOptions,
    outputField: OutputFieldDateComponent,
    outputFieldOptions,
  };
};

export default date;
