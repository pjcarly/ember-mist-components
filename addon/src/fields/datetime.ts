import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import InputFieldDatetimeComponent from '../components/input-field/datetime.gts';
import type { InputDateOptions } from '../components/gf-input/date.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldDatetimeComponent from '@getflights/ember-field-components/components/output-field/datetime';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

type FieldDatetimeOptions = BaseInputFieldOptions & {
  inputOptions?: Omit<InputDateOptions<false>, 'enableTime'>;
};

const datetime = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldDatetimeOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseOutputFieldOptions.outputOptions,
      displayTime: true,
    },
  };

  return {
    type: 'datetime',
    inputField: InputFieldDatetimeComponent,
    inputFieldOptions,
    outputField: OutputFieldDatetimeComponent,
    outputFieldOptions,
  };
};

export default datetime;
