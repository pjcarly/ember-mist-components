import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldAutonumberComponent from '../components/input-field/autonumber.gts';
import OutputFieldAutonumberComponent from '../components/output-field/autonumber.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';

type FieldAutonumberOptions = BaseInputFieldOptions & {
  prefixPattern: string;
  minLength?: number;
};

const autonumber = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldAutonumberOptions,
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseOutputFieldOptions.outputOptions,
      prefixPattern: options.prefixPattern,
      minLength: options.minLength,
    },
  };

  return {
    type: 'autonumber',
    inputField: InputFieldAutonumberComponent,
    inputFieldOptions,
    // @ts-ignore
    outputField: OutputFieldAutonumberComponent,
    outputFieldOptions,
  };
};

export default autonumber;
