import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldCountryCodeComponent from '../components/input-field/country-code.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldCountryCodeComponent from '../components/output-field/country-code.gts';

type FieldCountryCodeOptions = BaseInputFieldOptions;

const countryCode = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldCountryCodeOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'country-code',
    inputField: InputFieldCountryCodeComponent,
    inputFieldOptions,
    outputField: OutputFieldCountryCodeComponent,
    outputFieldOptions,
  };
};

export default countryCode;
