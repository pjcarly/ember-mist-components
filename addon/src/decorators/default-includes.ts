export default function defaultIncludes(...includes: string[]): ClassDecorator {
  return (target: any) => {
    if (!('settings' in target)) {
      target['settings'] = {};
    }

    target['settings']['defaultIncludes'] = includes;
  };
}
