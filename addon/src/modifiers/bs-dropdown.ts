import { registerDestructor } from '@ember/destroyable';
import {
  autoUpdate,
  computePosition,
  flip,
  offset,
  shift,
} from '@floating-ui/dom';
import Modifier from 'ember-modifier';
import { tracked } from 'tracked-built-ins';

export default class BsDropdownModifier extends Modifier<{
  Element: HTMLAnchorElement | HTMLButtonElement;
}> {
  referenceElement?: HTMLAnchorElement | HTMLButtonElement;
  parentElement?: HTMLElement;
  dropdownMenu?: HTMLElement | null | undefined;
  autoUpdateCleanup?: () => void;

  constructor(owner: any, args: any) {
    super(owner, args);
    registerDestructor(this, this.cleanup);
  }

  modify(element: HTMLAnchorElement | HTMLButtonElement) {
    // Cleanup old code
    this.cleanup();

    this.referenceElement = element;
    this.parentElement = this.referenceElement.parentElement!;
    this.dropdownMenu =
      this.parentElement?.querySelector<HTMLElement>('.dropdown-menu');

    if (!this.dropdownMenu) {
      return;
    }
    /**
     * Events
     */
    this.referenceElement.addEventListener('click', this.onReferenceClick);
    document.addEventListener('click', this.onDocumentClick);
  }

  onDocumentClick = (e: MouseEvent) => {
    if (!this.floatingElementVisible) {
      return;
    }

    // Close when the click is not the reference element
    if (e.target === this.referenceElement) {
      return;
    }

    this.toggleFloatingElement(false);
  };

  onReferenceClick = () => {
    this.toggleFloatingElement();
  };

  cleanup = () => {
    this.referenceElement?.removeEventListener('click', this.onReferenceClick);
    document.removeEventListener('click', this.onDocumentClick);
    this.autoUpdateCleanup?.();
  };

  @tracked floatingElementVisible = false;

  toggleFloatingElement = (
    newState: boolean = !this.floatingElementVisible,
  ) => {
    if (newState === this.floatingElementVisible) {
      // Already visible or hidden
      return;
    }

    // @ts-ignore disabled does exist
    if (this.referenceElement.disabled) {
      if (!this.floatingElementVisible) {
        return;
      } else {
        // Always force it to hide when it is disabled
        newState = false;
      }
    }

    this.floatingElementVisible = newState;

    if (this.floatingElementVisible) {
      this.parentElement!.classList.add('show');
      this.dropdownMenu!.classList.add('show');

      this.autoUpdateCleanup = autoUpdate(
        this.referenceElement!,
        this.dropdownMenu!,
        this.updatePosition,
      );
    } else {
      this.parentElement!.classList.remove('show');
      this.dropdownMenu!.classList.remove('show');

      this.autoUpdateCleanup?.();
      this.autoUpdateCleanup = undefined;
    }
  };

  private updatePosition = () => {
    if (!this.floatingElementVisible) {
      return;
    }

    if (!this.referenceElement || !this.dropdownMenu) {
      // Error: invalid floating ui component: no reference element found
      return;
    }

    const middleware = [flip(), shift(), offset(5)];

    computePosition(this.referenceElement, this.dropdownMenu, {
      placement: 'bottom-start',
      middleware,
    }).then(({ x, y }) => {
      Object.assign(this.dropdownMenu!.style, {
        left: `${x}px`,
        top: `${y}px`,
      });
    });
  };
}
