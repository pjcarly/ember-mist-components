import { inject as service } from '@ember/service';
import Modifier from 'ember-modifier';
import { registerDestructor } from '@ember/destroyable';
import type RouterService from '@ember/routing/router-service';
import type Owner from '@ember/owner';

export interface OnRouteWillChangeSignature {
  Element: Element;
  Args: {
    Positional: [() => void];
  };
}

export default class OnRouteWillChangeModifier extends Modifier<OnRouteWillChangeSignature> {
  @service declare router: RouterService;

  constructor(owner: Owner, args: any) {
    super(owner, args);
    registerDestructor(this, () => {
      if (this.callback) {
        this.router.off('routeWillChange', this.callback);
      }
    });
  }

  callback?: () => void;

  modify(
    _element: OnRouteWillChangeSignature['Element'],
    [callback]: OnRouteWillChangeSignature['Args']['Positional'],
  ) {
    this.callback = callback;
    this.router.on('routeWillChange', callback);
  }
}
