import {
  add,
  eachDayOfInterval,
  eachWeekOfInterval,
  endOfISOWeek,
  endOfMonth,
  getISOWeek,
  getISOWeeksInYear,
  getISOWeekYear,
  getYear,
  isSameISOWeek,
  isSameMonth,
  setISOWeek,
  setISOWeekYear,
  setMonth,
  setYear,
  startOfISOWeek,
  startOfMonth,
  sub,
} from 'date-fns';
import { tracked } from 'tracked-built-ins';

export interface CalendarOptions {
  onCenterChange?: ((newCenter: Date) => void) | undefined;
  view: CalendarView;
}

export type CalendarView = 'month' | 'week';

const defaultOptions: CalendarOptions = {
  view: 'month',
};

export default class Calendar {
  constructor(
    center: Date = new Date(),
    options: Partial<CalendarOptions> = {},
  ) {
    this._options = tracked(Object.assign({}, defaultOptions, options));

    this._center = center;
  }

  @tracked private _options: CalendarOptions;

  get options(): CalendarOptions {
    return this._options;
  }

  set options(options: Partial<CalendarOptions>) {
    Object.assign(this._options, options);
  }

  /**
   * Get the current view (week or month)
   */
  get view() {
    return this.options.view;
  }

  /**
   * Set the current view (week or month)
   */
  set view(view: CalendarView) {
    const todayWasVisible = this.todayVisible;

    this.options.view = view;

    // Make sure today stays visible, if it was visible before
    if (!this.todayVisible && todayWasVisible) {
      this.center = new Date();
    }
  }

  //#region Selecting the current date range (adjusting the center)
  /**
   * The value the calendar is centered around (current week, month... are calculated based on this value)
   */
  @tracked private _center: Date;

  /**
   * Get the current center
   */
  get center() {
    return this._center;
  }

  /**
   * Set the current center
   */
  set center(newCenter: Date) {
    this._center = newCenter;
    this.options.onCenterChange?.(newCenter);
  }

  /**
   * Subtract 1 week from the current center
   */
  previousWeek = () => {
    this.center = startOfISOWeek(sub(this.center, { weeks: 1 }));
  };

  /**
   * Get the current month (index)
   */
  get week() {
    return getISOWeek(this.center);
  }

  /**
   * Add 1 week to the current center
   */
  nextWeek = () => {
    this.center = startOfISOWeek(add(this.center, { weeks: 1 }));
  };

  /**
   * Set the current week to a different (ISO) week
   */
  set week(week: number) {
    this.center = startOfISOWeek(setISOWeek(this.center, week));
  }

  /**
   * Subtract 1 month from the current center
   */
  previousMonth = () => {
    this.center = startOfMonth(sub(this.center, { months: 1 }));
  };

  /**
   * Get the current month (index)
   */
  get month() {
    return this.center.getMonth();
  }

  /**
   * Add 1 month to the current center
   */
  nextMonth = () => {
    this.center = startOfMonth(add(this.center, { months: 1 }));
  };

  /**
   * Set the current month to a different month
   */
  set month(month: number) {
    this.center = startOfMonth(setMonth(this.center, month));
  }

  /**
   * Subtract 1 year from the current center
   */
  previousYear = () => {
    const previousYear = sub(this.center, { years: 1 });
    if ('week' === this.view) {
      this.center = startOfISOWeek(previousYear);
    } else {
      this.center = startOfMonth(previousYear);
    }
  };

  /**
   * Get the current year
   */
  get year() {
    if ('week' === this.view) {
      return getISOWeekYear(this.center);
    } else {
      return getYear(this.center);
    }
  }
  /**
   * Add 1 year to the current center
   */
  nextYear = () => {
    const nextYear = add(this.center, { years: 1 });
    if ('week' === this.view) {
      this.center = startOfISOWeek(nextYear);
    } else {
      this.center = startOfMonth(nextYear);
    }
  };

  /**
   * Set the current year to a different year
   */
  set year(year: number) {
    if ('week' === this.view) {
      const newYear = setISOWeekYear(this.center, year);
      this.center = startOfISOWeek(newYear);
    } else {
      const newYear = setYear(this.center, year);
      this.center = startOfMonth(newYear);
    }
  }
  //#endregion

  //#region Getting the calendar
  /**
   * Get all the weeks of a certain month (the current month of the date argument)
   */
  private monthWeeks(dayInMonth: Date): Date[][] {
    return eachWeekOfInterval(
      {
        start: startOfMonth(dayInMonth),
        end: endOfMonth(dayInMonth),
      },
      {
        weekStartsOn: 1,
      },
    ).map(this.weekDays.bind(this));
  }

  /**
   * Get all the days of a certain week (the current month of the date argument)
   */
  private weekDays(dayInWeek: Date): Date[] {
    return eachDayOfInterval({
      start: startOfISOWeek(dayInWeek),
      end: endOfISOWeek(dayInWeek),
    });
  }
  //#endregion

  /**
   * Get a list of ISO week numbers of the current year
   */
  get ISOWeeks() {
    const amountOfWeeks = getISOWeeksInYear(this.center);
    return Array.from({ length: amountOfWeeks }, (_, i) => i + 1);
  }

  /**
   * Get a list of currently visible days, depending on the view (week of month)
   */
  get visibleDays() {
    if ('month' === this.view) {
      const currentMonthInWeeks = this.monthWeeks(this.center);
      return currentMonthInWeeks.flatMap((weekInDays) => weekInDays);
    } else {
      return this.weekDays(this.center);
    }
  }

  /**
   * Is today visible?
   */
  get todayVisible() {
    switch (this.view) {
      case 'week':
        return isSameISOWeek(new Date(), this.center);
      case 'month':
      default:
        return isSameMonth(new Date(), this.center);
    }
  }
}
