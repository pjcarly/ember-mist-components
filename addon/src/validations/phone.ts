import type { Validation } from '@getflights/ember-attribute-validations/interfaces/validation';
import {
  isBlankString,
  isNone,
} from '@getflights/ember-attribute-validations/-private/utils';
import { assert } from '@ember/debug';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import type PhoneIntlService from '../services/phone-intl';

export function phone(): Validation {
  return {
    name: 'phone',
    test: async (value, api) => {
      assert('Owner must be set', api?.owner);

      if (!isNone(value) && !isBlankString(value)) {
        const phoneIntlService = api.owner.lookup(
          'service:phone-intl',
        ) as PhoneIntlService;

        if (!(await phoneIntlService.isValidNumber(value))) {
          throw new ValidationFailure();
        }
      }

      return true;
    },
  };
}
