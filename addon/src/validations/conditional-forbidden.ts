import type { ContextualValidation } from '@getflights/ember-attribute-validations/interfaces/validation';
import type { ContextualBelongsToValidation } from '@getflights/ember-attribute-validations/interfaces/belongs-to-validation';
import type { NestedKey } from '../utils/get-nested';
import getNested from '../utils/get-nested.ts';
import { forbidden } from '@getflights/ember-attribute-validations/validations/forbidden';

function checkCondition<O extends object>(
  target: O,
  assertionFnOrField: AssertionFn<O> | keyof O | NestedKey<O>,
) {
  if (typeof assertionFnOrField === 'function') {
    return assertionFnOrField(target);
  } else {
    return Boolean(getNested(target, assertionFnOrField));
  }
}

export type AssertionFn<O> = (target: O) => boolean;

export function conditionalForbidden<O extends object, F extends keyof O>(
  assertionOrField: NoInfer<AssertionFn<O> | keyof O | NestedKey<O>>,
): ContextualValidation<O, F> {
  const forbiddenValidation = forbidden();

  return {
    name: 'conditional-forbidden',
    test: (value, api) => {
      if (checkCondition(api.context.target, assertionOrField)) {
        return forbiddenValidation.test(value, api);
      }

      return Promise.resolve(true);
    },
  };
}

export function conditionalForbiddenId<O extends object, F extends keyof O>(
  assertionOrField: AssertionFn<NoInfer<O>> | keyof O | NestedKey<O>,
): ContextualBelongsToValidation<O, F> {
  const conditionalForbiddenValidation = conditionalForbidden<O, F>(
    assertionOrField,
  );

  return {
    ...conditionalForbiddenValidation,
    test: (ref, ...args) => {
      return conditionalForbiddenValidation.test(ref.id(), ...args);
    },
  };
}
