import type { ContextualValidation } from '@getflights/ember-attribute-validations/interfaces/validation';
import { isNone } from '@getflights/ember-attribute-validations/-private/utils';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

import type { NestedKey } from '../utils/get-nested';
import getNested from '../utils/get-nested.ts';

export function dependent<
  O extends object,
  F extends keyof O,
  DF extends keyof O | NestedKey<O>,
>(
  dependentField: DF,
  dependencies: Map<string, string[]>,
): ContextualValidation<O, F> {
  return {
    name: 'dependent',
    test: async (value, api) => {
      if (!isNone(value)) {
        const dependentFieldValue = getNested(
          api.context.target,
          dependentField,
        ) as unknown as string;

        const possibleValues = dependencies.get(dependentFieldValue) ?? [];

        if (!possibleValues.includes(value)) {
          throw new ValidationFailure();
        }
      }

      return true;
    },
  };
}
