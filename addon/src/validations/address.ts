import type { Validation } from '@getflights/ember-attribute-validations/interfaces/validation';
import { isNone } from '@getflights/ember-attribute-validations/-private/utils';
import { isAddress } from '../utils/address.ts';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import { assert } from '@ember/debug';
import type AddressService from '../services/address.ts';

export function address(): Validation {
  return {
    name: 'address',
    test: async (value, api) => {
      assert('Owner must be set', api?.owner);

      if (!isNone(value)) {
        // Not an address
        if (!isAddress(value)) {
          throw new ValidationFailure();
        }

        const addressService = api!.owner.lookup(
          'service:address',
        ) as AddressService;

        const format = await addressService.loadAddressFormat(
          value.countryCode,
        );

        if (!format || !format.validate(value)) {
          throw new ValidationFailure();
        }
      }

      return true;
    },
  };
}
