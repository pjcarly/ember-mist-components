import type { ContextualValidation } from '@getflights/ember-attribute-validations/interfaces/validation';
import type { ContextualBelongsToValidation } from '@getflights/ember-attribute-validations/interfaces/belongs-to-validation';
import type { NestedKey } from '../utils/get-nested';
import getNested from '../utils/get-nested.ts';
import { required } from '@getflights/ember-attribute-validations/validations/required';

function checkCondition<O extends object>(
  target: O,
  assertionFnOrField: AssertionFn<O> | keyof O | NestedKey<O>,
) {
  if (typeof assertionFnOrField === 'function') {
    return assertionFnOrField(target);
  } else {
    return Boolean(getNested(target, assertionFnOrField));
  }
}

export type AssertionFn<O> = (target: O) => boolean;

export function conditionalRequired<O extends object, F extends keyof O>(
  assertionOrField: NoInfer<AssertionFn<O> | keyof O | NestedKey<O>>,
): ContextualValidation<O, F> {
  const requiredValidation = required();

  return {
    name: 'conditional-required',
    test: (value, api) => {
      if (checkCondition(api.context.target, assertionOrField)) {
        return requiredValidation.test(value, api);
      }

      return Promise.resolve(true);
    },
  };
}

export function conditionalRequiredId<O extends object, F extends keyof O>(
  assertionOrField: AssertionFn<NoInfer<O>> | keyof O | NestedKey<O>,
): ContextualBelongsToValidation<O, F> {
  const conditionalRequiredValidation = conditionalRequired<O, F>(
    assertionOrField,
  );

  return {
    ...conditionalRequiredValidation,
    test: (ref, ...args) => {
      return conditionalRequiredValidation.test(ref.id(), ...args);
    },
  };
}
