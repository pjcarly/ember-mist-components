import type { Validation } from '@getflights/ember-attribute-validations/interfaces/validation';
import {
  isBlankString,
  isNone,
} from '@getflights/ember-attribute-validations/-private/utils';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import { assert } from '@ember/debug';
import type AddressService from '../services/address.ts';

export function country(): Validation {
  return {
    name: 'country',
    test: async (value, api) => {
      assert('Owner must be set', api?.owner);

      // Early return, no unnecessary requests for country select options.
      if (!isNone(value) && !isBlankString(value)) {
        const addressService = api!.owner.lookup(
          'service:address',
        ) as AddressService;

        const selectOptions =
          await addressService.countrySelectOptionsRequest.promise;

        if (
          !selectOptions?.some((selectOption) => value === selectOption.value)
        ) {
          throw new ValidationFailure();
        }
      }

      return true;
    },
  };
}
