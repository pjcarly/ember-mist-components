import Helper from '@ember/component/helper';
import { getOwner } from '@ember/application';
import { cached } from '@glimmer/tracking';

type Positional = [
  template: string,
  id: string,
  digest: string,
  html?: boolean,
];

type Return = string | undefined;

export interface PDFURLHelperSignature {
  Args: {
    Positional: Positional;
  };
  Return: Return;
}

export default class PDFURLHelper extends Helper<PDFURLHelperSignature> {
  @cached
  get config(): any {
    // @ts-ignore
    return getOwner(this).resolveRegistration('config:environment');
  }

  compute([template, id, digest, html = false]: Positional): Return {
    if (!template || !id || !digest) {
      return;
    }

    let returnURL = `${this.config.apiEndpoint}template/generate/${template}?id=${id}&digest=${digest}`;

    if (html) {
      returnURL = `${returnURL}&html`;
    }

    return returnURL;
  }
}
