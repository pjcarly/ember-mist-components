import { guidFor } from '@ember/object/internals';
import { isBlank } from '@ember/utils';

export default function guid(value: any) {
  if (isBlank(value)) {
    return null;
  }

  return guidFor(value);
}
