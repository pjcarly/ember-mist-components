import Helper from '@ember/component/helper';
import type DrupalModel from '../models/drupal-model.ts';
import { service } from '@ember/service';
import EntityRouterService from '../services/entity-router.ts';

type Positional = [model: DrupalModel, route: string];

export interface PNRMarginColorSignature {
  Args: {
    Positional: Positional;
  };
  Return: () => void;
}

export default class ModelTransitionHelper extends Helper<PNRMarginColorSignature> {
  @service declare entityRouter: EntityRouterService;

  compute([model, route]: Positional) {
    return () => {
      this.entityRouter.transitionToModelRoute(model, route);
    };
  }
}
