import { tracked } from '@glimmer/tracking';
import {
  type Collection,
  type CollectionPageInfo,
  type SomeModel,
} from '../components/-base-collection.ts';
import Helper from '@ember/component/helper';
import { action } from '@ember/object';

type Positional<M extends SomeModel> = [collection: Collection<M>];
type Named = {
  pageSize?: number;
};
type Return<M extends SomeModel> = Collection<M>;

interface CollectionAddPaginationSignature<M extends SomeModel> {
  Args: {
    Positional: Positional<M>;
    Named: Named;
  };
  Return: Return<M>;
}

/**
 * Add pagination to an existing collection *that does not have pagination*.
 */
export default class CollectionAddPaginationHelper<
  M extends SomeModel,
> extends Helper<CollectionAddPaginationSignature<M>> {
  @tracked originalCollection!: Collection<M>;

  private get records() {
    // Slice for pagination
    return this.originalCollection.records.slice(
      this.resultRowFirst - 1,
      this.resultRowLast,
    );
  }

  //#region PAGINATION
  @tracked pageCurrent = 1;

  @action
  setPage(page: number) {
    this.pageCurrent = page;
  }

  @tracked pageSize = 25;

  @action
  setPageSize(size: number) {
    this.pageSize = size;
    // also reset page to 1
    this.pageCurrent = 1;
  }

  private get pageCount() {
    if (this.pageSize < 1) {
      return 0;
    }

    return Math.ceil(this.originalCollection.records.length / this.pageSize);
  }

  private get resultRowFirst() {
    // TODO
    return this.resultRowLast - this.pageSize + 1;
  }

  private get resultRowLast() {
    // TODO
    return this.pageSize * this.pageCurrent;
  }

  private get pagination(): CollectionPageInfo {
    return {
      count: this.records.length,
      totalCount: this.originalCollection.records.length,
      pageSize: this.pageSize,
      pageCurrent: this.pageCurrent,
      pageCount: this.pageCount,
      resultRowFirst: this.resultRowFirst,
      resultRowLast: this.resultRowLast,
      setPage: this.setPage,
      setPageSize: this.setPageSize,
    };
  }
  //#endregion

  compute([collection]: Positional<M>, { pageSize }: Named): Return<M> {
    this.originalCollection = collection;

    if (pageSize) {
      this.pageSize = pageSize;
    }

    return {
      ...this.originalCollection,
      records: this.records,
      pagination: this.pagination,
    };
  }
}
