/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable ember/no-new-mixins */
import Mixin from '@ember/object/mixin';
import type ModelRegistry from 'ember-data/types/registries/model';
import type { Snapshot } from '@ember-data/store';
import type Model from '@ember-data/model';
import type ChangeTrackerModel from '../models/change-tracker-model';

const isChangeTrackerModel = (model: Model): model is ChangeTrackerModel => {
  return 'didChange' in model;
};

export default Mixin.create({
  keepAttribute(record: Model, key: string) {
    return (
      // always keep all attributes for new records
      record.get('isNew') ||
      // only keep changed attributes
      !!record.changedAttributes()[key]
    );
  },

  serializeAttribute<K extends keyof ModelRegistry>(
    snapshot: Snapshot<K>,
    json: {},
    key: string,
    ...args: any[] // Make sure we pass on ALL arguments, in case we forgot some unused ones
  ) {
    if (this.keepAttribute(snapshot.record, key)) {
      return this._super(snapshot, json, key, ...args);
    }
  },

  keepRelationship(record: Model, key: string) {
    return (
      // always keep all relationships for new records
      record.get('isNew') ||
      !isChangeTrackerModel(record) ||
      // only keep changed relationships (for change tracker models)
      record.didChange(key)
    );
  },

  serializeBelongsTo<K extends keyof ModelRegistry>(
    snapshot: Snapshot<K>,
    json: {},
    relationship: { key: string },
    ...args: any[] // Make sure we pass on ALL arguments, in case we forgot some unused ones
  ) {
    if (this.keepRelationship(snapshot.record, relationship.key)) {
      return this._super(snapshot, json, relationship, ...args);
    }
  },

  serializeHasMany<K extends keyof ModelRegistry>(
    snapshot: Snapshot<K>,
    json: {},
    relationship: { key: string },
    ...args: any[] // Make sure we pass on ALL arguments, in case we forgot some unused ones
  ) {
    if (this.keepRelationship(snapshot.record, relationship.key)) {
      return this._super(snapshot, json, relationship, ...args);
    }
  },
});
