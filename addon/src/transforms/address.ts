import Transform from '@ember-data/serializer/transform';
import {
  camelizeObject,
  dasherizeObject,
} from '../utils/camelize-dasherize-object.ts';
import type Address from '../interfaces/address';
import type { AddressAttr } from '../interfaces/address';

export default class AddressTransform extends Transform {
  deserialize(
    serialized: Record<string, string> | null,
  ): AddressAttr | undefined {
    return serialized ? Object.freeze(camelizeObject(serialized)) : serialized;
  }

  serialize(deserialized: Address | undefined) {
    return deserialized ? dasherizeObject(deserialized) : null;
  }
}
