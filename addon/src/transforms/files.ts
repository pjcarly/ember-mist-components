import Transform from '@ember-data/serializer/transform';
import { isArray } from '@ember/array';
import { isBlank } from '@ember/utils';
import type DrupalFile from '../interfaces/drupal-file';

export default class FilesTransform extends Transform {
  deserialize(serialized: DrupalFile[] | undefined | null) {
    return serialized ? serialized : [];
  }

  serialize(deserialized: DrupalFile[] | undefined | null) {
    if (!isBlank(deserialized) && isArray(deserialized)) {
      return deserialized;
    } else {
      return [];
    }
  }
}
