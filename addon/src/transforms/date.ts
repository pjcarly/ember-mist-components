import Transform from '@ember-data/serializer/transform';
import { isBlank } from '@ember/utils';
import { format, parseISO } from 'date-fns';

export default class DateTransform extends Transform {
  deserialize(serialized: any) {
    if (isBlank(serialized)) {
      return null;
    }

    const parsed = parseISO(serialized);
    return parsed;
  }

  serialize(deserialized: Date) {
    if (isBlank(deserialized)) {
      return null;
    }

    return format(deserialized, 'yyyy-MM-dd');
  }
}
