import { isNone as none } from '@ember/utils';
import Transform from '@ember-data/serializer/transform';

/**
 * This transform exists because a normal string is limited to 255 characters (because a default validation is added to all "string" transforms)
 */
export default class RichtextTransform extends Transform {
  deserialize(serialized: any) {
    return none(serialized) ? null : String(serialized);
  }

  serialize(deserialized: any) {
    return none(deserialized) ? null : String(deserialized);
  }
}
