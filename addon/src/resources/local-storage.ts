import { resource, resourceFactory } from 'ember-resources';
import { tracked } from 'tracked-built-ins';

export interface TrackedLocalStorageItem {
  key: string;
  value: any;
  set: (newValue: any) => void;
  clear: () => void;
}

const resourceMap = new Map<string, TrackedLocalStorageItem>();

const LocalStorageResource = resourceFactory((key: string) => {
  if (resourceMap.has(key)) {
    return resourceMap.get(key)!;
  }

  const itemResource = resource(({ on }) => {
    const item = localStorage.getItem(key);
    const initialValue = item ? JSON.parse(item) : undefined;

    const setValue = (value: any) => {
      // Update value (because 'storage' event only updates changes by other pages)
      data.value = value;
      // Persist to local storage, other pages will be updated by 'storage' event
      localStorage.setItem(key, JSON.stringify(value));
    };

    const clearValue = () => {
      data.value = null;
      localStorage.removeItem(key);
    };

    const data = tracked<TrackedLocalStorageItem>({
      key,
      value: initialValue,
      set: setValue,
      clear: clearValue,
    });

    // Updated by another page
    const storageEventListener = (ev: StorageEvent) => {
      if (ev.key === key) {
        data.value = ev.newValue ? JSON.parse(ev.newValue) : undefined;
      }
    };

    // Event listeners for updates from other pages
    window.addEventListener('storage', storageEventListener);

    on.cleanup(() => {
      window.removeEventListener('storage', storageEventListener);
    });

    return data;
  });

  resourceMap.set(key, itemResource);
  return itemResource;
});

export default LocalStorageResource;
