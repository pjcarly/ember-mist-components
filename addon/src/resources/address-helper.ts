import { setOwner } from '@ember/application';
import type Owner from '@ember/owner';
import { resource, resourceFactory } from 'ember-resources';
import { tracked } from 'tracked-built-ins';
import { IGNORED_ADDRESS_FIELDS } from '../models/address-format.ts';
import { service } from '@ember/service';
import type AddressService from '../services/address.ts';
import { trackedFunction } from 'reactiveweb/function';
import type { FieldOfAddress } from '../interfaces/address';

export interface AddressDisplayColumn {
  label: string;
  field: FieldOfAddress;
  subdivision?: boolean;
  required: boolean;
}

export interface AddressDisplayRow {
  columns: AddressDisplayColumn[];
}

export class AddressHelper {
  @service('address') declare addressService: AddressService;

  constructor(owner: Owner, countryCode: string | undefined) {
    setOwner(this, owner);

    this.countryCode = countryCode;
  }

  @tracked countryCode: string | undefined;

  findFormat = trackedFunction(this, async () => {
    if (!this.countryCode) {
      return undefined;
    }

    return await this.addressService.loadAddressFormat(this.countryCode);
  });

  get addressFormat() {
    return this.findFormat?.value;
  }

  get formatError() {
    return this.countryCode && this.findFormat.isError;
  }

  get formatLoading() {
    return this.findFormat.isLoading;
  }

  get rows() {
    const rows = tracked<AddressDisplayRow>([]);

    const addressFormat = this.addressFormat;

    if (addressFormat) {
      const format = addressFormat.format;
      const rawRows = format.split('\n');

      for (const rawRow of rawRows) {
        const columns: AddressDisplayColumn[] = [];
        const rawColumns = rawRow.split('%');

        for (let rawColumn of rawColumns) {
          rawColumn = rawColumn.replace(/[^0-9a-z]/gi, ''); // we remove all none alpha numeric characters
          if (rawColumn) {
            const column = this.getDisplayColumn(rawColumn as FieldOfAddress);
            if (column) {
              columns.push(column);
            }
          }
        }

        rows.push({
          columns,
        });
      }
    }

    return rows;
  }

  getDisplayColumn(field: FieldOfAddress) {
    if (IGNORED_ADDRESS_FIELDS.includes(field)) {
      return;
    }

    const addressFormat = this.addressFormat!;

    const { subdivisionDepth, requiredFields } = addressFormat;

    const column: AddressDisplayColumn = {
      label: `${addressFormat.labels[field]}`,
      field: field,
      required: requiredFields.includes(field),
    };

    if (column.required) {
      column.label += ' *';
    }

    if (subdivisionDepth > 0) {
      const usedFields = addressFormat.usedFields;
      const positionOfField = usedFields.indexOf(field) + 1;

      if (positionOfField <= subdivisionDepth) {
        column.subdivision = true;
      }
    }

    return column;
  }
}

const AddressHelperResource = resourceFactory(
  (countryCodeFn: () => string | undefined) => {
    let countryCode: string | undefined;
    let helper: AddressHelper | undefined = undefined;

    return resource(({ owner }) => {
      const newCountryCode = countryCodeFn();

      if (!helper) {
        helper = new AddressHelper(owner, newCountryCode);
      } else {
        if (countryCode !== newCountryCode) {
          helper.countryCode = newCountryCode;
        }
      }

      countryCode = newCountryCode;
      return helper;
    });
  },
);

export default AddressHelperResource;
