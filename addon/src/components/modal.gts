import Component from '@glimmer/component';
import type { ModifierLike, WithBoundArgs } from '@glint/template';
import { tracked } from 'tracked-built-ins';
import { modifier } from 'ember-modifier';
import { concat, hash } from '@ember/helper';
import type { TOC } from '@ember/component/template-only';
import { debug } from '@ember/debug';
import { Modal } from 'bootstrap.native';

export const MODAL_ROOT_ELEMENT_ID = 'ember-mist-modal-wormhole';

export interface ModalControls {
  show: () => void;
  close: () => void;
  visible: boolean;
}

export interface ModalSignature {
  Args: {
    preventCancel?: boolean;
    onOpen?: () => void;
    onClose?: () => void;
    defaultOpen?: boolean;
  };
  Blocks: {
    default: [
      {
        Dialog: WithBoundArgs<typeof ModalDialog, 'register' | 'close'>;
      } & ModalControls,
    ];
  };
}

export default class ModalComponent extends Component<ModalSignature> {
  private modal?: Modal;

  @tracked private visible = false;

  private registerDialog = modifier((modalElement: HTMLDivElement) => {
    this.modal = new Modal(modalElement, {
      // We handle backdrop clicks ourselves
      backdrop: 'static',
      keyboard: !this.args.preventCancel,
    });
    modalElement.addEventListener('click', this.modalClickEventHandler);
    modalElement.addEventListener('shown.bs.modal', this.shownEventHandler);
    modalElement.addEventListener('hidden.bs.modal', this.hiddenEventHandler);

    if (this.args.defaultOpen) {
      this.modal.show();
    }

    return () => {
      modalElement.removeEventListener('click', this.modalClickEventHandler);
      modalElement.removeEventListener(
        'shown.bs.modal',
        this.shownEventHandler,
      );
      modalElement.removeEventListener(
        'hidden.bs.modal',
        this.hiddenEventHandler,
      );

      this.modal?.dispose();
    };
  });

  private modalClickEventHandler = (e: MouseEvent) => {
    if (!this.modal) {
      return;
    }

    if (this.args.preventCancel) {
      return;
    }

    // Close when the modal was clicked (not the dialog or any child)
    if (e.target === this.modal.element) {
      this.modal.hide();
    }
  };

  private hiddenEventHandler = () => {
    this.visible = false;
    this.args.onClose?.();
  };

  private shownEventHandler = () => {
    this.visible = true;
    this.args.onOpen?.();
  };

  private closeModal = () => {
    this.modal?.hide();
  };

  private showModal = () => {
    this.modal?.show();
  };

  <template>
    {{yield
      (hash
        Dialog=(component
          ModalDialog register=this.registerDialog close=this.closeModal
        )
        close=this.closeModal
        show=this.showModal
        visible=this.visible
      )
    }}
  </template>
}

const ModalItem: TOC<{
  Args: {
    class: string;
  };
  Element: HTMLDivElement;
  Blocks: {
    default: [];
  };
}> = <template>
  <div class={{@class}} ...attributes>
    {{yield}}
  </div>
</template>;

class ModalDialog extends Component<{
  Args: {
    register: ModifierLike<{ Element: HTMLDivElement }>;
    close: () => void;
    size?: 'fit-content' | 'sm' | 'lg' | 'xl';
    fade?: boolean;
    centered?: boolean;
  };
  Element: HTMLDivElement;
  Blocks: {
    default: [
      {
        Header: WithBoundArgs<typeof ModalItem, 'class'>;
        Body: WithBoundArgs<typeof ModalItem, 'class'>;
        Footer: WithBoundArgs<typeof ModalItem, 'class'>;
      },
    ];
  };
}> {
  constructor(owner: any, args: any) {
    super(owner, args);

    this.modalRootElement = document.querySelector(
      `#${MODAL_ROOT_ELEMENT_ID}`,
    )!;

    if (!this.modalRootElement) {
      debug(
        `Element with id #${MODAL_ROOT_ELEMENT_ID} not found. Adding one to the start of the body.`,
      );

      const newRootElement = document.createElement('div');
      newRootElement.id = MODAL_ROOT_ELEMENT_ID;
      const body = document.querySelector('body')!;
      document
        .querySelector('body')!
        .insertBefore(newRootElement, body.firstChild);
      this.modalRootElement = newRootElement;
    }
  }

  modalRootElement: HTMLElement;

  get size() {
    return this.args.size &&
      ['fit-content', 'sm', 'lg', 'xl'].includes(this.args.size)
      ? this.args.size
      : undefined;
  }

  get centered() {
    return this.args.centered ?? true;
  }

  <template>
    {{#in-element this.modalRootElement insertBefore=null}}
      <div class='modal {{if @fade " fade"}} ' tabindex='-1' {{@register}}>
        <div
          class='modal-dialog{{if @size (concat " modal-" this.size)}}{{if
              this.centered
              " modal-dialog-centered"
            }}'
          ...attributes
        >
          <div class='modal-content'>
            {{yield
              (hash
                Header=(component ModalItem class='modal-header')
                Body=(component ModalItem class='modal-body')
                Footer=(component ModalItem class='modal-footer')
              )
            }}
          </div>
        </div>
      </div>
    {{/in-element}}
  </template>
}
