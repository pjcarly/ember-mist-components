import { on } from '@ember/modifier';
import ListViewSelectComponent from './list-view-select.gts';
import LoadingComponent from './loading.gts';
import { fn } from '@ember/helper';
import t from 'ember-intl/helpers/t';
import { eq } from 'ember-truth-helpers';

export default class ListViewTabsComponent extends ListViewSelectComponent<false> {
  <template>
    {{#if this.selectOptionsLoading}}
      <LoadingComponent />
    {{else}}
      <ul class='list-view-tabs nav nav-tabs' role='tablist' ...attributes>
        <li class='{{unless this.selectedListViewId "active"}} nav-item'>
          {{! template-lint-disable require-context-role }}
          <button
            class='{{unless this.selectedListViewId "active"}} nav-link'
            role='tab'
            type='button'
            {{on 'click' (fn this.setNewListView '')}}
          >
            {{t 'label.all'}}
          </button>
        </li>
        {{#each this.selectOptions as |listView|}}
          <li
            class='{{if (eq listView.value this.selectedListViewId) "active"}}
              nav-item'
          >
            {{! template-lint-disable require-context-role }}
            <button
              class='{{if (eq listView.value this.selectedListViewId) "active"}}
                nav-link'
              role='tab'
              type='button'
              {{on 'click' (fn this.setNewListView listView.value)}}
            >
              {{listView.label}}
            </button>
          </li>
        {{/each}}
      </ul>
    {{/if}}
    {{yield this.selectedListViewId}}
  </template>
}
