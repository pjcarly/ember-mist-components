import { action } from '@ember/object';
import { service } from '@ember/service';
import ToastService from '../services/toast.ts';
import Component from '@glimmer/component';
import type IntlService from 'ember-intl/services/intl';
import { modifier } from 'ember-modifier';
import { tracked } from 'tracked-built-ins';

interface CopyTextSignature {
  Args: {
    text: string;
  };
}

export default class CopyTextComponent extends Component<CopyTextSignature> {
  @service declare toast: ToastService;
  @service declare intl: IntlService;

  get canCopy() {
    return Boolean(navigator.clipboard.writeText);
  }

  @tracked inputElement?: HTMLInputElement;

  inputModifier = modifier((element: HTMLInputElement) => {
    this.inputElement = element;
  });

  @action
  selectText() {
    if (this.inputElement) {
      if (this.inputElement.select) {
        this.inputElement.select();
      } else {
        this.inputElement.setSelectionRange(0, this.inputElement.value.length);
      }
    }
  }

  @action
  copyToClipboard() {
    navigator.clipboard.writeText(this.args.text).then(() => {
      this.toast.info(
        this.intl.t('ember-mist-components.terminal.copied'),
        this.args.text,
      );
    });
  }
}
