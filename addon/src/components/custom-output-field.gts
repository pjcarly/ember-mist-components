import { service } from '@ember/service';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';
import Component from '@glimmer/component';
import type { FieldOf, SomeModel } from './-base-collection';
import { hash } from '@ember/helper';

export interface CustomOutputFieldSignature<
  O extends SomeModel,
  F extends FieldOf<O>,
> {
  Args: {
    model: O;
    field: F;
    label?: string;
    prefix?: string | string[];
    suffix?: string | string[];
  };
  Blocks: {
    default: [{ outputClass: string }];
  };
}

export default class CustomOutputFieldComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<CustomOutputFieldSignature<O, F>> {
  @service declare fieldInformation: FieldInformationService;

  private readonly outputClass: string = 'output-field__output';

  private get label(): string {
    const computedLabel = this.fieldInformation.getTranslatedFieldLabel(
      this.args.model,
      this.args.field,
    );
    return this.args.label ?? computedLabel;
  }

  get prefixes() {
    if (!this.args.prefix) {
      return undefined;
    }

    return Array.isArray(this.args.prefix)
      ? this.args.prefix
      : [this.args.prefix];
  }

  get suffixes() {
    if (!this.args.suffix) {
      return undefined;
    }

    return Array.isArray(this.args.suffix)
      ? this.args.suffix
      : [this.args.suffix];
  }

  <template>
    <div class='output-field'>
      {{#if this.label}}
        <label class='output-field__label'>
          {{this.label}}
        </label>
      {{/if}}

      <div class='output-field__box'>
        {{#each this.prefixes as |prefix|}}
          <span class='output-field__prefix'>
            {{prefix}}
          </span>
        {{/each}}

        {{yield (hash outputClass=this.outputClass)}}

        {{#each this.suffixes as |suffix|}}
          <span class='output-field__suffix'>
            {{suffix}}
          </span>
        {{/each}}
      </div>
    </div>
  </template>
}
