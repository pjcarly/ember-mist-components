import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { cached } from '@glimmer/tracking';
import type { ComponentLike } from '@glint/template';
import { Dropdown } from 'bootstrap.native';
import { modifier } from 'ember-modifier';
import { tracked } from 'tracked-built-ins';

export interface MenuDropdownSignature {
  Args: {
    iconComponent?: ComponentLike<any>;
  };
  Blocks: {
    default: [];
  };
  Element: HTMLLIElement;
}

export default class MenuDropdownComponent extends Component<MenuDropdownSignature> {
  @cached
  get ariaId(): string {
    return `${guidFor(this)}-trigger`;
  }

  @tracked bsDropdown?: Dropdown;

  dropdownModifier = modifier((element: HTMLButtonElement) => {
    const bootstrapDropdown = new Dropdown(element);
    this.bsDropdown = bootstrapDropdown;

    return () => {
      this.bsDropdown = undefined;
    };
  });
}
