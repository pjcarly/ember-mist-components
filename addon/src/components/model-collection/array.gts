import type Store from '@ember-data/store';
import { assert } from '@ember/debug';
import { service } from '@ember/service';
import BaseCollectionComponent, {
  type BaseCollectionArguments,
  type BaseCollectionSignature,
  type Collection,
  type CollectionPageInfo,
  type CollectionSortingInfo,
  type ModelKey,
} from '../-base-collection.ts';
import Order, { Direction } from '../../query/Order.ts';
import { action } from '@ember/object';
import type ModelRegistry from 'ember-data/types/registries/model';
import sortBy from 'lodash.sortby';
import { camelize } from '@ember/string';

interface ModelCollectionArrayArguments<
  K extends ModelKey,
  M extends ModelRegistry[K],
> extends BaseCollectionArguments {
  // Required
  modelName: K;
  models: Array<M>;
  isLoading?: boolean;
  meta?: any;
  // Fake pagination
  pageSize?: number;
  // Refresh
  refresh?: () => void;
  /**
   * Passing a `sort` argument disables all sorting in the ModelCollection::Array and will call this argument when a sort is requested.
   */
  sort?: {
    sortOrder: Order | undefined;
    setSort: (path: string, direction?: -1 | 1) => void;
  };
}

interface ModelCollectionArraySignature<
  K extends ModelKey,
  M extends ModelRegistry[K],
> extends BaseCollectionSignature<M> {
  Args: ModelCollectionArrayArguments<K, M>;
}

/**
 * Returns a collection for an array of records.
 * Adds 'fake' pagination, search, sort... so you can use it with CollectionUi components.
 */
export default class ModelCollectionArrayComponent<
  K extends ModelKey,
  M extends ModelRegistry[K],
> extends BaseCollectionComponent<M, ModelCollectionArraySignature<K, M>> {
  @service declare store: Store;

  constructor(owner: unknown, args: ModelCollectionArrayArguments<K, M>) {
    super(owner, args);

    assert(
      'The ModelCollection::Array component needs at least an array of @models and a @modelName.',
      Boolean(args.models && args.modelName),
    );
  }

  //#region DATA
  private get filteredModels() {
    // Make a copy of the array, also prevents Ember Arrays, ManyArrays...
    const models = this.args.models.slice();

    if (!this.searchString) {
      return models;
    }

    const searchString = this.searchString.replaceAll('*', '');

    return models.filter((model: M) => {
      return this.searchFields.some((field) => {
        const value = model[field as keyof M];
        const stringValue = String(value);
        return stringValue.toLowerCase().includes(searchString.toLowerCase());
      });
    });
  }

  @action
  setSort(path: string, direction?: -1 | 1) {
    if (this.args.sort) {
      this.args.sort.setSort(path, direction);
    } else {
      super.setSort(path, direction);
    }
  }

  private get sortOrder() {
    if (this.args.sort) {
      return this.args.sort.sortOrder;
    }

    if (this.desiredSortOrder) {
      return this.desiredSortOrder;
    }

    // In a ModelCollection::Array we can use the api list view for sorting too, because there is no query.
    if (this.listView.sortOrder) {
      return new Order(
        this.listView.sortOrder.field,
        this.listView.sortOrder.dir &&
        this.listView.sortOrder.dir.toUpperCase() == 'DESC'
          ? Direction.DESC
          : Direction.ASC,
      );
    }

    return undefined;
  }

  private get sortedModels() {
    // Sorting is handled by a parent
    if (this.args.sort) {
      return this.filteredModels;
    }

    if (!this.sortOrder) {
      return this.filteredModels;
    }

    const sortedRecords = sortBy(
      this.filteredModels,
      camelize(this.sortOrder.field),
    );

    if (this.sortOrder.direction === Direction.DESC) {
      sortedRecords.reverse();
    }

    return sortedRecords;
  }

  private get records(): M[] {
    // Slice for pagination
    return this.sortedModels.slice(this.resultRowFirst - 1, this.resultRowLast);
  }
  //#endregion

  //#region PAGINATION
  get pageCurrent() {
    return this.desiredPage;
  }

  private get pageSize() {
    if (this.desiredPageSize) {
      return Math.min(this.desiredPageSize, this.filteredModels.length);
    }

    if (this.args.pageSize) {
      return Math.min(this.args.pageSize, this.filteredModels.length);
    }

    return this.filteredModels.length;
  }

  private get pageCount() {
    if (this.pageSize < 1) {
      return 0;
    }

    return Math.ceil(this.filteredModels.length / this.pageSize);
  }

  private get resultRowFirst() {
    // TODO
    return this.resultRowLast - this.pageSize + 1;
  }

  private get resultRowLast() {
    // TODO
    return this.pageSize * this.pageCurrent;
  }

  private get pagination(): CollectionPageInfo {
    return {
      count: this.records.length,
      totalCount: this.filteredModels.length,
      pageSize: this.pageSize,
      pageCurrent: this.pageCurrent,
      pageCount: this.pageCount,
      resultRowFirst: this.resultRowFirst,
      resultRowLast: this.resultRowLast,
      setPage: this.setPage,
      setPageSize: this.setPageSize,
    };
  }
  //#endregion

  @action
  refresh() {
    if (this.args.refresh) {
      this.args.refresh();
    } else {
      console.log('No refresh possible for this collection.');
    }
  }

  get modelName() {
    return this.args.modelName;
  }

  get sorting(): CollectionSortingInfo {
    return {
      order: this.sortOrder,
      setSort: this.setSort,
      fields: this.existingFields,
    };
  }

  get collection(): Collection<M> {
    return {
      pagination: this.pagination,
      modelName: this.modelName,
      records: this.records,
      fields: this.fields,
      sorting: this.sorting,
      search: this.search,
      meta: this.args.meta ?? {},
      isLoading: this.args.isLoading ?? false,
      refresh: this.refresh,
    };
  }

  <template>{{yield this.collection}}</template>
}
