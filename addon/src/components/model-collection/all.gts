import ModelCollectionComponent from '../model-collection.gts';
import type { Collection, ModelKey } from '../-base-collection.ts';
import type ModelRegistry from 'ember-data/types/registries/model';
import { trackedFunction } from 'reactiveweb/function';

/**
 * Returns a collection with ALL records for a certain modelName (on 1 page).
 * You can add conditions etc by passing a baseQuery.
 * BE VERY CAREFULL WITH THIS COMPONENT AND USE A DATE CONDITION WHERE POSSIBLE, OTHERWISE THE BACKEND WILL BE VERY MAD!
 */
export default class ModelCollectionAllComponent<
  K extends ModelKey,
  M extends ModelRegistry[K],
> extends ModelCollectionComponent<K, M> {
  // @ts-ignore because dataRequest is slightly different from ModelCollection
  protected dataRequest = trackedFunction(this, async () => {
    const { query } = this;
    const { endpoint } = this.args;

    if (!query || this.args.disabled) {
      return;
    }

    // In a trackedFunction, everything before the first 'await' will be tracked.
    // After this line, we will change some properties on query (by running it, changing the page...).
    // That is why we disconnect from auto-tracking here.
    await Promise.resolve();

    // Set max limit on query
    query.setLimit(2000);

    let records: M[] = [];
    let meta = {};

    const fetchPage = async () => {
      if (query.fetchCount > 0) {
        query.nextPage();
      }

      await (endpoint
        ? query.fetchFromEndpoint(
            this.http,
            this.store,
            `${this.http.endpoint}${endpoint}`,
          )
        : query.fetch(this.store)
      ).then(async (models) => {
        // @ts-ignore
        const { meta: modelsMeta }: object | undefined = models;

        meta = {
          ...meta,
          ...modelsMeta,
        };

        records = [...records, ...(models.toArray() as M[])];

        if (
          modelsMeta['page-current'] &&
          modelsMeta['page-count'] &&
          modelsMeta['page-current'] < modelsMeta['page-count']
        ) {
          await fetchPage();
        }
      });
    };

    await fetchPage();

    return {
      records,
      meta,
    };
  });
  //#endregion

  get records() {
    return this.dataRequest.value?.records ?? [];
  }

  get pagination() {
    return {
      count: this.records.length,
      pageCount: 1,
      pageCurrent: 1,
      pageSize: this.records.length,
      resultRowFirst: 1,
      resultRowLast: this.records.length,
      totalCount: this.records.length,
      setPage: () => {
        // do nothing
      },
      setPageSize: () => {
        // do nothing
      },
    };
  }

  get collection(): Collection<M> {
    return {
      ...super.collection,
      pagination: this.pagination,
      records: this.records,
      meta: this.meta,
      refresh: this.dataRequest.retry,
      isLoading: this.dataRequest.isLoading,
    };
  }

  <template>{{yield this.collection}}</template>
}
