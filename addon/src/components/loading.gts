import { service } from '@ember/service';
import Component from '@glimmer/component';
import type UiService from '../services/ui.ts';
import type { ComponentLike } from '@glint/template';
import type { TOC } from '@ember/component/template-only';

const LoadingDefault: TOC<{
  Element: HTMLElement;
}> = <template>
  <span>
    Loading...</span>
</template>;

export default class LoadingComponent extends Component<{
  Element: HTMLElement;
}> {
  @service declare ui: UiService;

  get loadingComponent(): ComponentLike<{ Element: HTMLElement }> {
    return this.ui.loadingComponent ?? LoadingDefault;
  }

  <template><this.loadingComponent ...attributes /></template>
}
