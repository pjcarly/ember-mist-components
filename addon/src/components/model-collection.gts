import type Store from '@ember-data/store';
import { assert } from '@ember/debug';
import { service } from '@ember/service';
import Query from '../query/Query.ts';
import BaseCollectionComponent, {
  type BaseCollectionArguments,
  type BaseCollectionSignature,
  type Collection,
  type CollectionSortingInfo,
  type ModelKey,
} from './-base-collection.ts';
import { cached } from '@glimmer/tracking';
import type ModelRegistry from 'ember-data/types/registries/model';
import { parseIntOrDefault } from '../utils/parse-int.ts';
import Order, { Direction } from '../query/Order.ts';
import { trackedFunction } from 'reactiveweb/function';
import HttpService from '../services/http.ts';
import { camelize } from '@ember/string';
// import { debounce } from 'reactiveweb/debounce';

export interface ModelCollectionArguments<K extends ModelKey>
  extends BaseCollectionArguments {
  // Required
  modelName: K | K[];
  // Optional: a query to copy from
  baseQuery?: Query<K>;
  // Optional: specific endpoint
  endpoint?: string;
  // List view id (for query)
  listViewId?: string;
  // Disabled (don't fetch)
  disabled?: boolean;
}

interface ModelCollectionSignature<
  K extends ModelKey,
  M extends ModelRegistry[K],
> extends BaseCollectionSignature<M> {
  Args: ModelCollectionArguments<K>;
}

/**
 * Returns a basic collection for a certain modelName.
 * You can add conditions etc by passing a baseQuery.
 */
export default class ModelCollectionComponent<
  K extends ModelKey,
  M extends ModelRegistry[K],
> extends BaseCollectionComponent<M, ModelCollectionSignature<K, M>> {
  @service declare http: HttpService;
  @service declare store: Store;

  constructor(owner: unknown, args: ModelCollectionArguments<K>) {
    super(owner, args);

    assert(
      'The ModelCollection component needs at least a @modelName.',
      Boolean(args.modelName),
    );

    if (Array.isArray(args.modelName)) {
      assert(
        'You have passed an empty array to @modelName on ModelCollection.',
        Boolean(args.modelName.length > 0),
      );
    }
  }

  @cached
  get modelName() {
    if (Array.isArray(this.args.modelName)) {
      // Check the modelName on the listView
      return this.apiListView?.model ?? this.args.modelName[0]!;
    } else {
      return this.args.modelName as K;
    }
  }

  //#region DATA

  /**
   * Depends on
   * - args.modelName
   * - args.baseQuery + args.baseQuery.*
   * - args.listViewId
   * - modelListView --> modelName --> args.listViewId + apiListView
   * - desiredSortOrder
   * - searchString
   * - desiredPage
   * - desiredPageSize
   */
  @cached
  get generatedQuery() {
    const modelName = Array.isArray(this.args.modelName)
      ? this.args.modelName[0]!
      : this.args.modelName;

    // 1. Make query for modelName
    const query = new Query(modelName);

    // 2. Copy stuff from baseQuery
    if (this.args.baseQuery) {
      query.copyFrom(this.args.baseQuery);
      // Make sure it has the correct modelName after copying
      query.setModelName(modelName);
    }

    // 3. Add listView stuff
    if (this.args.listViewId) {
      query.setListView(this.args.listViewId);
    }

    // If there is no list view on the query, check if there is a rows property on the modelListView
    if (!query.hasListView() && this.modelListView) {
      const limit = this.modelListView.rows ?? this.args.baseQuery?.getLimit();
      if (limit) {
        query.setLimit(limit);
      }
    }

    // 4. Sorting
    let sortOrder = this.desiredSortOrder;

    // If there is no desired sort order, and there is no list view on the query, check if there is a sort order on the modelListView
    if (!sortOrder && !query.hasListView() && this.modelListView.sortOrder) {
      sortOrder = new Order(
        this.modelListView.sortOrder.field,
        this.modelListView.sortOrder.dir &&
        this.modelListView.sortOrder.dir.toUpperCase() == 'DESC'
          ? Direction.DESC
          : Direction.ASC,
      );
    }

    if (sortOrder) {
      query.clearOrders();
      query.addOrder(sortOrder);
    }

    // 5. Search
    if (this.searchString) {
      query.setSearch(this.searchString, ...this.searchFields);
    }

    // 6. Pagination
    if (this.desiredPage > 1) {
      query.setPage(this.desiredPage);
    }

    if (this.desiredPageSize) {
      query.setLimit(this.desiredPageSize);
    }

    return query;
  }

  // private debouncedQuery = use(
  //   this,
  //   debounce(200, () => this.generatedQuery)
  // );

  @cached
  get query() {
    return this.generatedQuery;
    // return this.debouncedQuery.current;
  }

  protected dataRequest = trackedFunction(this, async () => {
    const { query } = this;
    const { disabled, endpoint } = this.args;

    if (!query || disabled) {
      return;
    }

    // We need to disconnect from autotracking here, to avoid an infinite loop.
    // Why? TBD
    await Promise.resolve();

    const records = await (endpoint
      ? query.fetchFromEndpoint(
          this.http,
          this.store,
          `${this.http.endpoint}${endpoint}`,
        )
      : query.fetch(this.store));

    return {
      records: records.toArray(),
      // @ts-ignore
      meta: records.meta,
    };
  });

  @cached
  get meta() {
    if (this.dataRequest.value) {
      // @ts-ignore
      const { meta: dasherizedMeta }: { [s: string]: any } | undefined =
        this.dataRequest.value;

      return Object.entries(dasherizedMeta ?? {}).reduce(
        (result: any, [key, value]) => {
          const camelizedKey = camelize(key);
          result[camelizedKey] = value;
          return result;
        },
        {},
      );
    }

    return {};
  }

  get records() {
    return this.dataRequest.value?.records ?? [];
  }
  //#endregion

  get pagination() {
    const {
      pageCount,
      pageCurrent,
      pageSize,
      resultRowFirst,
      resultRowLast,
      totalCount,
    } = this.meta;

    return {
      count: this.records.length,
      pageCount: parseIntOrDefault(pageCount),
      pageCurrent: parseIntOrDefault(pageCurrent, this.desiredPage),
      pageSize: parseIntOrDefault(pageSize),
      resultRowFirst: parseIntOrDefault(resultRowFirst),
      resultRowLast: parseIntOrDefault(resultRowLast),
      totalCount: parseIntOrDefault(totalCount),
      setPage: this.setPage,
      setPageSize: this.setPageSize,
    };
  }

  get sorting(): CollectionSortingInfo {
    let order: CollectionSortingInfo['order'] = this.query?.getOrders()[0];

    if (!order) {
      order = this.listView.sortOrder
        ? {
            field: this.listView.sortOrder.field,
            direction:
              this.listView.sortOrder.dir?.toUpperCase() == 'DESC'
                ? Direction.DESC
                : Direction.ASC,
          }
        : undefined;
    }

    return {
      order,
      setSort: this.setSort,
      fields: this.existingFields,
    };
  }

  get collection(): Collection<M> {
    return {
      modelName: this.modelName,
      pagination: this.pagination,
      sorting: this.sorting,
      search: this.search,
      fields: this.fields,
      records: this.records,
      meta: this.meta,
      refresh: this.dataRequest.retry,
      isLoading: this.dataRequest.isLoading,
    };
  }

  <template>{{yield this.collection}}</template>
}
