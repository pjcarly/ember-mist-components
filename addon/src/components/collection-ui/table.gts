import Component from '@glimmer/component';
import type { Collection, ModelKey, SomeModel } from '../-base-collection.ts';
import { camelize, dasherize } from '@ember/string';
import { service } from '@ember/service';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';
import { Direction } from '../../query/Order.ts';
import UiService from '../../services/ui.ts';
import type Store from '@ember-data/store';
import { cached } from '@glimmer/tracking';
import MistModel from '../../models/mist-model.ts';
import type RouterService from '@ember/routing/router-service';
import or from 'ember-truth-helpers/helpers/or';
import and from 'ember-truth-helpers/helpers/and';
import eq from 'ember-truth-helpers/helpers/eq';
import { on } from '@ember/modifier';
import { fn } from '@ember/helper';
import LoadingComponent from '../loading.gts';
import t from 'ember-intl/helpers/t';
import InputFieldComponent from '@getflights/ember-field-components/components/input-field';
import OutputFieldComponent from '@getflights/ember-field-components/components/output-field';
// @ts-ignore
import hrefTo from 'ember-href-to/helpers/href-to';
import gt from 'ember-truth-helpers/helpers/gt';
import not from 'ember-truth-helpers/helpers/not';

type CollectionUiTableArguments<M extends SomeModel> = {
  /**
   * The collection you want to render a table for.
   */
  collection: Collection<M>;
  /**
   * Set a custom sort function for collection-ui/table (overwrites other sorting)
   */
  sort?: (recordA: M, recordB: M) => number;
} & ( // The following arguments are not required, so I added a '?' to the first one:
  | {
      /**
       * By default, the table will create links to transition to the record's view route.
       * You can overwrite the view route using this argument.
       */
      viewRouteName?: string;
    }
  | {
      /**
       * Optional callback when a record is clicked.
       * If no callback is given, the table will transition the user to the view route of the model.
       */
      recordSelected: (record: M) => void;
    }
  | {
      /**
       * Whether you want to render input fields instead of output fields for all fields.
       */
      editable: boolean | string[];
    }
);

interface CollectionUiTableSignature<M extends SomeModel> {
  Args: CollectionUiTableArguments<M>;
  Blocks: {
    preColumnHeader: [];
    preColumn: [record: M];
    postColumnHeader: [];
    postColumn: [record: M];
    placeholder: [];
  };
  Element: HTMLDivElement;
}

export default class CollectionUiTableComponent<
  M extends SomeModel,
> extends Component<CollectionUiTableSignature<M>> {
  @service declare fieldInformation: FieldInformationService;
  @service declare ui: UiService;
  @service declare router: RouterService;
  @service declare store: Store;

  @cached
  get columns() {
    const order = this.args.collection.sorting.order;
    const isEditable = 'editable' in this.args && this.args.editable === true;

    return this.args.collection.fields.map((modelColumn) => {
      // Camelize the key, but KEEP the path separator (.)
      const camelizedColumn = modelColumn
        .split('.')
        .map((columnPart) => camelize(columnPart))
        .join('.');

      const columnIsSorted = order?.field === dasherize(modelColumn);

      const column: ColumnInterface = {
        valuePath: camelizedColumn,
        canSort: this.args.collection.sorting.fields.includes(modelColumn),
        input:
          isEditable ||
          ('editable' in this.args &&
            Array.isArray(this.args.editable) &&
            this.args.editable.includes(camelizedColumn)),
      };

      column.label = this.fieldInformation.getTranslatedFieldLabel(
        this.args.collection.modelName,
        camelizedColumn,
      );

      column.modelName = this.args.collection.modelName;

      if (columnIsSorted) {
        column.sortDirection = order?.direction === Direction.ASC ? 1 : -1;
      }

      return column;
    });
  }

  sortOnColumn = (column: ColumnInterface) => {
    const dasherizedColumn = dasherize(column.valuePath);

    if (column.sortDirection) {
      // Change sort direction
      this.args.collection.sorting.setSort(
        dasherizedColumn,
        (column.sortDirection * -1) as -1 | 1,
      );
    } else {
      // Sort (default direction)
      this.args.collection.sorting.setSort(dasherizedColumn);
    }
  };

  @cached
  get globalActionMeta() {
    let isSelectable = false;
    let doTransition: boolean | null = false;

    if ('recordSelected' in this.args) {
      isSelectable = true;
    } else if ('viewRouteName' in this.args) {
      doTransition = true;
    } else if (!('editable' in this.args)) {
      doTransition = null; // unknown
    }

    return {
      isSelectable,
      doTransition,
    };
  }

  actionMeta = (record: any) => {
    const meta = {
      ...this.globalActionMeta,
    };

    if (meta.doTransition === null) {
      meta.doTransition = this.isMistModel(record) && record.hasViewRoute;
    }

    return meta;
  };

  viewRouteName = (record: any) => {
    return 'viewRouteName' in this.args
      ? this.args.viewRouteName
      : this.isMistModel(record) && record.hasViewRoute
        ? (record as MistModel).viewRouteName
        : undefined;
  };

  private isMistModel(record: any): record is MistModel {
    return 'hasViewRoute' in record;
  }

  recordSelected = (record: any) => {
    'recordSelected' in this.args && this.args.recordSelected?.(record);
  };

  get records() {
    if (this.args.sort) {
      return [...this.args.collection.records].sort(this.args.sort);
    }

    return this.args.collection.records;
  }

  <template>
    <div class='table-responsive' ...attributes>
      <table class='table {{if @collection.records "table-hover "}}cui-table'>
        {{#let
          (or (has-block 'preColumn') (has-block 'preColumnHeader'))
          as |hasPreColumn|
        }}
          {{#let
            (or (has-block 'postColumn') (has-block 'postColumnHeader'))
            as |hasPostColumn|
          }}
            <thead>
              <tr>
                {{#if (and hasPreColumn @collection.records)}}
                  <th class='cui-table__column'>
                    {{yield to='preColumnHeader'}}
                  </th>
                {{/if}}
                {{#each this.columns as |column|}}
                  {{#if (and column.canSort (not @sort))}}
                    {{! template-lint-disable require-presentational-children }}
                    <th
                      role='button'
                      class='cui-table__column cui-table__column--sortable
                        {{if
                          (eq column.sortDirection 1)
                          "cui-table__column--sorted-asc"
                        }}
                        {{if
                          (eq column.sortDirection -1)
                          "cui-table__column--sorted-desc"
                        }}'
                      {{on 'click' (fn this.sortOnColumn column)}}
                    >
                      {{if column.label column.label column.valuePath}}
                    </th>
                  {{else}}
                    <th class='cui-table__column'>
                      {{if column.label column.label column.valuePath}}
                    </th>
                  {{/if}}
                {{/each}}
                {{#if (and hasPostColumn @collection.records)}}
                  <th class='cui-table__column'>
                    {{yield to='postColumnHeader'}}
                  </th>
                {{/if}}
              </tr>
            </thead>
            <tbody>
              {{#if @collection.isLoading}}
                <tr class='cui-table__row'>
                  <td class='cui-table-cell' colspan={{this.columns.length}}>
                    <LoadingComponent />
                  </td>
                </tr>
              {{else}}
                {{#each this.records as |record|}}
                  {{#let (this.actionMeta record) as |actionMeta|}}
                    {{! template-lint-disable no-invalid-interactive }}
                    <tr
                      class='cui-table__row cui-table__row--data'
                      data-test='modelTableRow'
                      role={{if actionMeta.isSelectable 'button'}}
                      {{on 'click' (fn this.recordSelected record)}}
                    >
                      {{#if hasPreColumn}}
                        <td class='cui-table__cell cui-table__cell--pre'>
                          {{yield record to='preColumn'}}
                        </td>
                      {{/if}}
                      {{#each this.columns as |column i|}}
                        <td class='cui-table__cell cui-table__cell--data'>
                          {{#if column.input}}
                            {{#let
                              (component
                                InputFieldComponent
                                model=record
                                field=column.valuePath
                                inline=true
                              )
                              as |InputComponent|
                            }}
                              <InputComponent class='form-control' />
                            {{/let}}
                          {{else}}
                            {{#let
                              (component
                                OutputFieldComponent
                                model=record
                                field=column.valuePath
                                inline=true
                              )
                              as |OutputComponent|
                            }}
                              {{#if actionMeta.doTransition}}
                                <a
                                  class='cui-table__cell__wrapper'
                                  href={{hrefTo
                                    (this.viewRouteName record)
                                    record.id
                                  }}
                                  {{! Only focus the first column, so people using keyboard can use TAB to switch between rows }}
                                  tabindex={{if (gt i 0) -1}}
                                >
                                  <OutputComponent />
                                </a>
                              {{else}}
                                <OutputComponent />
                              {{/if}}
                            {{/let}}
                          {{/if}}
                        </td>
                      {{/each}}
                      {{#if hasPostColumn}}
                        <td class='cui-table__cell cui-table__cell--post'>
                          {{yield record to='postColumn'}}
                        </td>
                      {{/if}}
                    </tr>
                  {{/let}}
                {{else}}
                  {{#if (has-block 'placeholder')}}
                    <tr class='cui-table__row'>
                      <td
                        class='cui-table-cell cui-table-cell--placeholder'
                        colspan={{this.columns.length}}
                      >
                        {{yield to='placeholder'}}
                      </td>
                    </tr>
                  {{else}}
                    <tr class='cui-table__row'>
                      <td
                        class='cui-table__cell cui-table__cell--empty'
                        colspan={{this.columns.length}}
                      >
                        {{t 'ember-mist-components.table.no_data'}}
                      </td>
                    </tr>
                  {{/if}}
                {{/each}}
              {{/if}}
            </tbody>
          {{/let}}
        {{/let}}
      </table>
    </div>
  </template>
}

export interface ColumnInterface {
  label?: string;
  modelName?: ModelKey;
  valuePath: string;
  canSort: boolean;
  sortDirection?: -1 | 1;
  input: boolean;
}
