import type { Collection, SomeModel } from '../-base-collection.ts';
import Component from '@glimmer/component';
import { tracked } from 'tracked-built-ins';
import { isBlank } from '@ember/utils';
import { service } from '@ember/service';
import type ModelInformationService from '../../services/model-information.ts';
import CollectionUiSearchComponent from './search.gts';
import MenuItemComponent from '../menu-item.ts';
import MenuDropdownComponent from '../menu-dropdown.ts';
import IconComponent from '../icon.gts';
import t from 'ember-intl/helpers/t';
import or from 'ember-truth-helpers/helpers/or';

interface CollectionUiHeaderOptions {
  search?: boolean;
  refresh?: boolean;
  lockSearch?: boolean;
}

interface CollectionUiHeaderSignature<M extends SomeModel> {
  Args: {
    collection: Collection<M>;
    options?: CollectionUiHeaderOptions;
  };
  Blocks: {
    title?: [];
    filters?: [];
    new?: [];
    actions?: [];
    dropdown?: [];
  };
}

export default class CollectionUiHeaderComponent<
  M extends SomeModel,
> extends Component<CollectionUiHeaderSignature<M>> {
  @service declare modelInformation: ModelInformationService;

  get options(): CollectionUiHeaderOptions {
    return {
      // Default
      search: true,
      refresh: true,
      // Args
      ...this.args.options,
    };
  }

  get searchVisible() {
    return (
      this.options.lockSearch ||
      !isBlank(this.args.collection.search.searchString) ||
      this._searchOpened
    );
  }

  get canToggleSearch() {
    return !this.options.lockSearch;
  }

  @tracked private _searchOpened = false;

  get computedTitle() {
    return this.modelInformation.getTranslatedPlural(
      this.args.collection.modelName,
    );
  }

  toggleSearch = () => {
    this._searchOpened = !this.searchVisible;

    if (!this._searchOpened && this.args.collection.search.searchString) {
      this.args.collection.search.clearSearch();
    }
  };

  <template>
    <div class='cui-header'>
      {{#if this.searchVisible}}
        <CollectionUiSearchComponent
          @collection={{@collection}}
          @close={{if this.canToggleSearch this.toggleSearch}}
        />
      {{else}}
        <div class='cui-header__info'>
          <h5 class='cui-header__title'>
            {{#if (has-block 'title')}}
              {{yield to='title'}}
            {{else}}
              {{this.computedTitle}}
            {{/if}}
          </h5>

          {{#if (has-block 'filters')}}
            <div class='cui-header__filters'>
              {{yield to='filters'}}
            </div>
          {{/if}}
        </div>

        <ul class='cui-header__actions action-menu'>
          {{yield to='actions'}}
          {{#if (has-block 'new')}}
            <MenuItemComponent class='cui-header__action-new'>
              {{yield to='new'}}
            </MenuItemComponent>
          {{/if}}
          {{#if this.options.search}}
            <MenuItemComponent @itemClicked={{this.toggleSearch}}>
              <IconComponent @name='search' />
            </MenuItemComponent>
          {{/if}}
          {{#if (or (has-block 'dropdown') this.options.refresh)}}
            <MenuDropdownComponent class='cui-header__action-more'>
              <MenuItemComponent @itemClicked={{@collection.refresh}}>
                {{t 'ember-mist-components.labels.refresh'}}
                <IconComponent @name='refresh' />
              </MenuItemComponent>
              {{yield to='dropdown'}}
            </MenuDropdownComponent>
          {{/if}}
        </ul>
      {{/if}}
    </div>
  </template>
}
