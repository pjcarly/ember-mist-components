import Component from '@glimmer/component';
import { on } from '@ember/modifier';
import type { BaseInputArguments } from '@getflights/ember-field-components/components/input/-base';
import { modifier } from 'ember-modifier';
import intlTelInput from 'intl-tel-input';
import { tracked } from 'tracked-built-ins';
import { service } from '@ember/service';
import type AddressService from '../../services/address.ts';
import type PhoneIntlService from '../../services/phone-intl.ts';
import { task } from 'ember-concurrency';
import LoadingComponent from '../loading.gts';

export interface InputPhoneOptions {
  initialCountry?: string;
  /**
   * Country whitelist
   */
  onlyCountries?: string[];
  /**
   * Country blacklist (ignored when a whitelist is passed)
   * DEPRECATED: use excludeCountries in new code
   */
  blockedCountries?: string[];
  /**
   * Country blacklist (ignored when a whitelist is passed)
   */
  excludeCountries?: string[];
  placeholderNumberType?: Parameters<
    intlTelInput.Plugin['setPlaceholderNumberType']
  >[0];
}

export interface InputPhoneArguments extends BaseInputArguments {
  value: string | undefined;
  valueChanged?: (newValue: string | undefined) => void;
  options?: InputPhoneOptions;
}

export interface InputPhoneSignature {
  Args: InputPhoneArguments;
  Element: HTMLElement | HTMLInputElement;
}

export default class InputPhoneComponent extends Component<InputPhoneSignature> {
  @service declare phoneIntl: PhoneIntlService;
  @service declare address: AddressService;

  constructor(owner: any, args: InputPhoneSignature['Args']) {
    super(owner, args);

    this.initialize.perform();
  }

  initialize = task(async () => {
    // Utils need to be loaded to use getNumber() properly
    await Promise.resolve();

    await this.phoneIntl.loadUtils.perform();
  });

  instance?: intlTelInput.Plugin;

  valueChanged = () => {
    if (this.args.disabled) {
      return;
    }

    this.args.valueChanged?.(this.instance?.getNumber());
  };

  userSelectedCountry = false;
  countryChanged = () => {
    this.userSelectedCountry = true;
    if (this.args.disabled) {
      return;
    }

    this.args.valueChanged?.(this.instance?.getNumber());
  };

  get initialCountry() {
    // Backwards compatibility
    let excludeCountries =
      this.args.options?.excludeCountries ??
      this.args.options?.blockedCountries;
    const { initialCountry, onlyCountries } = this.args.options ?? {};

    if (!initialCountry) {
      return undefined;
    }

    /**
     * Initial country is not in the allow list
     */
    if (
      onlyCountries &&
      !onlyCountries.some(
        (only) => only.toLowerCase() === initialCountry.toLowerCase(),
      )
    ) {
      return undefined;
    }

    /**
     * Initial country is excluded
     */
    if (
      excludeCountries &&
      excludeCountries.some(
        (excluded) => excluded.toLowerCase() === initialCountry.toLowerCase(),
      )
    ) {
      return undefined;
    }

    return initialCountry;
  }

  @tracked inputElement?: HTMLInputElement;

  inputModifier = modifier(
    (element: HTMLInputElement, [value]: [value?: string]) => {
      if (!this.instance) {
        this.instance = intlTelInput(element, {
          initialCountry: this.initialCountry,
          separateDialCode: true,
          // dropdownContainer,
          preferredCountries: this.address.defaultCountry
            ? [this.address.defaultCountry]
            : [],
          placeholderNumberType:
            this.args.options?.placeholderNumberType ?? 'FIXED_LINE',
          excludeCountries: this.args.options?.excludeCountries ?? [],
          onlyCountries: this.args.options?.onlyCountries ?? [],
        });

        this.inputElement = element;
      }

      if (value) {
        this.instance.setNumber(value ?? '');
      } else {
        if (!this.userSelectedCountry && this.initialCountry) {
          this.instance.setCountry(this.initialCountry);
        }
      }

      element.addEventListener('countrychange', this.countryChanged);

      return () => {
        element.removeEventListener('countrychange', this.countryChanged);
      };
    },
  );

  <template>
    {{#if this.initialize.isRunning}}
      <div class='input loader'>
        <LoadingComponent ...attributes />
      </div>
    {{else}}
      <input
        type='tel'
        class='input input-phone'
        ...attributes
        value={{@value}}
        disabled={{@disabled}}
        required={{@required}}
        {{on 'input' this.valueChanged}}
        {{this.inputModifier @value}}
      />
    {{/if}}
  </template>
}
