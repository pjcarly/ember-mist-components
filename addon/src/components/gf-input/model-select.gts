import { service } from '@ember/service';
import Component from '@glimmer/component';
import LoadingComponent from '../loading.gts';
import type IntlService from 'ember-intl/services/intl';
import type { BaseInputArguments } from '@getflights/ember-field-components/components/input/-base';
import InputSelectComponent, {
  type InputSelectOptions,
} from '@getflights/ember-field-components/components/input/select';
import type DynamicSelectOptionService from '../../services/dynamic-select-options.ts';
import type ModelInformationService from '../../services/model-information.ts';
import { trackedFunction } from 'reactiveweb/function';
import type ModelRegistry from 'ember-data/types/registries/model';
import Query from '../../query/Query.ts';
import type Store from '@ember-data/store';

export interface GfInputModelSelectOptions<K extends keyof ModelRegistry> {
  /**
   * Model you want to render a select for
   */
  modelName: K;
  /**
   * Field on the model you want to render in the select option
   */
  nameField?: string;
  baseQuery?: Query<K>;
}

export interface GfInputModelSelectArguments<
  K extends keyof ModelRegistry,
  M extends ModelRegistry[K],
  Required extends boolean | undefined = undefined,
> extends BaseInputArguments {
  value: M | undefined;
  valueChanged?: (newValue: Required extends true ? M : M | undefined) => void;
  required?: Required;
  options: GfInputModelSelectOptions<K>;
}

export interface GfInputModelSelectSignature<
  K extends keyof ModelRegistry,
  M extends ModelRegistry[K],
  Required extends boolean | undefined,
> {
  Args: GfInputModelSelectArguments<K, M, Required>;
  Element: HTMLSpanElement | HTMLSelectElement;
}

export default class GfInputModelSelectComponent<
  K extends keyof ModelRegistry,
  M extends ModelRegistry[K],
  Required extends boolean | undefined,
> extends Component<GfInputModelSelectSignature<K, M, Required>> {
  @service declare dynamicSelectOptions: DynamicSelectOptionService;
  @service declare intl: IntlService;
  @service declare modelInformation: ModelInformationService;
  @service declare store: Store;

  get baseQuery() {
    const query = new Query(this.args.options.modelName);

    if (this.args.options?.baseQuery) {
      query.copyFrom(this.args.options.baseQuery);
    }

    return query;
  }

  private selectOptionsRequest = trackedFunction(this, async () => {
    return await this.dynamicSelectOptions.getModelSelectOptions.perform(
      this.args.options.modelName,
      this.baseQuery.hasConditions ||
        this.baseQuery.hasConditionLogic ||
        this.baseQuery.hasOrders
        ? this.baseQuery
        : undefined,
      this.args.options?.nameField,
    );
  });

  get isLoading() {
    return this.selectOptionsRequest.isLoading;
  }

  get isError() {
    return this.selectOptionsRequest.isError;
  }

  get selectOptions() {
    return this.selectOptionsRequest.value ?? [];
  }

  get options(): InputSelectOptions {
    return {
      selectOptions: this.selectOptions ?? [],
    };
  }

  get value() {
    return this.args.value?.id;
  }

  valueChanged = (id: string | undefined) => {
    if (!id) {
      // @ts-ignore typing of Required fucks up these types
      this.args.valueChanged?.(undefined);
      return;
    }
    // id to model
    const record = this.store.peekRecord(this.args.options.modelName, id)!;
    this.args.valueChanged?.(record);
  };

  <template>
    {{#if this.isLoading}}
      <span class='gf-input gf-input-model-select' ...attributes>
        <div class='gf-input-model-select__loading form-control'>
          <LoadingComponent />
        </div>
      </span>
    {{else}}
      <InputSelectComponent
        class='form-control'
        @value={{this.value}}
        @valueChanged={{this.valueChanged}}
        @disabled={{@disabled}}
        @required={{@required}}
        @options={{this.options}}
        ...attributes
      />
    {{/if}}
  </template>
}
