import { service } from '@ember/service';
import AddressService from '../../services/address.ts';
import Component from '@glimmer/component';
import LoadingComponent from '../loading.gts';
import type IntlService from 'ember-intl/services/intl';
import type { BaseInputArguments } from '@getflights/ember-field-components/components/input/-base';
import InputSelectComponent, {
  type InputSelectOptions,
} from '@getflights/ember-field-components/components/input/select';

export interface GfInputCountryCodeOptions {
  placeholder?: string;
}

export interface GfInputCountryCodeArguments<
  Required extends boolean | undefined = undefined,
> extends BaseInputArguments {
  value: string | undefined;
  valueChanged?: (
    newValue: Required extends true ? string : string | undefined,
  ) => void;
  required?: Required;
  options?: GfInputCountryCodeOptions;
}

export interface GfInputCountryCodeSignature<
  Required extends boolean | undefined,
> {
  Args: GfInputCountryCodeArguments<Required>;
  Element: HTMLSelectElement;
}

export default class GfInputCountryCodeComponent<
  Required extends boolean | undefined,
> extends Component<GfInputCountryCodeSignature<Required>> {
  @service declare address: AddressService;
  @service declare intl: IntlService;

  get selectOptions() {
    return this.address.countrySelectOptions;
  }

  get isLoading() {
    return this.address.countrySelectOptionsRequest.isLoading;
  }

  get options(): InputSelectOptions {
    return {
      selectOptions: this.selectOptions ?? [],
      placeholder: this.intl.t('ember-mist-components.labels.country_none'),
    };
  }

  <template>
    <div class='gf-input gf-input-country-code'>
      {{#if this.isLoading}}
        <div class='gf-input-country-code__loading form-control'>
          <LoadingComponent />
        </div>
      {{else}}
        <InputSelectComponent
          class='form-control'
          @value={{@value}}
          @valueChanged={{@valueChanged}}
          @required={{@required}}
          @options={{this.options}}
          ...attributes
        />
      {{/if}}
    </div>
  </template>
}
