import type ModelRegistry from 'ember-data/types/registries/model';
import type { ModelKey } from '../-base-collection';
import type Query from '../../query/Query';
import type { ModalControls } from '../modal';
import Component from '@glimmer/component';
import { service } from '@ember/service';
import type { IntlService } from 'ember-intl';
import type ModelInformationService from '../../services/model-information';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import { tracked } from 'tracked-built-ins';
import ModalComponent from '../modal.gts';
import { on } from '@ember/modifier';
import IconComponent from '../icon.gts';
import ModelCollectionComponent from '../model-collection.gts';
import InputSelectComponent from '@getflights/ember-field-components/components/input/select';
import { hash } from '@ember/helper';
import CollectionUiHeaderComponent from '../collection-ui/header.gts';
import CollectionUiTableComponent from '../collection-ui/table.gts';
import CollectionUiPaginatorComponent from '../collection-ui/paginator.gts';
import t from 'ember-intl/helpers/t';

export interface GfInputLookupWindowOptions<K extends ModelKey> {
  /**
   * The passed in name of the model
   */
  modelName: K | (K | ModelKey)[];
  modelListView?: string;
  /**
   * Query that can be passed in to limit the results to
   */
  baseQuery?: Query;
}

export interface GfInputLookupWindowArguments<
  K extends ModelKey,
  M extends ModelRegistry[K],
> {
  value?: M;
  valueChanged: (model: M) => void;
  options: GfInputLookupWindowOptions<K>;
}

export interface GfInputLookupWindowSignature<
  K extends ModelKey,
  M extends ModelRegistry[K],
> {
  Args: GfInputLookupWindowArguments<K, M>;
  Blocks: {
    default: [show: ModalControls['show']];
  };
  Element: HTMLButtonElement;
}

export default class GfInputLookupWindowComponent<
  K extends ModelKey,
  M extends ModelRegistry[K],
> extends Component<GfInputLookupWindowSignature<K, M>> {
  @service declare intl: IntlService;
  @service declare modelInformation: ModelInformationService;

  modalVisible = false;

  //#region Model selection
  get isMultiModelNames() {
    return Array.isArray(this.args.options.modelName);
  }

  get modelNameSelectOptions() {
    const modelNames = Array.isArray(this.args.options.modelName)
      ? this.args.options.modelName
      : [this.args.options.modelName];

    return modelNames.map((modelName): SelectOption => {
      return {
        value: modelName,
        label: this.modelInformation.getTranslatedPlural(modelName),
      };
    });
  }

  @tracked private _selectedModelName?: ModelKey;

  get selectedModelName() {
    if (this._selectedModelName) {
      return this._selectedModelName;
    }

    if (Array.isArray(this.args.options.modelName)) {
      return this.args.value
        ? (this.modelInformation.getModelName(this.args.value) as ModelKey)
        : this.args.options.modelName[0]!;
    }

    return this.args.options.modelName;
  }

  selectedModelNameChanged = (selectedModelName: string) => {
    this._selectedModelName = selectedModelName as ModelKey;
  };
  //#endregion

  createRowSelectCallback = (controls: ModalControls) => {
    return (model: M) => {
      controls.close();
      this.args.valueChanged(model);
    };
  };

  <template>
    <ModalComponent as |modal|>
      {{#if (has-block)}}
        {{yield modal.show}}
      {{else}}
        <button
          type='button'
          class='btn btn-default'
          {{on 'click' modal.show}}
          ...attributes
        >
          <IconComponent @name='search' />
        </button>
      {{/if}}
      <modal.Dialog @size='lg' as |dialog|>
        <dialog.Header>
          <button class='btn btn-icon' type='button' {{on 'click' modal.close}}>
            <i class='icon-cancel'></i>
          </button>
        </dialog.Header>
        <dialog.Body>
          {{#if modal.visible}}
            {{#if this.isMultiModelNames}}
              <div>
                <InputSelectComponent
                  class='form-control'
                  @options={{hash selectOptions=this.modelNameSelectOptions}}
                  @value={{this.selectedModelName}}
                  @required={{true}}
                  @valueChanged={{this.selectedModelNameChanged}}
                />
              </div>
            {{/if}}
            <ModelCollectionComponent
              @modelName={{this.selectedModelName}}
              @baseQuery={{@options.baseQuery}}
              @modelListView={{@options.modelListView}}
              as |collection|
            >
              <CollectionUiHeaderComponent
                @collection={{collection}}
                @options={{hash lockSearch=true}}
              />
              <CollectionUiTableComponent
                @collection={{collection}}
                @recordSelected={{this.createRowSelectCallback modal}}
              />
              <CollectionUiPaginatorComponent @collection={{collection}} />
            </ModelCollectionComponent>
          {{/if}}
        </dialog.Body>
        <dialog.Footer>
          <button type='button' class='btn btn-link' {{on 'click' modal.close}}>
            {{t 'label.close'}}
          </button>
        </dialog.Footer>
      </modal.Dialog>
    </ModalComponent>
  </template>
}
