import templateOnly from '@ember/component/template-only';

export interface MenuItemSignature {
  Args: {
    id?: string;
    itemClicked?: () => void;
    role?: string;
    count?: number;
    targetRoute?: string;
  };
  Blocks: {
    default: [];
  };
  Element: HTMLLIElement;
}

const MenuItemComponent = templateOnly<MenuItemSignature>();
export default MenuItemComponent;
