import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import GfInputDrupalImageComponent, {
  type GfInputDrupalImageOptions,
} from '../gf-input/drupal-image.gts';
import { service } from '@ember/service';
import type ModelInformationService from '../../services/model-information';

export default class InputFieldImageComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, GfInputDrupalImageOptions>;
  Element: HTMLInputElement;
}> {
  @service declare modelInformation: ModelInformationService;

  get inputFieldArgs() {
    return {
      ...this.args,
      inputOptions: {
        ...this.args.inputOptions,
        field: this.args.field,
        // @ts-ignore
        modelName: this.modelInformation.getModelName(this.args.model),
        multiple: false,
      },
    };
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{GfInputDrupalImageComponent}}
      @fieldType='image'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
