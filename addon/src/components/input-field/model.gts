import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import GfInputLookupComponent, {
  type GfInputLookupOptions,
} from '../gf-input/lookup.gts';
import type ModelRegistry from 'ember-data/types/registries/model';

export default class InputFieldModelComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, GfInputLookupOptions<keyof ModelRegistry>>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{GfInputLookupComponent}}
      @fieldType='model'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
