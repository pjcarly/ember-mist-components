import { guidFor } from '@ember/object/internals';
import type { InputFieldLayoutSignature } from '@getflights/ember-field-components/components/input-field/-layouts/index';
import Component from '@glimmer/component';

export default class InputFieldBootstrapLayoutDefault extends Component<InputFieldLayoutSignature> {
  private readonly inputClass = 'form-control';

  private get inputId() {
    return this.args.inputId ?? guidFor(this);
  }

  <template>
    <div class='row'>
      <div
        class='input-group{{if @required " is-required"}}{{if
            @errors
            " has-errors"
          }}{{if @disabled " disabled"}}'
      >
        {{#if @label}}
          <label for={{this.inputId}} class='control-label'>
            {{@label}}
          </label>
        {{/if}}

        {{#each @prefixes as |prefix|}}
          <div class='input-group-text'>
            {{prefix}}
          </div>
        {{/each}}

        <@inputComponent
          class='{{this.inputClass}}{{if @errors " is-invalid"}}'
          id={{this.inputId}}
          ...attributes
        />

        {{#each @suffixes as |suffix|}}
          <div class='input-group-text'>
            {{suffix}}
          </div>
        {{/each}}

        {{#if @helptext}}
          <div class='form-text'>
            {{@helptext}}
          </div>
        {{/if}}

        {{#each @errors as |error|}}
          <div class='invalid-feedback'>
            {{error}}
          </div>
        {{/each}}
      </div>
    </div>
  </template>
}
