import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import { service } from '@ember/service';
import type ModelInformationService from '../../services/model-information.ts';
import type Model from '@ember-data/model';
import { cached } from '@glimmer/tracking';
import GfInputModelSelectComponent, {
  type GfInputModelSelectOptions,
} from '../gf-input/model-select.gts';
import type ModelRegistry from 'ember-data/types/registries/model';

export default class InputFieldModelSelectComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<
    O,
    F,
    GfInputModelSelectOptions<keyof ModelRegistry>
  >;
  Element: HTMLInputElement;
}> {
  @service declare modelInformation: ModelInformationService;

  @cached
  get modelName() {
    return this.modelInformation.getModelName(
      this.args.model as unknown as Model,
    )!;
  }

  get relationshipModelname() {
    return this.modelInformation.getBelongsToModelName(
      this.modelName,
      this.args.field,
    )!;
  }

  get inputFieldArgs() {
    return {
      ...this.args,
      inputOptions: {
        ...this.args.inputOptions,
        modelName: this.relationshipModelname,
      },
    };
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{GfInputModelSelectComponent}}
      @fieldType='model-select'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
