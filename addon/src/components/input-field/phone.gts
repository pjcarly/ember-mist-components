import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import InputPhoneComponent, {
  type InputPhoneOptions,
} from '../input/phone.gts';

export default class InputFieldPhoneComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, InputPhoneOptions>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    const args = { ...this.args };

    /**
     * In case we ever want initialCountry to always be the same as the address
     */
    // if (!args.inputOptions?.initialCountry) {
    //   if ('address' in this.args.model) {
    //     const { countryCode } = this.args.model['address'] ?? {};

    //     if (countryCode) {
    //       args.inputOptions = {
    //         ...args.inputOptions,
    //         initialCountry: countryCode,
    //       };
    //     }
    //   }
    // }

    return args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputPhoneComponent}}
      @fieldType='phone'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
