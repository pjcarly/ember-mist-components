import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import type { TOC } from '@ember/component/template-only';
import t from 'ember-intl/helpers/t';
import InputTextComponent, {
  type InputTextSignature,
} from '@getflights/ember-field-components/components/input/text';

// eslint-disable-next-line prettier/prettier
const AutoNumberComponent: TOC<{ Element: InputTextSignature['Element'] }> =
  <template>
    <InputTextComponent
      @value={{t 'ember-mist-components.labels.automatically_generated'}}
      @disabled={{true}}
      ...attributes
    />
  </template>;

export default class InputFieldAutonumberComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return {
      ...this.args,
    };
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{AutoNumberComponent}}
      @fieldType='autonumber'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
