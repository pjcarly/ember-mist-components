import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import GfInputDrupalFileComponent, {
  type GfInputDrupalFileOptions,
} from '../gf-input/drupal-file.gts';
import { service } from '@ember/service';
import type ModelInformationService from '../../services/model-information';

export default class InputFieldFileComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, GfInputDrupalFileOptions<false>>;
  Element: HTMLInputElement;
}> {
  @service declare modelInformation: ModelInformationService;

  get inputFieldArgs() {
    return {
      ...this.args,
      inputOptions: {
        ...this.args.inputOptions,
        field: this.args.field,
        // @ts-ignore
        modelName: this.modelInformation.getModelName(this.args.model),
        multiple: false,
      },
    };
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{GfInputDrupalFileComponent}}
      @fieldType='file'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
