import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import GfInputCountryCodeComponent, {
  type GfInputCountryCodeOptions,
} from '../gf-input/country-code.gts';

export default class InputFieldCountryCodeComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, GfInputCountryCodeOptions>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{GfInputCountryCodeComponent}}
      @fieldType='country'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
