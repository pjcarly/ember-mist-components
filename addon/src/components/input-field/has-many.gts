import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import { service } from '@ember/service';
import type ModelInformationService from '../../services/model-information';
import GfInputMultiComponent from '../gf-input/multi.gts';
import GfInputLookupComponent, {
  type GfInputLookupOptions,
} from '../gf-input/lookup.gts';
import type ModelRegistry from 'ember-data/types/registries/model';
import type { GfInputModelSelectOptions } from '../gf-input/model-select';
import type Query from '../../query/Query';
import GfInputModelSelectComponent from '../gf-input/model-select.gts';
import type { SyncHasMany } from '@ember-data/model';
import { fn } from '@ember/helper';

interface InputHasManyOptions<K extends keyof ModelRegistry> {
  modelName: K;
  baseQuery?: Query<K>;
  select?: boolean;
}

interface InputHasManyArguments<
  K extends keyof ModelRegistry,
  M extends ModelRegistry[K],
> {
  value: SyncHasMany<M>;
  valueChanged: (newValue: M[]) => void;
  options: InputHasManyOptions<K>;
}

interface InputHasManySignature<
  K extends keyof ModelRegistry,
  M extends ModelRegistry[K],
> {
  Args: InputHasManyArguments<K, M>;
}

class InputHasMany<
  K extends keyof ModelRegistry,
  M extends ModelRegistry[K],
> extends Component<InputHasManySignature<K, M>> {
  get valueAsArray() {
    return this.args.value.slice();
  }

  removeItem = (index: number) => {
    const newValue = [...this.valueAsArray];
    newValue.splice(index, 1);
    this.args.valueChanged(newValue);
  };

  reorderItems = (reorderedItems: M[]) => {
    this.args.valueChanged([...reorderedItems]);
  };

  addNewItem = (item: M | undefined) => {
    if (item) {
      this.args.valueChanged([...this.valueAsArray, item as M]);
    }
  };

  valueChanged = (index: number, newValue: M | undefined) => {
    if (!newValue) {
      this.removeItem(index);
    } else {
      const arrayWithChangedValue = this.valueAsArray.map((value, i) => {
        if (i === index) {
          return newValue;
        }

        return value;
      });
      this.args.valueChanged(arrayWithChangedValue);
    }
  };

  valueFor = (index: number) => {
    return this.args.value.objectAt(index);
  };

  get inputModelSelectOptions(): GfInputModelSelectOptions<K> {
    return {
      modelName: this.args.options.modelName,
      baseQuery: this.args.options.baseQuery,
    };
  }

  get inputLookupOptions(): GfInputLookupOptions<K> {
    return {
      modelName: this.args.options.modelName,
      baseQuery: this.args.options.baseQuery,
    };
  }

  <template>
    <div class='gf-input gf-input-has-many'>
      <GfInputMultiComponent
        @value={{this.valueAsArray}}
        @reorderItems={{this.reorderItems}}
        @hideNew={{true}}
        as |index|
      >
        {{#if @options.select}}
          <GfInputModelSelectComponent
            @value={{this.valueFor index}}
            @options={{this.inputModelSelectOptions}}
            @valueChanged={{fn this.valueChanged index}}
          />
        {{else}}
          <GfInputLookupComponent
            @value={{this.valueFor index}}
            @options={{this.inputLookupOptions}}
            @valueChanged={{fn this.valueChanged index}}
          />
        {{/if}}
      </GfInputMultiComponent>

      {{#if @options.select}}
        <GfInputModelSelectComponent
          @value={{undefined}}
          @options={{this.inputModelSelectOptions}}
          @valueChanged={{this.addNewItem}}
        />
      {{else}}
        <GfInputLookupComponent
          @value={{undefined}}
          @options={{this.inputLookupOptions}}
          @valueChanged={{this.addNewItem}}
        />
      {{/if}}
    </div>
  </template>
}

export default class InputFieldHasManyComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  @service declare modelInformation: ModelInformationService;

  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{InputHasMany}}
      @fieldType='HasMany'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
