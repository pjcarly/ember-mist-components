import Component from '@glimmer/component';
import InputSelectComponent from '@getflights/ember-field-components/components/input/select';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import type { InputSelectOptions } from '@getflights/ember-field-components/components/input/select';
import { service } from '@ember/service';
import LoadingComponent from '../loading.gts';
import type { IntlService } from 'ember-intl';
import type { TOC } from '@ember/component/template-only';
import { use } from 'ember-resources';
import FieldSelectHelperResource from '../../resources/field-select-helper.ts';

export const LoadingSelect: TOC<{ Element: HTMLDivElement }> = <template>
  <div ...attributes>
    <LoadingComponent />
  </div>
</template>;

export default class InputFieldSelectComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, InputSelectOptions>;
  Element: HTMLInputElement;
}> {
  @service declare intl: IntlService;

  private helperResource = use(
    this,
    FieldSelectHelperResource(() => ({
      model: this.args.model,
      field: this.args.field,
      selectOptions: this.args.inputOptions?.selectOptions,
    })),
  );

  get helper() {
    return this.helperResource.current;
  }

  get inputFieldArgs() {
    const args = {
      ...this.args,
      inputOptions: {
        placeholder: this.helper.placeholder,
        ...this.args.inputOptions,
        selectOptions: this.helper.selectOptions,
      },
    };

    if (
      !args.inputOptions?.selectOptions ||
      args.inputOptions.selectOptions.length === 0
    ) {
      args.inputOptions.selectOptions = this.helper.asyncSelectOptions ?? [];

      if (this.helper.isError) {
        args.inputOptions.placeholder = this.intl.t(
          'input-field.select.could_not_load',
        );
      }
    }

    return args;
  }

  get inputComponent() {
    if (!this.args.inputOptions?.selectOptions && this.helper.isLoading) {
      return LoadingSelect;
    }

    return InputSelectComponent;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{this.inputComponent}}
      @fieldType='select'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
