import { service } from '@ember/service';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';
import Component from '@glimmer/component';
import type { FieldOf, SomeModel } from './-base-collection';
import { guidFor } from '@ember/object/internals';
import { hash } from '@ember/helper';
import { errorsFor } from '@getflights/ember-field-components/components/field-messages';

export interface CustomInputFieldSignature<
  O extends SomeModel,
  F extends FieldOf<O>,
> {
  Args: {
    model: O;
    field: F;
    label?: string;
    required?: boolean;
    showErrors?: boolean;
    prefix?: string | string[];
    suffix?: string | string[];
    inputId?: string;
  };
  Blocks: {
    default: [{ inputId: string; inputClass: string }];
  };
}

export default class CustomInputFieldComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<CustomInputFieldSignature<O, F>> {
  @service declare fieldInformation: FieldInformationService;

  get inputClass() {
    let inputClass = 'form-control';

    if (this.errors && this.errors.length > 0) {
      inputClass += ' is-invalid';
    }

    return inputClass;
  }

  private get inputId() {
    return this.args.inputId ?? guidFor(this);
  }

  private get label(): string {
    const computedLabel = this.fieldInformation.getTranslatedFieldLabel(
      this.args.model,
      this.args.field,
    );
    return this.args.label ?? computedLabel;
  }

  get prefixes() {
    if (!this.args.prefix) {
      return undefined;
    }

    return Array.isArray(this.args.prefix)
      ? this.args.prefix
      : [this.args.prefix];
  }

  get suffixes() {
    if (!this.args.suffix) {
      return undefined;
    }

    return Array.isArray(this.args.suffix)
      ? this.args.suffix
      : [this.args.suffix];
  }

  private get errors() {
    const showErrors =
      this.args.showErrors === undefined ? true : this.args.showErrors;

    if (showErrors) {
      const { model, field } = this.args;
      return errorsFor(model, field);
    }

    return undefined;
  }

  <template>
    <div class='row'>
      <div
        class='input-group{{if @required " is-required"}}{{if
            this.errors
            " has-errors"
          }}'
      >
        {{#if this.label}}
          <label for={{this.inputId}} class='control-label'>
            {{this.label}}
          </label>
        {{/if}}

        {{#each this.prefixes as |prefix|}}
          <div class='input-group-text'>
            {{prefix}}
          </div>
        {{/each}}

        {{yield (hash inputId=this.inputId inputClass=this.inputClass)}}

        {{#each this.suffixes as |suffix|}}
          <div class='input-group-text'>
            {{suffix}}
          </div>
        {{/each}}

        {{#each this.errors as |error|}}
          <div class='invalid-feedback'>
            {{error}}
          </div>
        {{/each}}
      </div>
    </div>
  </template>
}
