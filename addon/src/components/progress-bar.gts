import { htmlSafe } from '@ember/template';
import Component from '@glimmer/component';

export interface ProgressBarArguments {
  value?: number;
  options?: ProgressBarOptions;
}

export interface ProgressBarOptions {
  min?: number;
  max?: number;
}

export interface ProgressBarSignature {
  Args: ProgressBarArguments;
  Element: HTMLDivElement;
}

export default class ProgressBarComponent extends Component<ProgressBarSignature> {
  get min(): number {
    return this.args.options?.min ?? 0;
  }

  get max(): number {
    return this.args.options?.max ?? 100;
  }

  get value(): number {
    return this.args.value ?? 0;
  }

  get style() {
    const width = Math.round((this.value / (this.max - this.min)) * 100);
    return htmlSafe(`width: ${width}%`);
  }

  <template>
    <div
      class='progress'
      role='progressbar'
      aria-valuenow={{this.value}}
      aria-valuemin={{this.min}}
      aria-valuemax={{this.max}}
      ...attributes
    >
      {{! template-lint-disable no-inline-styles }}
      <div class='progress-bar' style={{this.style}}></div>
    </div>
  </template>
}
