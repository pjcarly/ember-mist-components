import type { TOC } from '@ember/component/template-only';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import { get } from '@ember/helper';
import type DrupalFile from '../../interfaces/drupal-file';
import eq from 'ember-truth-helpers/helpers/eq';
import OutputDrupalFileComponent from './drupal-file.gts';
import { isNone } from '@ember/utils';
import or from 'ember-truth-helpers/helpers/or';
import lt from 'ember-truth-helpers/helpers/lt';

export interface OutputDrupalFilesOptions {}

export interface OutputDrupalFilesArguments extends BaseOutputArguments {
  value: DrupalFile[] | undefined;
  options?: OutputDrupalFilesOptions;
}

export interface OutputDrupalFilesSignature {
  Args: OutputDrupalFilesArguments;
  Element: HTMLSpanElement;
}

const OutputDrupalFilesComponent: TOC<OutputDrupalFilesSignature> = <template>
  {{#if (eq @value.length 1)}}
    <OutputDrupalFileComponent @value={{get @value 0}} ...attributes />
  {{else}}
    {{#let (or (isNone @value) (lt @value.length 1)) as |isNoneOrEmpty|}}
      <span
        class='output output-drupal-files{{if isNoneOrEmpty " output--empty"}}'
        ...attributes
      >
        {{#unless isNoneOrEmpty}}
          <ul class='output-drupal-files__list'>
            {{#each @value as |singleValue|}}
              <li class='output-drupal-files__list-item'>
                <OutputDrupalFileComponent @value={{singleValue}} />
              </li>
            {{/each}}
          </ul>
        {{/unless}}
      </span>
    {{/let}}
  {{/if}}
</template>;
export default OutputDrupalFilesComponent;
