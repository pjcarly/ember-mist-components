import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import Component from '@glimmer/component';
import type Address from '../../interfaces/address';
import { service } from '@ember/service';
import type UiService from '../../services/ui';
import { get } from '@ember/object';
import or from 'ember-truth-helpers/helpers/or';
import OutputTextComponent from '@getflights/ember-field-components/components/output/text';
import OutputCountryCodeComponent from './country-code.gts';
import { use } from 'ember-resources';
import AddressHelperResource, {
  AddressHelper,
} from '../../resources/address-helper.ts';
import { isNone } from '@ember/utils';

export type OutputAddressOptions = Record<never, never>;

export interface OutputAddressArguments extends BaseOutputArguments {
  value: Address | undefined;
}

export interface OutputAddressSignature {
  Args: OutputAddressArguments;
  Element: HTMLImageElement | HTMLSpanElement;
}

export default class OutputAddressComponent extends Component<OutputAddressSignature> {
  @service declare ui: UiService;

  private helperResource = use(
    this,
    AddressHelperResource(() => this.args.value?.countryCode),
  );

  get helper(): AddressHelper {
    return this.helperResource.current;
  }

  <template>
    {{#let (isNone @value) as |isEmpty|}}
      <div class='output-address{{if isEmpty " output--empty"}}' ...attributes>
        {{#unless isEmpty}}
          {{#if this.helper.formatError}}
            <div class='alert alert-warning'>
              Error loading address format
            </div>
          {{/if}}

          {{#if @value}}
            {{#if this.helper.addressFormat}}
              {{#each this.helper.rows as |row|}}
                <div class='row g-0'>
                  <div class='col-12'>
                    {{#each row.columns as |column|}}
                      {{#let (get @value column.field) as |value|}}
                        {{#if (or value column.required)}}
                          {{! @glint-ignore }}
                          <OutputTextComponent @value={{value}} />
                        {{/if}}
                      {{/let}}
                      {{! </div> }}
                    {{/each}}
                  </div>
                </div>
              {{/each}}
            {{/if}}
          {{/if}}

          <div class='row'>
            <div class='col-12 country-select'>
              <OutputCountryCodeComponent @value={{this.helper.countryCode}} />
            </div>
          </div>

          {{#if this.helper.formatLoading}}
            <div class='address-spinner'>
              <this.ui.loadingComponent />
            </div>
          {{/if}}
        {{/unless}}
      </div>
    {{/let}}
  </template>
}
