import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import eq from 'ember-truth-helpers/helpers/eq';
import type MistModel from '../../models/mist-model';
import OutputModelComponent from './model.gts';
import Component from '@glimmer/component';
import { isNone } from '@ember/utils';

export interface OutputModelsOptions {
  inline?: boolean;
  nameField?: string;
}

export interface OutputModelsArguments extends BaseOutputArguments {
  value: MistModel[] | undefined;
  options?: OutputModelsOptions;
}

export interface OutputModelsSignature {
  Args: OutputModelsArguments;
  Element: HTMLSpanElement;
}

export default class OutputModelsComponent extends Component<OutputModelsSignature> {
  get firstValue(): MistModel | undefined {
    if (!this.args.value) {
      return undefined;
    }

    // ED SyncHasMany compatibility
    return 'firstObject' in this.args.value
      ? (this.args.value.firstObject as MistModel | undefined)
      : this.args.value[0];
  }

  get isNoneOrEmpty() {
    return isNone(this.args.value) || this.args.value.length < 1;
  }

  <template>
    <span
      class='output output-models{{if this.isNoneOrEmpty " output--empty"}}'
      ...attributes
    >
      {{#unless this.isNoneOrEmpty}}
        {{#if (eq @value.length 1)}}
          <OutputModelComponent
            @value={{this.firstValue}}
            @options={{@options}}
          />
        {{else}}
          <ul class='output-models__list'>
            {{#each @value as |singleValue|}}
              <li class='output-models__list-item'>
                <OutputModelComponent
                  @value={{singleValue}}
                  @options={{@options}}
                />
              </li>
            {{/each}}
          </ul>
        {{/if}}
      {{/unless}}
    </span>
  </template>
}
