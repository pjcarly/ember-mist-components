import { get } from '@ember/helper';
import { isNone } from '@ember/utils';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';
import { isGroup } from '@getflights/ember-field-components/interfaces/select-option-group';
import Component from '@glimmer/component';
import eq from 'ember-truth-helpers/helpers/eq';

export interface OutputMultiSelectOptions {
  selectOptions: (SelectOption | SelectOptionGroup)[];
  placeholder?: string;
}

export interface OutputMultiSelectArguments extends BaseOutputArguments {
  value: string[] | undefined;
  options: OutputMultiSelectOptions;
}

export interface OutputMultiSelectSignature {
  Args: OutputMultiSelectArguments;
  Element: HTMLSpanElement;
}

export default class OutputMultiSelectComponent extends Component<OutputMultiSelectSignature> {
  get value() {
    const allOptions = this.args.options.selectOptions.flatMap((option) => {
      return isGroup(option) ? option.selectOptions : option;
    });

    return this.args.value?.map((singleValue) => {
      const selectOption = allOptions.find(
        (option) => option.value === singleValue,
      );
      return selectOption?.label ?? `(?) ${this.args.value}`;
    });
  }

  get isNoneOrEmpty() {
    return isNone(this.value) || this.value.length < 1;
  }

  <template>
    <span
      class='output output-multi-select{{if
          this.isNoneOrEmpty
          " output--empty"
        }}'
      ...attributes
    >
      {{#unless this.isNoneOrEmpty}}
        {{#if (eq this.value.length 1)}}
          {{get this.value 0}}
        {{else}}
          <ul class='output-multi-select__list'>
            {{#each this.value as |singleValue|}}
              <li class='output-multi-select__list-item'>
                {{singleValue}}
              </li>
            {{/each}}
          </ul>
        {{/if}}
      {{/unless}}
    </span>
  </template>
}
