import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import type DrupalImage from '../../interfaces/drupal-image';
import type { TOC } from '@ember/component/template-only';
import IconComponent from '../icon.gts';
import ImageURLHelper from '../../helpers/image-url.ts';
import { isNone } from '@ember/utils';

export interface OutputDrupalImageLinkOptions {
  /**
   * Back-end image style to use
   */
  style?: string;
}

export interface OutputDrupalImageLinkArguments extends BaseOutputArguments {
  value: DrupalImage | undefined;
  options?: OutputDrupalImageLinkOptions;
}

export interface OutputDrupalImageLinkSignature {
  Args: OutputDrupalImageLinkArguments;
  Element: HTMLAnchorElement | HTMLSpanElement;
}

const OutputDrupalImageLinkComponent: TOC<OutputDrupalImageLinkSignature> =
  <template>
    {{#let (isNone @value.url) as |isEmpty|}}
      <span
        class='output output-drupal-image-link{{if isEmpty " output--empty"}}'
        ...attributes
      >
        {{#unless isEmpty}}
          <a
            class='output-drupal-image-link__link'
            href={{ImageURLHelper @value @options.style}}
            target='_blank'
            rel='noopener noreferrer'
          >
            {{@value.filename}}
            <IconComponent @name='new-window' />
          </a>
        {{/unless}}
      </span>
    {{/let}}
  </template>;
export default OutputDrupalImageLinkComponent;
