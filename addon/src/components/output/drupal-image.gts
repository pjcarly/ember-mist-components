import type { TOC } from '@ember/component/template-only';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import type DrupalImage from '../../interfaces/drupal-image';
import imageUrl from '../../helpers/image-url.ts';

export interface OutputDrupalImageOptions {
  /**
   * Back-end image style to use
   */
  style?: string;
}

export interface OutputDrupalImageArguments extends BaseOutputArguments {
  value: DrupalImage | undefined;
  options?: OutputDrupalImageOptions;
}

export interface OutputDrupalImageSignature {
  Args: OutputDrupalImageArguments;
  Element: HTMLImageElement;
}

const OutputDrupalImageComponent: TOC<OutputDrupalImageSignature> = <template>
  <img
    class='output output-image'
    src={{imageUrl @value @options.style}}
    alt={{@value.alt}}
    title={{@value.title}}
    ...attributes
  />
</template>;
export default OutputDrupalImageComponent;
