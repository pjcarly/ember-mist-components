import Component from '@glimmer/component';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import type MistModel from '../../models/mist-model.ts';
// @ts-ignore has no types
import hrefTo from 'ember-href-to/helpers/href-to';
import and from 'ember-truth-helpers/helpers/and';
import not from 'ember-truth-helpers/helpers/not';
import { get } from '@ember/object';
import { isNone } from '@ember/utils';

export interface OutputModelOptions {
  inline?: boolean;
  nameField?: string;
}

export interface OutputModelArguments extends BaseOutputArguments {
  value: MistModel | undefined;
  options?: OutputModelOptions;
}

export interface OutputModelSignature {
  Args: OutputModelArguments;
  Element: HTMLSpanElement;
}

export default class OutputModelComponent extends Component<OutputModelSignature> {
  get name() {
    if (this.args.options?.nameField) {
      return get(this.args.value, this.args.options.nameField);
    }

    // @ts-ignore
    return this.args.value?.name;
  }

  <template>
    {{#let (isNone @value) as |isEmpty|}}
      <span
        class='output output-model{{if isEmpty " output--empty"}}'
        ...attributes
      >
        {{#unless isEmpty}}
          {{#if (and @value.hasViewRoute (not @options.inline))}}
            <a
              class='output-model__link'
              href={{hrefTo @value.viewRouteName @value}}
            >
              {{this.name}}
            </a>
          {{else}}
            {{this.name}}
          {{/if}}
        {{/unless}}
      </span>
    {{/let}}
  </template>
}
