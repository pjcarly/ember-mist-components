import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import type DrupalImage from '../../interfaces/drupal-image';
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { modifier } from 'ember-modifier';
import { Carousel } from 'bootstrap.native';
import { concat, fn, hash } from '@ember/helper';
import eq from 'ember-truth-helpers/helpers/eq';
import OutputDrupalImageComponent from './drupal-image.gts';
import { on } from '@ember/modifier';

export interface OutputDrupalImagesOptions {
  /**
   * Back-end image style to use
   */
  style?: string;
  imageClicked?: (image: DrupalImage) => void;
}

export interface OutputDrupalImagesArguments extends BaseOutputArguments {
  value: DrupalImage[] | undefined;
  options?: OutputDrupalImagesOptions;
}

export interface OutputDrupalImagesSignature {
  Args: OutputDrupalImagesArguments;
  Element: HTMLDivElement;
}

export default class OutputDrupalImagesComponent extends Component<OutputDrupalImagesSignature> {
  constructor(owner: any, args: OutputDrupalImagesArguments) {
    super(owner, args);

    this.carouselName = guidFor(this) + '-carousel';
  }

  carouselName;

  bsCarousel?: Carousel;

  carouselModifier = modifier((element: HTMLDivElement) => {
    const carousel = new Carousel(element, {
      interval: false,
    });
    this.bsCarousel = carousel;
  });

  get style() {
    return this.args.options?.style ?? '';
  }

  imageClicked = (image: DrupalImage) => {
    this.args.options?.imageClicked?.(image);
  };

  <template>
    {{#if @value}}
      <div
        id={{this.carouselName}}
        class='carousel slide'
        data-bs-ride='carousel'
        {{this.carouselModifier}}
        ...attributes
      >
        <div class='carousel-indicators'>
          {{#each @value as |_imageValue index|}}
            <button
              type='button'
              data-bs-target={{concat '#' this.carouselName}}
              data-bs-slide-to={{index}}
              class={{if (eq index 0) ' active'}}
              aria-current={{if (eq index 0) 'true'}}
            ></button>
          {{/each}}
        </div>

        <div class='carousel-inner' role='listbox'>
          {{#each @value as |imageValue index|}}
            <div class='carousel-item{{if (eq index 0) " active"}}'>
              <OutputDrupalImageComponent
                @value={{imageValue}}
                @options={{hash style=this.style}}
                class='d-block w-100'
                {{on 'click' (fn this.imageClicked imageValue)}}
              />
            </div>
          {{/each}}
        </div>

        <button
          class='carousel-control-prev'
          type='button'
          data-bs-target='#carouselExampleIndicators'
          data-bs-slide='prev'
        >
          <span class='carousel-control-prev-icon' aria-hidden='true'></span>
          <span class='visually-hidden'>Previous</span>
        </button>
        <button
          class='carousel-control-next'
          type='button'
          data-bs-target='#carouselExampleIndicators'
          data-bs-slide='next'
        >
          <span class='carousel-control-next-icon' aria-hidden='true'></span>
          <span class='visually-hidden'>Next</span>
        </button>
      </div>
    {{/if}}
  </template>
}
