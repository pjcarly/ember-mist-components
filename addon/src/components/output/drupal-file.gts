import type { TOC } from '@ember/component/template-only';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import { concat } from '@ember/helper';
import IconComponent from '../icon.gts';
import type DrupalFile from '../../interfaces/drupal-file';
import { isNone } from '@ember/utils';

export interface OutputDrupalFileOptions {
  filename?: string;
}

export interface OutputDrupalFileArguments extends BaseOutputArguments {
  value: DrupalFile | undefined;
  options?: OutputDrupalFileOptions;
}

export interface OutputDrupalFileSignature {
  Args: OutputDrupalFileArguments;
  Element: HTMLSpanElement;
}

const OutputDrupalFileComponent: TOC<OutputDrupalFileSignature> = <template>
  {{#let (isNone @value.id) as |isEmpty|}}
    <span
      class='output output-drupal-file{{if isEmpty " output--empty"}}'
      ...attributes
    >
      {{#unless isEmpty}}
        <a
          class='output-drupal-file__link'
          type='button'
          href={{if
            @options.filename
            (concat @value.url '&filename=' @options.filename)
            @value.url
          }}
          target='_blank'
          rel='noopener noreferrer'
        >
          {{#if @options.filename}}
            {{@options.filename}}
          {{else}}
            {{@value.filename}}
          {{/if}}
          <IconComponent @name='download' />
        </a>
      {{/unless}}
    </span>
  {{/let}}
</template>;
export default OutputDrupalFileComponent;
