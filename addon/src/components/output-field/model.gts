import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputModelComponent, {
  type OutputModelOptions,
  type OutputModelSignature,
} from '../output/model.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldModelComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputModelOptions>;
  Element: OutputModelSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputModelComponent}}
      @fieldType='model'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
