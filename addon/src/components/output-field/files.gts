import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputDrupalFilesComponent, {
  type OutputDrupalFilesOptions,
  type OutputDrupalFilesSignature,
} from '../output/drupal-files.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldFilesComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputDrupalFilesOptions>;
  Element: OutputDrupalFilesSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputDrupalFilesComponent}}
      @fieldType='files'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
