import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import OutputModelsComponent, {
  type OutputModelsOptions,
  type OutputModelsSignature,
} from '../output/models.gts';

export default class OutputFieldHasManyComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputModelsOptions>;
  Element: OutputModelsSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputModelsComponent}}
      @fieldType='has-many'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
