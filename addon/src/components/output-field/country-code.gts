import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputCountryCodeComponent, {
  type OutputCountryCodeOptions,
  type OutputCountryCodeSignature,
} from '../output/country-code.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldCountryCodeComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputCountryCodeOptions>;
  Element: OutputCountryCodeSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputCountryCodeComponent}}
      @fieldType='country'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
