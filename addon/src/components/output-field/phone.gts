import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputPhoneComponent, {
  type OutputPhoneOptions,
  type OutputPhoneSignature,
} from '../output/phone.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldPhoneComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputPhoneOptions>;
  Element: OutputPhoneSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputPhoneComponent}}
      @fieldType='phone'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
