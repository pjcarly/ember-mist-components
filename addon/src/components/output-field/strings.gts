import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import OutputStringsComponent, {
  type OutputStringsOptions,
  type OutputStringsSignature,
} from '../output/strings.gts';

export default class OutputFieldStringsComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputStringsOptions>;
  Element: OutputStringsSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputStringsComponent}}
      @fieldType='strings'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
