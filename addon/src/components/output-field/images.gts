import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputDrupalImagesComponent, {
  type OutputDrupalImagesOptions,
  type OutputDrupalImagesSignature,
} from '../output/drupal-images.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldImagesComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputDrupalImagesOptions>;
  Element: OutputDrupalImagesSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputDrupalImagesComponent}}
      @fieldType='images'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
