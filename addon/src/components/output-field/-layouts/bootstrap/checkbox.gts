import type { OutputFieldLayoutSignature } from '@getflights/ember-field-components/components/output-field/-layouts/index';
import Component from '@glimmer/component';
import { modifier } from 'ember-modifier';

export default class OutputFieldBootstrapLayoutCheckbox extends Component<OutputFieldLayoutSignature> {
  private readonly outputClass: string = 'output-field__output';

  outputCheckboxModifier = modifier((element: HTMLElement) => {
    let inputElement = element.matches('input')
      ? element
      : element.querySelector('input');

    inputElement?.classList.add('form-check-input');

    return () => {
      inputElement?.classList.remove('form-check-input');
    };
  });

  <template>
    <div class='output-field'>
      {{#if @label}}
        <label class='output-field__label'>
          {{@label}}
        </label>
      {{/if}}

      <div class='output-field__box'>
        {{#each @prefixes as |prefix|}}
          <span class='output-field__prefix'>
            {{prefix}}
          </span>
        {{/each}}

        <@outputComponent
          class={{this.outputClass}}
          ...attributes
          {{this.outputCheckboxModifier}}
        />

        {{#each @suffixes as |suffix|}}
          <span class='output-field__suffix'>
            {{suffix}}
          </span>
        {{/each}}
      </div>
    </div>
  </template>
}
