import Component from '@glimmer/component';
import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputTextMaskComponent, {
  type OutputTextMaskOptions,
  type OutputTextMaskSignature,
} from '../output/text-mask.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldTextMaskComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputTextMaskOptions>;
  Element: OutputTextMaskSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      {{! @glint-ignore }}
      @outputComponent={{OutputTextMaskComponent}}
      @fieldType='text'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
