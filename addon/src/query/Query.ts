import type Store from '@ember-data/store';
import { A } from '@ember/array';
import type HttpService from '../services/http.ts';
import Condition, { type QueryFilter } from './Condition.ts';
import Order from './Order.ts';
import type NativeArray from '@ember/array/-private/native-array';
import type ModelRegistry from 'ember-data/types/registries/model';

export interface QueryParams {
  page?: {
    number?: number;
    limit?: number;
    includeLimits?: { [key: string]: number };
  };
  include?: string;
  sort?: string;
  filter?: { [key: string]: QueryFilter | number | string };
  _single?: boolean;
  fields?: { [key: string]: string };
}

export default class Query<
  K extends keyof ModelRegistry = keyof ModelRegistry,
> {
  private modelName: K; // The type of model you want to query
  private conditions: Condition[] = []; // The conditions of the query
  private conditionLogic?: string; // The condition logic you want to use for the condtions. Defaults to AND for everything
  private orders: Order[] = []; // The sort orders
  private includes: string[] = []; // Which related records you want the response to include
  private listView?: string; // Which list view was selected
  private includeDefaultIncludes = true;
  private fields = new Map<string, string[]>();
  private includeLimits = new Map<string, number>();

  private searchFields: string[] = []; // The field you want to search on
  private searchQuery?: string | null; // The search query you want to search with

  private limit?: number; // The limit of records you want the result to have
  private page = 1; // The page of results you want to be on

  public fetchCount = 0;

  constructor(modelName: K) {
    this.modelName = modelName;
  }

  /**
   * Adds a condition to the Query
   * @param condition Condition to add
   */
  addCondition(condition: Condition): Query<K> {
    this.conditions.push(condition);
    return this;
  }

  /**
   * Removes an existing condition from the Query
   * @param condition Condition to remove
   */
  removeCondition(condition: Condition): Query<K> {
    this.conditions = this.conditions.filter((c) => {
      return c !== condition;
    });
    return this;
  }

  /**
   * Removes all the conditions that were already added to the Query
   */
  clearConditions(): Query<K> {
    this.conditions = [];
    return this;
  }

  /**
   * Adds an order to the query, the order in which orders were added determine the sorting of the orders
   * @param order Order to add
   */
  addOrder(order: Order): Query<K> {
    this.orders.push(order);
    return this;
  }

  /**
   * Removes the orders that were already added to the Query
   */
  clearOrders(): Query<K> {
    this.orders = [];
    return this;
  }

  /**
   * Returns the orders
   */
  getOrders(): Order[] {
    return this.orders;
  }

  /**
   * Returns the current page the query is on
   */
  getCurrentPage(): number {
    return this.page;
  }

  /**
   * Adds an include to the Query
   * @param relationshipName The relationship to include in the results
   */
  addInclude(relationshipName: string) {
    if (this.includes.indexOf(relationshipName) === -1) {
      this.includes.push(relationshipName);
    }

    return this;
  }

  /**
   * Add a limit for an included relationship (to limit the result of an relationship)
   * @param relationshipName The relationship you want to add a limit for
   * @param limit The limit
   */
  addIncludeLimit(relationshipName: string, limit: number) {
    this.includeLimits.set(relationshipName, limit);
    return this;
  }

  /**
   * Remove an includeLimit for a relationship
   * @param relationshipName The name of the relationship you want to remove
   */
  removeIncludeLimit(relationshipName: string) {
    if (this.includeLimits.has(relationshipName)) {
      this.includeLimits.delete(relationshipName);
    }

    return this;
  }

  /**
   * Check if we have an include limit for a relationshipname
   * @param relationshipName The name of the relationship you want to check
   */
  hasIncludeLimit(relationshipName: string): boolean {
    return this.includeLimits.has(relationshipName);
  }

  /**
   * Remove all the includeLimits
   */
  clearIncludeLimits() {
    this.includeLimits.clear();
    return this;
  }

  /**
   * Limits the query to a provided list view
   * @param id The ID of the list view
   */
  setListView(id: string) {
    this.listView = id;
    return this;
  }

  /**
   * Should the query include the default includes defined on the model (defaults to true)
   * @param value Whether or not to include the default model includes
   */
  setIncludeDefaultIncludes(value: boolean) {
    this.includeDefaultIncludes = value;
    return this;
  }

  /**
   * Removes the list view
   */
  clearListView() {
    this.listView = undefined;
    return this;
  }

  /**
   * Check if a query has a list view
   */
  hasListView(): boolean {
    return this.listView !== undefined;
  }

  /**
   * Removes all the includes
   */
  clearIncludes() {
    this.includes = [];
    return this;
  }

  /**
   * Limit the results to certain amount of results
   * @param limit the limit
   */
  setLimit(limit: number) {
    this.limit = limit;
    return this;
  }

  /**
   * Removes the limit
   */
  clearLimit() {
    this.limit = undefined;
    return this;
  }

  /**
   * Check if a query has a limit
   */
  hasLimit(): boolean {
    return this.limit !== undefined;
  }

  /**
   * Get the limit
   */
  getLimit(): number | undefined {
    return this.limit;
  }

  /**
   * Set the condition logic. For example AND(1,2, OR(3, 4)). The number being the position of the codntion in the condtions array
   * If this value is undefined, AND() will be used for everything
   * @param conditionLogic the logic you want to set
   */
  setConditionLogic(conditionLogic: string) {
    this.conditionLogic = conditionLogic;
    return this;
  }

  /**
   * Removes the filter logic
   */
  clearConditionLogic() {
    this.conditionLogic = undefined;
    return this;
  }

  /**
   * Sets the page of which you want the results to be returned (depending on the limit and the amount of results)
   * @param page The page you want returned
   */
  setPage(page: number) {
    this.page = page;
    return this;
  }

  /**
   * Clears the page
   */
  clearPage() {
    this.page = 1;
    return this;
  }

  /**
   * Sets the type of model you want to query
   * @param modelName The new model name
   */
  setModelName<X extends keyof ModelRegistry>(modelName: X): Query<X> {
    // @ts-ignore
    this.modelName = modelName;
    // @ts-ignore
    return this as Query<X>;
  }

  /**
   * Sets a search query on the Query.
   * @param search The search query
   * @param searchField The field you want to apply te search to, if this is not provided the first Sort field will be used, if that is undefined 'name' will be used
   */
  setSearch(search: string, ...searchFields: string[]) {
    this.searchQuery = search;
    this.searchFields = searchFields;
    return this;
  }

  /**
   * Clears the search and the searchfield
   */
  clearSearch() {
    this.searchQuery = undefined;
    this.searchFields = [];
    return this;
  }

  /**
   * Add a field for a certain type to limit the query by
   * @param type The type to set a field for
   * @param field The field to set
   */
  addField(type: string, field: string) {
    if (!this.fields.has(type)) {
      this.fields.set(type, []);
    }

    this.fields.get(type)?.push(field);
    return this;
  }

  /**
   * Set fields for a certain type
   * @param type The type to set the fields for
   * @param fields Which fields to set
   */
  setFields(type: string, fields: string[]) {
    this.fields.set(type, fields);
    return this;
  }

  /**
   * Clear any fields for a possible type
   * @param type The type to clear
   */
  clearFieldsForType(type: string) {
    this.fields.delete(type);
    return this;
  }

  /**
   * Clear all the fields
   */
  clearFields() {
    this.fields = new Map<string, string[]>();
    return this;
  }

  /**
   * Returns the fields where the search should be performed on
   */
  get searchFieldsComputed(): string[] | undefined {
    if (this.searchFields && this.searchFields.length > 0) {
      return this.searchFields;
    }
  }

  /**
   * Checks if the query has conditions
   */
  get hasConditions(): boolean {
    return this.conditions.length > 0;
  }

  /**
   * Checks if the query has orders
   */
  get hasOrders(): boolean {
    return this.orders.length > 0;
  }

  /**
   * Checks if the query has condition logic
   */
  get hasConditionLogic(): boolean {
    return this.conditionLogic ? true : false;
  }

  /**
   * Increments the page by 1
   */
  nextPage() {
    this.page++;
  }

  /**
   * Reduces page by 1
   */
  prevPage() {
    if (this.page > 1) {
      this.page--;
    }
  }

  /**
   * Executes the query, and returns a Promise. Native Ember Data store is used, all fetched models will be loaded in the store
   */
  async fetch(store: Store) {
    // First we lookup the default includes and add them to the query
    if (this.includeDefaultIncludes) {
      const defaultIncludes = this.getDefaultIncludes(store);

      for (const defaultInclude of defaultIncludes) {
        this.addInclude(defaultInclude);
      }
    }

    return store.query(this.modelName, this.queryParams).then((results) => {
      this.fetchCount++;
      return results;
    });
  }

  /**
   * Executes the query for a single record, and returns a Promise. Native Ember Data store is used, all fetched models will be loaded in the store
   */
  async fetchRecord(store: Store): Promise<ModelRegistry[K] | null> {
    // First we lookup the default includes and add them to the query
    if (this.includeDefaultIncludes) {
      const defaultIncludes = this.getDefaultIncludes(store);

      for (const defaultInclude of defaultIncludes) {
        this.addInclude(defaultInclude);
      }
    }

    const queryParams = this.queryParams;
    queryParams._single = true;

    return store
      .queryRecord(this.modelName, queryParams)
      .then((result: any) => {
        this.fetchCount++;
        return result;
      });
  }

  /**
   * This method can be called when you want to fetch records from an endpoint different than
   * the model endpoint. using fetch or fetchRecord the ModelStore is used to define the endpoint
   * But this method you can use a custom endpoint. This can be useful for:
   *  - An model that can be queried from 2 different endpoints
   *  - An endpoint returning multiple types of models
   *
   * @param http The HTTP service to use for the fetch
   * @param store The store you want to push the results in
   * @param endpoint The endpoint you want to fetch from
   */
  async fetchFromEndpoint(
    http: HttpService,
    store: Store,
    endpoint: string,
    abortController?: AbortController,
  ): Promise<NativeArray<ModelRegistry[K]>> {
    return http
      .fetch(endpoint, 'GET', undefined, this.queryParams, abortController)
      .then((response: Response) => {
        this.fetchCount++;

        return response.json().then((results) => {
          const models = A<ModelRegistry[K]>();

          if (results.data) {
            // Lets push the results in the store
            store.pushPayload(results);

            // And now get each result from the Store
            results.data.forEach((result: any) => {
              const model = store.peekRecord(
                result.type,
                result.id,
              ) as ModelRegistry[K];
              models.pushObject(model);
            });
          }

          return Object.assign(models, { meta: results.meta });
        });
      });
  }

  /**
   * This method can be called when you want to fetch a single record from an endpoint different than
   * the model endpoint. using fetch or fetchRecord the ModelStore is used to define the endpoint
   * But this method you can use a custom endpoint. This can be useful for:
   *  - An model that can be queried from 2 different endpoints
   *  - An endpoint returning multiple types of models
   * The difference beweteen fetchFromEndpoint and fetchRecordFromEndpoint is that this method returns a single model
   * instead of an Array of models
   *
   * @param http The HTTP service to use for the fetch
   * @param store The store you want to push the results in
   * @param endpoint The endpoint you want to fetch from
   */
  async fetchRecordFromEndpoint(
    http: HttpService,
    store: Store,
    endpoint: string,
    abortController?: AbortController,
  ): Promise<ModelRegistry[K] | null> {
    return http
      .fetch(endpoint, 'GET', undefined, this.queryParams, abortController)
      .then((response: Response) => {
        this.fetchCount++;

        return response.json().then((results) => {
          if (results.data?.id && results.data?.type) {
            // Lets push the results in the store
            store.pushPayload(results);

            return store.peekRecord(
              results.data.type,
              results.data.id,
            ) as ModelRegistry[K];
          }

          return null;
        });
      });
  }

  /**
   * Returns the default includes defined on the modelclass;
   * @param store The store where you want to search for the default includes of the model
   */
  getDefaultIncludes(store: Store): string[] {
    const modelClass = store.modelFor(this.modelName);

    if (
      Object.prototype.hasOwnProperty.call(modelClass, 'settings') &&
      Object.prototype.hasOwnProperty.call(
        // @ts-ignore
        modelClass.settings,
        'defaultIncludes',
      )
    ) {
      // @ts-ignore
      return modelClass.settings.defaultIncludes;
    }

    return [];
  }

  /**
   * Sets all the values based on the provided Query
   * IMPORTANT: all previous values will be overridden
   * @param query The query to copy from
   */
  copyFrom(query: Query<keyof ModelRegistry>) {
    this.setModelName(query.modelName);

    this.clearConditions();
    query.conditions.forEach((condition) => {
      this.addCondition(
        new Condition(
          condition.field,
          condition.operator,
          condition.value,
          condition.id,
        ),
      );
    });

    this.conditionLogic = query.conditionLogic;

    this.clearOrders();
    query.orders.forEach((order) => {
      this.addOrder(new Order(order.field, order.direction));
    });

    this.includes = query.includes.slice();
    this.limit = query.limit;
    this.page = query.page;
    this.searchFields = query.searchFields;
    this.searchQuery = query.searchQuery;
    this.listView = query.listView;
    this.fields = new Map(query.fields);
    this.includeLimits = new Map(query.includeLimits);
    return this;
  }

  /**
   * Returns the query parameters that can be used in a EmberData store.query(modelName, queryParams) function.
   */
  get queryParams(): QueryParams {
    const queryParams: QueryParams = {};

    if (this.page > 1) {
      queryParams.page = {
        number: this.page,
      };
    }

    // The limit of amount of results we want to receive
    if (this.limit) {
      if (!queryParams.page) {
        queryParams.page = {};
      }
      queryParams.page.limit = this.limit;
    }

    if (this.includeLimits.size > 0) {
      if (!queryParams.page) {
        queryParams.page = {};
      }
      queryParams.page.includeLimits = {};

      this.includeLimits.forEach((limit, relationshipName) => {
        if (queryParams.page?.includeLimits) {
          queryParams.page.includeLimits[relationshipName] = limit;
        }
      });
    }

    // Any related entities we want to receive
    if (this.includes && this.includes.length > 0) {
      queryParams.include = this.includes.join(',');
    }

    // The sort order of the results
    if (this.orders && this.orders.length > 0) {
      const orders: string[] = [];
      for (const order of this.orders) {
        orders.push(order.orderParam);
      }

      queryParams.sort = orders.join(',');
    }

    // Now the filters
    const filterParam: { [key: string]: QueryFilter | number | string } = {};
    let conditionIndex = 1; // we keep an index over all filters, as each filter will be passed in the query string with as key the index

    // Next we add possible conditions added to the query params object
    if (this.conditions && this.conditions.length > 0) {
      for (const condition of this.conditions) {
        filterParam[conditionIndex++] = condition.conditionParam;
      }
    }

    // We must also check if a search query was passed, and add a condition for it as well
    // let alteredLogic = null;
    // if (this.search) {
    //   const searchCondition = new Condition(
    //     this.searchFieldComputed,
    //     this.searchOperator,
    //     this.search
    //   );

    //   filterParam['_search']

    //   filterParam[conditionIndex++] = searchCondition.conditionParam;

    //   // because we added something to the filters based on a search, we must also check for logic
    //   // Standard, the logic will not thnk of searches. only for conditions that were added to the query

    //   if (this.conditionLogic) {
    //     alteredLogic = `AND(${this.conditionLogic}, ${conditionIndex - 1})`;
    //   }
    // }

    if (this.listView) {
      filterParam['_listview'] = this.listView;
    }

    if (this.searchQuery) {
      filterParam['_query'] = this.searchQuery;

      if (this.searchFieldsComputed) {
        filterParam['_queryFields'] = this.searchFieldsComputed.join(',');
      }
    }

    // if (alteredLogic) {
    //  filterParam['_logic'] = alteredLogic;
    // } else if (this.conditionLogic) {
    if (this.conditionLogic) {
      filterParam['_logic'] = this.conditionLogic;
    }

    if (this.fields.size > 0) {
      const fieldsParam: { [key: string]: string } = {};
      for (const [key, value] of this.fields.entries()) {
        fieldsParam[key] = value.join(',');
      }

      queryParams.fields = fieldsParam;
    }

    // And finally, if there were conditions, we add them to the query params
    if (Object.keys(filterParam).length > 0) {
      queryParams.filter = filterParam;
    }

    return queryParams;
  }
}
