/**
 * Any Address field, except for countryCode
 */
export type FieldOfAddress = Exclude<keyof Address, 'countryCode'>;

export default interface Address {
  countryCode: string;
  administrativeArea?: string | undefined;
  locality?: string | undefined;
  dependentLocality?: string | undefined;
  postalCode?: string | undefined;
  sortingCode?: string | undefined;
  addressLine1?: string | undefined;
  addressLine2?: string | undefined;
}

export type AddressAttr = Readonly<Address>;
