import DrupalModel from '../models/drupal-model.ts';
import type { AddressAttr } from './address.ts';
import type DrupalImage from './drupal-image';

export default interface ETicketConfigurationModelInterface
  extends DrupalModel {
  name: string;
  code: string;
  address?: AddressAttr;
  logo?: DrupalImage;
  phone?: string;
  // customer: CompanyModelInterface | ContactModelInterface;
  // template?: PDFModel;
}
