export default interface DrupalFile {
  id: string;
  url: string;
  uri: string;
  hash: string;
  filesize: number;
  filename: string;
  filemime: string;
}
