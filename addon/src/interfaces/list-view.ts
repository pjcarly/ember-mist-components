import Model from '@ember-data/model';
import type ModelRegistry from 'ember-data/types/registries/model';

export default interface ListViewInterface {
  model: keyof ModelRegistry;
  rows?: number;
  columns: string[];
  sortOrder?: {
    field: string;
    dir?: 'ASC' | 'DESC';
  };
}

export type ListViewWithoutModel = Omit<ListViewInterface, 'model'>;

type ModelClass = typeof Model;

export interface ModelClassWithListViews extends ModelClass {
  settings?: {
    listViews?: {
      [key: string]: ListViewWithoutModel;
    };
  };
}
