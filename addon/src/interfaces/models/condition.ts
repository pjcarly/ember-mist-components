import Model from '@ember-data/model';

export default interface ConditionModelInterface extends Model {
  // name: string;
  // status: boolean;
  field: string;
  operator: string;
  value: string;
}
