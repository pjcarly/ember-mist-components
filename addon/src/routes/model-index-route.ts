import Route from '@ember/routing/route';
import EntityCacheService from '../services/entity-cache.ts';
import { service } from '@ember/service';
import type Transition from '@ember/routing/transition';
import type Controller from '@ember/controller';
import Model from '@ember-data/model';
import type SessionService from 'ember-simple-auth/services/session';
import type ModelRegistry from 'ember-data/types/registries/model';

export default abstract class ModelIndexRoute extends Route {
  @service declare entityCache: EntityCacheService;
  @service declare session: SessionService;

  abstract modelName: keyof ModelRegistry;
  listViewGrouping?: string;
  hideNew = false;

  async beforeModel(transition: Transition) {
    this.session.requireAuthentication(transition, 'login');
    return super.beforeModel(transition);
  }

  async afterModel(model: Model, transition: Transition) {
    await super.afterModel(model, transition);
    this.entityCache.clearReturnToModel();
  }

  // @ts-ignore
  setupController(
    controller: Controller,
    model: Model,
    transition: Transition,
  ) {
    // @ts-ignore
    super.setupController(controller, model, transition);
    // @ts-ignore
    controller.set('modelName', this.modelName);
    // @ts-ignore
    controller.set('listViewGrouping', this.listViewGrouping);
    // @ts-ignore
    controller.set('hideNew', this.hideNew);
  }

  activate(transition: Transition) {
    super.activate(transition);
    window.scrollTo(0, 0);
  }
}
