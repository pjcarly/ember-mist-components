import ModelNewRoute from './model-new-route.ts';
import LoggedInUserService from '../services/logged-in-user.ts';
import Model from '@ember-data/model';
import { service } from '@ember/service';

export default abstract class ModelNewWithOwnerRoute extends ModelNewRoute {
  @service declare loggedInUser: LoggedInUserService;

  async afterModel(model: Model) {
    // @ts-ignore
    model.set('owner', this.loggedInUser.user);
  }
}
