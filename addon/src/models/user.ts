import DrupalModel from '../models/drupal-model.ts';
import type DrupalImage from '../interfaces/drupal-image';
import { assert } from '@ember/debug';
import type UserModelInterface from '../interfaces/user.ts';
import validation from '@getflights/ember-attribute-validations/decorators/validation';
import { required } from '@getflights/ember-attribute-validations/validations/required';
import modelListView from '../decorators/model-list-view.ts';
import { attr } from '@ember-data/model';
import field from '@getflights/ember-field-components/decorators/field';
import text from '@getflights/ember-field-components/fields/text';
import image from '../fields/image.ts';
import datetime from '../fields/datetime.ts';
import emailField from '@getflights/ember-field-components/fields/email';
import checkboxSwitch from '@getflights/ember-field-components/fields/checkbox-switch';
import number from '@getflights/ember-field-components/fields/number';
import { precision } from '@getflights/ember-attribute-validations/validations/precision';
import { wholenumber } from '@getflights/ember-attribute-validations/validations/wholenumber';
import multiSelect from '../fields/multi-select.ts';

@modelListView('default', {
  columns: ['name', 'first-name', 'last-name', 'mail', 'phone'],
})
export default class UserModel
  extends DrupalModel
  implements UserModelInterface
{
  @attr('string')
  declare name: string;

  @attr('boolean')
  @field(checkboxSwitch())
  declare status: boolean;

  @attr('string')
  @field(text({ required: true }))
  @validation(required())
  declare firstName: string;

  @attr('string')
  @field(text({ required: true }))
  @validation(required())
  declare lastName: string;

  @attr('string')
  @field(emailField({ required: true }))
  @validation(required())
  declare mail: string;

  @attr('image')
  @field(image())
  declare userPicture?: DrupalImage;

  @attr('number')
  @field(number({ precision: 10, decimals: 0 }))
  @validation(precision(10), wholenumber())
  declare unseenNotifications: number;

  @attr('datetime')
  @field(datetime())
  declare notificationsViewed?: Date;

  @attr('strings')
  @field(multiSelect())
  declare permissions?: string[];

  setNotificationsViewed() {
    assert('Not yet implemented');
  }

  setAllNotificationsRead() {
    assert('Not yet implemented');
  }

  hasPermission(permission: string) {
    return this.permissions?.includes(permission) ?? false;
  }
}
