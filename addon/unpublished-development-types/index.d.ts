// Add any types here that you need for local development only.
// These will *not* be published as part of your addon, so be careful that your published code does not rely on them!

/**
 * TYPES
 */
import 'ember-cached-decorator-polyfill';

/**
 * GLINT
 */
import '@glint/environment-ember-loose';
import '@glint/environment-ember-template-imports';

import type MistComponentsRegistry from '@getflights/ember-mist-components/template-registry';
import type FieldComponentsRegistry from '@getflights/ember-field-components/template-registry';
import type EmberTruthRegistry from 'ember-truth-helpers/template-registry';
import type EmberIntlRegistry from 'ember-intl/template-registry';
import type EmberStargateRegistry from 'ember-stargate/template-registry';
import type EmberConcurrencyRegistry from 'ember-concurrency/template-registry';
import type EmberBasicDropdownRegistry from 'ember-basic-dropdown/template-registry';
import type EmberPowerSelectRegistry from 'ember-power-select/template-registry';

declare module '@glint/environment-ember-loose/registry' {
  export default interface Registry
    extends MistComponentsRegistry,
      FieldComponentsRegistry,
      EmberTruthRegistry,
      EmberIntlRegistry,
      EmberStargateRegistry,
      EmberConcurrencyRegistry,
      EmberBasicDropdownRegistry,
      EmberPowerSelectRegistry /* other addon registries */ {
    // local entries
  }
}

/**
 * EMBER DATA
 */
// Model Registry
// - Addons
import type MistModelRegistry from '@getflights/ember-mist-components/models/registry';
// - Local development only
import type ListViewModelInterface from '@getflights/@getflights/ember-mist-components/interfaces/models/list-view';

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry extends MistModelRegistry {
    // local entries
    'list-view': ListViewModelInterface;
  }
}

// Transform registry
import type MistTransformRegistry from '@getflights/ember-mist-components/transforms/registry';

declare module 'ember-data/types/registries/transform' {
  export default interface TransformRegistry extends MistTransformRegistry {}
}
